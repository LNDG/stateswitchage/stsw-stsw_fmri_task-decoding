% Get task design information for MRI session

IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
        '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';...
        '1215';'1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
        '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
        '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
        '2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
        '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
        '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2237';'2238';...
        '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

%% load behavioral data

behavData = load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/A_MergeIndividualData/B_data/SS_MergedDynamic_EEG_MRI_YA_09-Mar-2018.mat');

%% extract data by subject

for indID = 1:numel(IDs)
        
    %% get behavioral data (incl. condition assignment)

    behavIdx = find(strcmp(behavData.IDs_all, IDs{indID})); % get index of current subject in behavioral matrix
    SubjBehavData.Atts          = behavData.MergedDataMRI.Atts(:,behavIdx);
    SubjBehavData.StateOrders   = behavData.MergedDataMRI.StateOrders(:,behavIdx);
    SubjBehavData.RTs           = behavData.MergedDataMRI.RTs(:,behavIdx);
    SubjBehavData.Accs          = behavData.MergedDataMRI.Accs(:,behavIdx);
    SubjBehavData.CuedAttributes = reshape(cat(1,behavData.MergedDataMRI.expInfo{behavIdx}.AttCuesRun{:})',256,1);

    % create matrix with crucial condition info
    TrlInfo = NaN(256,7);
    for indTrial = 1:256
       tmp_cuedTrial = SubjBehavData.CuedAttributes(indTrial);
       if ismember(1,tmp_cuedTrial{:})
           TrlInfo(indTrial,1) = 1;
       end
       if ismember(2,tmp_cuedTrial{:})
           TrlInfo(indTrial,2) = 1;
       end
       if ismember(3,tmp_cuedTrial{:})
           TrlInfo(indTrial,3) = 1;
       end
       if ismember(4,tmp_cuedTrial{:})
           TrlInfo(indTrial,4) = 1;
       end
    end
    TrlInfo(:,5) = repmat(1:8,1,32)'; % serial position
    TrlInfo(:,6) = SubjBehavData.Atts;
    TrlInfo(:,7) = 1:256;
    TrlInfo(:,8) = SubjBehavData.StateOrders;
    TrlInfo(:,9) = SubjBehavData.RTs;
    TrlInfo(:,10) = SubjBehavData.Accs;
    
    %% add winning options for each feature
    
    % get trials where two options (in two-state scenario) align
    % operationalized via the relative response agreement among the cues

    winningOptions = NaN(256,4);
    count = 1;
    for indRun = 1:4 % loop across runs
        for indBlock = 1:8 % loop across blocks
            for indTrial = 1:8 % loop across trials
                if ~isempty(behavData.MergedDataMRI.expInfo{behavIdx})
                    cuedAttributes = behavData.MergedDataMRI.expInfo{behavIdx}.AttCuesRun{indRun}{indBlock,indTrial};
                    for indFeature = 1:4
                        winningOptions(count,indFeature) = behavData.MergedDataMRI.expInfo{behavIdx}.HighProbChoiceRun{indRun}{indBlock,indTrial}(indFeature);
                    end
                    count = count + 1; % increase trial number;
                else
                   winningOptions(:,:) = NaN;
                end
            end
        end
    end
    
    TrlInfo(:,11:14) = winningOptions;
    
    %% add combinatorials
    
    addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/study_information/paradigm/MAT/functions/allcomb/');
    
    % get all 16 unique sequences
    combs = allcomb([1,2],[1,2],[1,2],[1,2]);
    % get indication for the winning options for all trials still in the EEG recording
    curTrlWins = TrlInfo(:,11:14);
    % for each trial, assign combination ID
    for indTrial = 1:size(curTrlWins,1)
        TrlInfo(indTrial,15) = double(find(ismember(combs, curTrlWins(indTrial,:), 'rows')));        
    end
    
    %% add info
    
    TrlInfoLabels = {'Att1 cued', 'Att2 cued', 'Att3 cued', 'Att4 cued', ...
        'Serial position', 'WinningAtt', 'Continuous position', 'Cue Dimensionality',...
        'RT', 'Acc', 'Feat1Win', 'Feat2Win', 'Feat3Win', 'Feat4Win', 'Combinatorial'};
    
    %% save TrialInfo
    
    pn.dataOut = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP/L_V5motion/B_data/C_trialInfo/';
    save([pn.dataOut,IDs{indID},'_TrlInfo_mat'],'TrlInfo', 'TrlInfoLabels')
    
end
