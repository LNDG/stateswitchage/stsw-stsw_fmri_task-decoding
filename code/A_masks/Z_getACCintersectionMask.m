
currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..', '..'))
rootpath = pwd;

pn.data = fullfile(rootpath, 'data', 'A_standards');
pn.tools = fullfile(rootpath, '..', 'extended_preproc', 'tools'); 
    addpath(fullfile(pn.tools, 'nifti_toolbox'));

pn.taskpls = fullfile(pn.data, 'taskPLS_STSWD_SPM_YA_OA_3mm_1000P1000B_BfMRIbsr_lv1_unthresholded.img');
pn.mask_manual = fullfile(pn.data, 'ACC_rough_manual.nii.gz');
pn.mask_out = fullfile(pn.data, 'mask_ACC.nii');

% load masks
taskpls = load_untouch_nii(pn.taskpls);
mask_manual = load_untouch_nii(pn.mask_manual);

taskpls = taskpls.img;
taskpls(taskpls <3) = 0;
taskpls(taskpls >=3) = 1;

%% get binary matrices of intersection

intersection = double(mask_manual.img).*double(taskpls);

mask_manual.img = intersection;
save_untouch_nii(mask_manual,[pn.mask_out])
