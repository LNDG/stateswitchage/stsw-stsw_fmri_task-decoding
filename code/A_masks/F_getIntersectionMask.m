pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/';
pn.tools	= [pn.root, 'analyses/B4_PLS_preproc2/T_tools/'];  addpath(genpath(pn.tools));

pn.JuelichMask = [pn.root, 'analyses/L_V5motion/B_data/A_standards/VisualCortex_Julich_thr_MNI_3mm.nii.gz'];
pn.GMmask = [pn.root, 'analyses/L_V5motion/B_data/A_standards/mni_icbm152_nlin_sym_09c/mni_icbm152_gm_tal_nlin_sym_09c_MNI_3mm.nii.gz'];

pn.Visual = [pn.root, 'analyses/L_V5motion/B_data/A_standards/VisualCortex_unified_thr_MNI_3mm.nii.gz'];

% load masks
JuelichMask = load_untouch_nii(pn.JuelichMask);
GMmask = load_untouch_nii(pn.GMmask);

%% get binary matrices of subregions

maskLeft = JuelichMask.img.*GMmask.img;

JuelichMask.img = maskLeft;
save_untouch_nii(JuelichMask,[pn.Visual])
