pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/';
pn.tools	= [pn.root, 'analyses/B4_PLS_preproc2/T_tools/'];  addpath(genpath(pn.tools));
pn.maskOut = [pn.root, 'analyses/L_V5motion/B_data/A_standards/'];
pn.probPath = '/Applications/FSL/data/atlases/Juelich/Juelich-maxprob-thr0-2mm.nii.gz';

% load probabilistic mask
probData = load_untouch_nii(pn.probPath);

%% get binary matrices of subregions

% labels wrong in Juelich file?
% 88: left V5, 89: right V5
regions = {'leftV5'; 'rightV5'};
regionsID = [89,90];

probMask = probData.img;

for indComponent = 1:numel(regions)
    probMask_c = zeros(numel(probMask),1);
    probMask_c(probMask==regionsID(indComponent)) = 1;
    probMask_c = reshape(probMask_c,size(probMask,1),...
        size(probMask,2), size(probMask,3));
    probData.img = probMask_c;
    save_untouch_nii(probData,[pn.maskOut, regions{indComponent}, '.nii'])
end
