pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/';
pn.tools	= [pn.root, 'analyses/B4_PLS_preproc2/T_tools/'];  addpath(genpath(pn.tools));
pn.maskOut = [pn.root, 'analyses/L_V5motion/B_data/A_standards/'];
pn.probPath = '/Applications/FSL/data/atlases/Juelich/Juelich-maxprob-thr0-2mm.nii.gz';

% load probabilistic mask
probData = load_untouch_nii(pn.probPath);

%% get binary matrices of subregions

% labels start at zero in the labels, +1 here
regionsID = [46:57]+1;

probMask = probData.img;
probMask_c = zeros(numel(probMask),1);
probMask_c(ismember(probMask,regionsID)) = 1;
probMask_c = reshape(probMask_c,size(probMask,1),...
    size(probMask,2), size(probMask,3));
probData.img = probMask_c;
save_untouch_nii(probData,[pn.maskOut, 'SomatoMotor_Julich.nii'])
