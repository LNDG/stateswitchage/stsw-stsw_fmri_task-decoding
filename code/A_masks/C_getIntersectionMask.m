pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/';
pn.tools	= [pn.root, 'analyses/B4_PLS_preproc2/T_tools/'];  addpath(genpath(pn.tools));

pn.JuelichMaskLeft = [pn.root, 'analyses/L_V5motion/B_data/A_standards/leftV5_thr_MNI_3mm.nii.gz'];
pn.JuelichMaskRight = [pn.root, 'analyses/L_V5motion/B_data/A_standards/rightV5_thr_MNI_3mm.nii.gz'];
pn.GMmask = [pn.root, 'analyses/L_V5motion/B_data/A_standards/mni_icbm152_nlin_sym_09c/mni_icbm152_gm_tal_nlin_sym_09c_MNI_3mm.nii.gz'];
pn.NeuroSynthMotion = [pn.root, 'analyses/L_V5motion/B_data/A_standards/visualmotion_association-test_z_FDR_0.01_thr_MNI_3mm.nii.gz'];

pn.LeftOut = [pn.root, 'analyses/L_V5motion/B_data/A_standards/leftV5_unified_thr_MNI_3mm.nii.gz'];
pn.RightOut = [pn.root, 'analyses/L_V5motion/B_data/A_standards/rightV5_unified_thr_MNI_3mm.nii.gz'];

% load masks
JuelichMaskLeft = load_untouch_nii(pn.JuelichMaskLeft);
JuelichMaskRight = load_untouch_nii(pn.JuelichMaskRight);
GMmask = load_untouch_nii(pn.GMmask);
NeuroSynthMotion = load_untouch_nii(pn.NeuroSynthMotion);

%% get binary matrices of subregions

maskLeft = JuelichMaskLeft.img.*GMmask.img.*NeuroSynthMotion.img;
maskRight = JuelichMaskRight.img.*GMmask.img.*NeuroSynthMotion.img;

JuelichMaskLeft.img = maskLeft;
save_untouch_nii(JuelichMaskLeft,[pn.LeftOut])

JuelichMaskRight.img = maskRight;
save_untouch_nii(JuelichMaskRight,[pn.RightOut])
