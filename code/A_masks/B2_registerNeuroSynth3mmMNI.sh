#!/bin/bash

regions="visualmotion_association-test_z_FDR_0.01"

#thalamic regions
BASE='/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/L_V5motion/B_data/A_standards/'
REF='/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/L_V5motion/B_data/A_standards/mni_icbm152_nlin_sym_09c/mni_icbm152_gm_tal_nlin_sym_09c_MNI_3mm.nii'

for reg in $regions; do
flirt -in ${BASE}${reg}.nii.gz -ref ${REF} -applyxfm -usesqform -out ${BASE}${reg}_thr_mask.nii.gz
fslmaths ${BASE}${reg}_thr_mask.nii.gz -thr 0.5 -bin ${BASE}${reg}_thr_MNI_3mm.nii.gz
rm ${BASE}${reg}_thr_mask.nii.gz
done