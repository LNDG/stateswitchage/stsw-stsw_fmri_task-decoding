% output is trial-wise decoding accurace of attribute-specific prevalence
% discrimination
% trained across trials from all dims, excl. test trial

clear all; clc;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..', '..'))
rootpath = pwd;

pn.data = fullfile(rootpath, 'data', 'G_DecodingResults');
    addpath(fullfile(rootpath, 'tools', 'shadedErrorBar'))
    addpath(fullfile(rootpath, 'tools', 'BrewerMap'))
pn.trlinfo = fullfile(rootpath, 'data', 'C_trialInfo');
pn.fieldtrip = fullfile(rootpath, 'tools', 'fieldtrip');
    addpath(pn.fieldtrip); ft_defaults;
pn.plotFolder = fullfile(rootpath, 'figures', 'G_decode');
    addpath(genpath(fullfile(rootpath, 'tools', 'plot2svg')))

% set custom colormap
cBrew = brewermap(2,'RdBu');
cBrew = flipud(cBrew);
colormap(cBrew)
    
% 2131, 2237 removed due to missing runs
filename = fullfile(rootpath, 'code', 'id_list_mr_completeruns.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
tmp_IDs = IDs{1};

IDs{1} = tmp_IDs(cellfun(@str2num, tmp_IDs, 'un', 1)<2000);
IDs{2} = tmp_IDs(cellfun(@str2num, tmp_IDs, 'un', 1)>2000);

DA_merged = cell(1,2);
for indAge = 1:2
    for id = 1:numel(IDs{indAge})
        ID = IDs{indAge}{id};
        curFile = fullfile(pn.data, [ID, '_h2_accuracy.mat']);
        if ~exist(curFile)
            disp(['File missing: ', ID])
            DA_merged{indAge}(id,:,:) = NaN;
            continue;
        end
        load(curFile);
        if size(DA_end,1)==128 % if only the initial two runs are available
            DA_end = cat(1, DA_end, NaN(size(DA_end)));
        end
        DA_merged{indAge}(id,:,:) = DA_end;
        % load trial assignment to split into categories of interest
        load(fullfile(pn.trlinfo, [ID,'_TrlInfo_mat.mat']))
        for indAtt = 1:4
            for indDim = 1:4
                curTrials = TrlInfo(:,6) == indAtt & TrlInfo(:,8) == indDim;
                DecTarget_probed{indAge}(id,indDim,indAtt,:) = squeeze(nanmean(DA_end(curTrials,:),1));
                % dec acc for feature x when feature x was NOT probed
                curTrials = TrlInfo(:,6) ~= indAtt & TrlInfo(:,8) == indDim;
                DecTarget_unprobed{indAge}(id,indDim,indAtt,:) = squeeze(nanmean(DA_end(curTrials,:),1));
                % on trials on which attribute x was probed, was there
                % evidence for attributes y? There should be at increasing
                % loads as attributes y have also been cued
                curTrials = TrlInfo(:,6) == indAtt & TrlInfo(:,8) == indDim;
                DecTarget_probed_other{indAge}(id,indDim,indAtt,:) = squeeze(nanmean(nanmean(DA_end(curTrials,:),1),2));
                curTrials = TrlInfo(:,indAtt) == 1 & TrlInfo(:,8) == indDim;
                DecTarget_cued{indAge}(id,indDim,indAtt,:) = squeeze(nanmean(DA_end(curTrials,:),1));
                curTrials = TrlInfo(:,indAtt) == 1 & TrlInfo(:,6) ~= indAtt & TrlInfo(:,8) == indDim;
                DecTarget_cued_unprobed{indAge}(id,indDim,indAtt,:) = NaN(1,21);
                if indDim > 1
                    DecTarget_cued_unprobed{indAge}(id,indDim,indAtt,:) = squeeze(nanmean(DA_end(curTrials,:),1));
                end
%                 curTrials = TrlInfo(:,indAtt) == 1 & TrlInfo(:,6) ~= indAtt & TrlInfo(:,8) == indDim;
%                 DecTarget_cued_unprobed{indAge}(id,indDim,indAtt,:) = squeeze(nanmean(DA_end(curTrials,indAtt,1,2,:),1));
                curTrials = isnan(TrlInfo(:,indAtt)) & TrlInfo(:,8) == indDim;
                DecTarget_uncued{indAge}(id,indDim,indAtt,:) = squeeze(nanmean(DA_end(curTrials,:),1));
                curTrials = TrlInfo(:,8) == indDim;
                DecTarget_all{indAge}(id,indDim,indAtt,:) = squeeze(nanmean(DA_end(curTrials,:),1));
            end
        end
    end
end

%% plot results

time = ([1:21]-1).*.645;

% average across features within dimensionality
for indAge = 1:2
    DA_merged_uncued{indAge} = []; DA_merged_uncued{indAge} = squeeze(nanmean(nanmean(DecTarget_uncued{indAge}(:,1:4,:,:),3),2));
    DA_merged_cued{indAge} = []; DA_merged_cued{indAge} = squeeze(nanmean(nanmean(DecTarget_cued{indAge}(:,1:4,:,:),3),2));
    DA_merged_all{indAge} = []; DA_merged_all{indAge} = squeeze(nanmean(DA_merged{indAge}(:,:,:),2));
end
DA_merged_uncued{3} = cat(1,DA_merged_uncued{1},DA_merged_uncued{2});
DA_merged_cued{3} = cat(1,DA_merged_cued{1},DA_merged_cued{2});

%% perform cluster-based permutation tests (1D) of decoding vs. chance
% 
% stat = cell(1);
% for indCued = 1:2
%     if indCued == 1
%         curData = DA_merged_cued;
%     else
%         curData = DA_merged_uncued;
%     end
%     for indGroup = 1:3
%         if indGroup==3 % across younger and older adults
%             data_forFT = cat(1,curData{1},curData{2});
%         else
%             data_forFT = cat(1,curData{indGroup});
%         end
%         decode_forFT = [];
%         for indID = 1:size(data_forFT,1)
%             decode_forFT{1,indID}.data = data_forFT(indID,:);
%             decode_forFT{1,indID}.dimord = 'chan_time';
%             decode_forFT{1,indID}.time = 1:size(data_forFT,2);
%             decode_forFT{1,indID}.label{1} = 'decoding';
%             decode_forFT{2,indID} = decode_forFT{1,indID};
%             decode_forFT{2,indID}.data(:) = 50; 
%         end
% 
%         cfgStat = [];
%         cfgStat.method           = 'montecarlo';
%         cfgStat.statistic        = 'ft_statfun_depsamplesT';
%         cfgStat.correctm         = 'cluster';
%         cfgStat.clusteralpha     = 0.05;
%         cfgStat.clusterstatistic = 'maxsum';
%         cfgStat.minnbchan        = 0;
%         cfgStat.tail             = 0;
%         cfgStat.clustertail      = 0;
%         cfgStat.alpha            = 0.025;
%         cfgStat.numrandomization = 500;
%         cfgStat.parameter        = 'data';
%         cfgStat.neighbours      = []; % no neighbors here 
% 
%         subj = size(data_forFT,1);
%         conds = 2;
%         design = zeros(2,conds*subj);
%         for indCond = 1:conds
%         for i = 1:subj
%             design(1,(indCond-1)*subj+i) = indCond;
%             design(2,(indCond-1)*subj+i) = i;
%         end
%         end
%         cfgStat.design   = design;
%         cfgStat.ivar     = 1;
%         cfgStat.uvar     = 2;
% 
%         [stat{indCued, indGroup}] = ft_timelockstatistics(cfgStat, decode_forFT{1,:}, decode_forFT{2,:});
%     end
% end

%% plot average across age groups

h = figure('units','normalized','position',[.1 .1 .15 .2]);
set(gcf,'renderer','opengl')
    cla; hold on;
    patches.timeVec = [0 5 8 12.9];
    patches.colorVec = [1 1 1;.9 .9 .9; 1 1 1; 1 .9 .9; 1 1 1; .9 .9 .9];
    for indP = 1:numel(patches.timeVec)-1
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [0 0 [100 100]], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end; hold on;
    plot([0,12.9], [25,25], 'k--', 'LineWidth',2)
    for indAge = 3
        for indCond = 1
            curData = smoothts(squeeze(eval(['DA_merged_cued{indAge}'])),'b',1);
            standError = nanstd(curData,1)./sqrt(size(curData,1));
            dimPlot{1} = shadedErrorBar(time,nanmean(curData,1),standError, ...
                'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
        end
        %ylim([49.5 52])
        xlabel('Time (TR) from stim onset'); ylabel('Decoding accuracy (%)')
        xlim([0 12.9])
    end
%     statsMask = double(stat{1,3}.mask); statsMask(statsMask==0) = NaN;
%     plot(time, 49.9.*statsMask, 'color', cBrew(1,:), 'LineWidth',4)
    % add decoding for uncued trials
    for indAge = 3
        for indCond = 1
            curData = smoothts(squeeze(eval(['DA_merged_uncued{indAge}'])),'b',1);
            standError = nanstd(curData,1)./sqrt(size(curData,1));
            dimPlot{2} = shadedErrorBar(time,nanmean(curData,1),standError, ...
                'lineprops', {'color', [.6 .6 .6],'linewidth', 2, 'linestyle', ':'}, 'patchSaturation', .25);
        end
        ylim([24 30])
        xlabel('Time (TR) from stim onset'); ylabel('Decoding accuracy (%)')
        xlim([0 12.9])
    end
%     statsMask = double(stat{2,3}.mask); statsMask(statsMask==0) = NaN;
%     plot(time, 49.8.*statsMask, 'color', [.6 .6 .6], 'LineWidth',4)
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    legend([dimPlot{1}.mainLine, dimPlot{2}.mainLine],...
            {'Cued'; 'Uncued'}, 'orientation', 'vertical', 'location', 'NorthWest')
    legend('boxoff')
% 
% figureName = 'g6d_decoding_average';
% saveas(h, fullfile(pn.plotFolder, figureName), 'png');
% saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
% 

h = figure('units','normalized','position',[.1 .1 .15 .2]);
set(gcf,'renderer','opengl')
    cla; hold on;
    patches.timeVec = [0 5 8 12.9];
    patches.colorVec = [1 1 1;.9 .9 .9; 1 1 1; 1 .9 .9; 1 1 1; .9 .9 .9];
    for indP = 1:numel(patches.timeVec)-1
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [0 0 [100 100]], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end; hold on;
    plot([0,12.9], [25,25], 'k--', 'LineWidth',2)
    for indAge = 1:2
        for indCond = 1
            curData = smoothts(squeeze(eval(['DA_merged_all{indAge}'])),'b',1);
            standError = nanstd(curData,1)./sqrt(size(curData,1));
            dimPlot{1} = shadedErrorBar(time,nanmean(curData,1),standError, ...
                'lineprops', {'color', cBrew(indAge,:),'linewidth', 2}, 'patchSaturation', .25);
        end
        ylim([24 30])
        xlabel('Time (TR) from stim onset'); ylabel('Decoding accuracy (%)')
        xlim([0 12.9])
    end
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    legend([dimPlot{1}.mainLine, dimPlot{2}.mainLine],...
            {'YA'; 'OA'}, 'orientation', 'vertical', 'location', 'NorthWest')
    legend('boxoff')