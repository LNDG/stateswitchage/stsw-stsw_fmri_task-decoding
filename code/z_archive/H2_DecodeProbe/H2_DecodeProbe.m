function H2_DecodeProbe(rootpath, id)

    % Time-resolved decoding of prevalent feature
    % single timepoint training and testing
    
    % implements single-trial testing
    
    if ismac
        id = '1117';
        currentFile = mfilename('fullpath');
        [pathstr,~,~] = fileparts(currentFile);
        cd(fullfile(pathstr,'..','..'))
        rootpath = pwd;
    end
    
    %% add paths

    pn.libsvm   = fullfile(rootpath, 'tools', 'libsvm'); addpath(genpath(pn.libsvm));
    pn.data     = fullfile(rootpath, 'data', 'B_singleTrialV5');
    pn.trlinfo  = fullfile(rootpath, 'data', 'C_trialInfo');
    pn.out      = fullfile(rootpath, 'data', 'G_DecodingResults');
    
    mex -setup
    cd(fullfile(pn.libsvm,'matlab'));
    make

    % load voxel-wise data
    load(fullfile(pn.data, [id,'_VC_allVox.mat']))
    
    % load trial info to get prevalent options
    load(fullfile(pn.trlinfo, [id,'_TrlInfo_mat.mat']))
    
    % if trials are missing: only keep initial trials
    
    Ntrials_available = size(TrialSignal,1);
    TrlInfo = TrlInfo(1:Ntrials_available,:);
    
    % prepare for the contingency that some voxels may be NaN
    TrialSignal(isnan(TrialSignal)) = 0;
    
    N_chans = size(TrialSignal,2);
    N_time = size(TrialSignal,3);
    
    numIncluded = [];
    for indDim = 1:4
        D{indDim} = NaN(4,16,N_chans,N_time); % condition*trial*channel*time matrix
        for indAtt = 1:4
            disp(num2str(indAtt));
            includedTrials = find(TrlInfo(:,6) == indAtt & TrlInfo(:,8) == indDim); % attribute is probed
            numIncluded(indDim,indAtt) = numel(includedTrials);
            for indTrial = 1:numIncluded(indDim,indAtt)
                D{indDim}(indAtt,indTrial,:,1:N_time) = TrialSignal(includedTrials(indTrial),:,:);
                % keep track of which trials we analyze (we need to always exlude the tested trial, otherwise we bias the solution)
                IncludedTrials{indDim,indAtt} = includedTrials;
            end
        end
    end
    
    %% 3) Decoding
    
    % How to create a single-trial test set here?
    % For each attribute, create a decoder trained on exemplars from all Dims
    % This decoder targets the contrast between attributes
    % Apply this trained decoder on each single trial
    % The trial in question must be excluded from the training set
    % Perform 100 permutations of training trials to bootstrap single-trial
    % testing accuracy
    
    num_permutations=100;
    totalperms = Ntrials_available*num_permutations; count = 0;
    DA=NaN(Ntrials_available,num_permutations,N_time);
    for indTrial = 1:Ntrials_available
        %for indAtt = 1:4
            for perm =1:num_permutations
                count = count+1;
                tic

                % reserve 1 random trial for each condition that will not be
                % included in training set
                
                train = cell(1,4); % training set specific to dim 1, attribute
                for indDim_s = 1:4
                    for indAtt_s = 1:4
                        trialsAvailable     = find(~isnan(D{indDim_s}(indAtt_s,:,1,1)));
                        % check if the current test trial is in the
                        % set, if so remove it!
                        curTestPosinSet = find(ismember(IncludedTrials{indDim_s,indAtt_s}(trialsAvailable),indTrial));
                        if ~isempty(curTestPosinSet)
                            trialsAvailable(curTestPosinSet) = [];
                        end
                        randomTrials        = trialsAvailable(randperm(numel(trialsAvailable)));
                        % create three folds
                        Nfold = 3;
                        Nperfold = floor(numel(trialsAvailable)/Nfold);
                        trialsTrain{1} = randomTrials(1:Nperfold);
                        trialsTrain{2} = randomTrials(Nperfold+1:2*Nperfold);
                        if indDim_s == 1
                            train{1,indAtt_s} = cat(2, train{1,indAtt_s}, ...
                                nanmean(D{indDim_s}(indAtt_s,trialsTrain{1},:,:),2),...
                                nanmean(D{indDim_s}(indAtt_s,trialsTrain{2},:,:),2));
                        end
                    end
                end
                
                % use max. available number of training trials
                
                for indAtt_s = 1:4
                	L(1,indAtt_s) = size(train{1,indAtt_s},2);
                end
                L = min(L);

                %% perform SVM classification

                for time_point =1:N_time % all time points are independent
                    % L-1 pseudo trials go to testing set, the Lth to training set
                    MEEG_training_data=[squeeze(train{1}(1,1:L,:,time_point)) ; ...
                        squeeze(train{2}(1,1:L,:,time_point)); ...
                        squeeze(train{3}(1,1:L,:,time_point)); ...
                        squeeze(train{4}(1,1:L,:,time_point))];
                    labels_train=[ones(1,L) 2*ones(1,L) 3*ones(1,L) 4*ones(1,L)];
                    MEEG_testing_data=[squeeze(TrialSignal(indTrial,:,time_point))];
                    labels_test= TrlInfo(indTrial,6);
                    % train SVM
                    model = svmtrain(labels_train', MEEG_training_data,'-s 0 -t 0 -q');
                    % test SVM
                    [predicted_label, accuracy, decision_values] = svmpredict(labels_test', MEEG_testing_data , model);
                    % accuracy will be either 0 or 100%; this
                    % should average to a more reasonable estimate
                    % due to bootstrapping
                    if ~isempty(accuracy)
                        DA(indTrial,perm,time_point)=accuracy(1);
                    else
                        DA(indTrial,perm,time_point)=NaN;
                    end
                end % time point
                ttoc = toc;
                disp(['Perm ', num2str(count), '/', num2str(totalperms), ': ', num2str(ttoc), 's'])
            end % permutation
        %end % attribute loop
    end
    
    % average across permutations
    DA_end = squeeze(nanmean(DA,2));
    DA_std = squeeze(nanstd(DA,[],2));
    
    %figure; plot(squeeze(DA_end(1,:)))
    %figure; plot(squeeze(nanmean(DA_end,1)))
   
    %% save results

    save(fullfile(pn.out, [id, '_h2_accuracy.mat']), 'DA_end', 'DA_std');
    
end % function end