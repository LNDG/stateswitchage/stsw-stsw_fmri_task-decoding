
function G2_decodeWinOption_D1(ID)

    % Time-resolved decoding of prevalent feature
    % single timepoint training and testing
    % train on D1, test on each Dim separately
    
    % TO DO:
    % adjust data input
    % implement N fold cross-validation
    
    %% add paths

    if ismac
        pn.root     = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP/L_V5motion/';
        % add libsvm toolbox
        pn.libsvm   = [pn.root, 'T_tools/libsvm-3.11/']; addpath(genpath(pn.libsvm));
    else
        pn.root     = '/home/mpib/LNDG/StateSwitch/WIP/L_V5motion/';
        pn.libsvm   = [pn.root, 'T_tools/libsvm-3.11/'];
    end
    pn.data    = [pn.root, 'B_data/B_singleTrialV5/'];
    pn.trlinfo = [pn.root, 'B_data/C_trialInfo/'];
    pn.out     = [pn.root, 'B_data/G_DecodingResults/'];

    mex -setup
    cd([pn.libsvm,'matlab/']);
    make

    % load voxel-wise data
    load([pn.data,ID,'_VC_allVox.mat'])
    
    % load trial info to get prevalent options
    load([pn.trlinfo, ID,'_TrlInfo_mat.mat'])
    
    % prepare for the contingency that some voxels may be NaN
    TrialSignal(isnan(TrialSignal)) = 0;
    
    N_chans = size(TrialSignal,2);
    N_time = size(TrialSignal,3);
    N_conditions = 2;
    
    numIncluded = [];
    indDim = 1;
    for indAtt = 1:4
        disp(num2str(indAtt));
        D{indAtt} = NaN(2,32,N_chans,N_time); % condition*trial*channel*time matrix
        curTrial = find(TrlInfo(:,6) == indAtt);% & TrlInfo(:,8) == indDim); % attribute is probed
        curTrialWinMiss = TrlInfo(curTrial,10+indAtt);
        for indOption = 1:2
            includedTrials = curTrial(curTrialWinMiss==indOption);
            numIncluded(indAtt, indOption) = numel(includedTrials);
            for indTrial = 1:numIncluded(indAtt, indOption)
                D{indAtt}(indOption,indTrial,:,1:N_time) = TrialSignal(includedTrials(indTrial),:,:);
            end
        end
    end
    
    %% 3) Decoding
    
    num_permutations=100;
    DA=NaN(4,num_permutations,N_conditions,N_conditions,N_time);
    for indAtt = 1:4
        for perm =1:num_permutations
            tic
            
            % select random subset of min. available trials across conditions in random order
            subsetAmount = min(min(numIncluded));
            for indD = unique([1, indAtt])
                permutedD{indD} = NaN(4,subsetAmount,N_chans,N_time);
                for indCond = 1:2
                    trialsAvailable          = find(~isnan(D{indD}(indCond,:,1,1)));
                    trialsSelected           = trialsAvailable(randperm(numel(trialsAvailable),subsetAmount));
                    permutedD{indD}(indCond,:,:,:) = D{indD}(indCond,trialsSelected,:,:);
                end
            end

            L = subsetAmount;

            %% perform SVM classification
            
            for condA=1:N_conditions %loop through all conditions
                for condB = condA+1:N_conditions  %loop through all conditions >condA+1
                    for time_point =1:N_time % all time points are independent
                        % L-1 pseudo trials go to testing set, the Lth to training set
                        MEEG_training_data=[squeeze(permutedD{indAtt}(condA,1:end-1,:,time_point)) ; squeeze(permutedD{indAtt}(condB,1:end-1,:,time_point))];
                        MEEG_testing_data=[squeeze(permutedD{indAtt}(condA,end,:,time_point))' ; squeeze(permutedD{indAtt}(condB,end,:,time_point))'];
                        % you have to define class labels for the trials you will train and test
                        labels_train=[ones(1,L-1) 2*ones(1,L-1)]; %they are L times 1, and L times 2 in a row (you just label any classes as 1 and 2, as the SVM at any time point only sees 2 classes
                        labels_test= [1 2]; % you will test on one trial from condA, and one from cond B
                        % train SVM
                        model = svmtrain(labels_train', MEEG_training_data,'-s 0 -t 0 -q');
                        % test SVM
                        [predicted_label, accuracy, decision_values] = svmpredict(labels_test', MEEG_testing_data , model);
                        DA(indAtt,perm,condA,condB,time_point)=accuracy(1);
                    end % time point
                end % cond A
            end %cond B
            toc
        end % permutation
    end % attribute loop
    
    % average across permutations
    DA_end = squeeze(nanmean(DA,2));
    DA_std = squeeze(nanstd(DA,[],2));
    
    %figure; plot(squeeze(DA_end(2,1,2,:)))
   
    %% save results

    save([pn.out, ID, '_DecodeWinOptionD1_VC.mat'], 'DA_end', 'DA_std');
    
end % function end