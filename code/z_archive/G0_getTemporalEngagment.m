clear all; clc;

addpath('/Volumes/Kosciessa/Tools/shadedErrorBar-7002ebc')

% N = 42 YAs
IDs{1} = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

% N = 51 OAs (2131, 2237 removed due to missing run)
IDs{2} = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

pn.root = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP/L_V5motion/';

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
cBrew = brewermap(4,'RdBu');

%% contrast this decoding result with avg. V5 engagement

V5_merged = cell(1,2);
for indAge = 1:2
    for id = 1:numel(IDs{indAge})
        curFile = [pn.root, 'B_data/B_singleTrialV5/' IDs{indAge}{id}, '_V5_allVox.mat'];
        if ~exist(curFile)
            disp(['File missing: ', IDs{indAge}{id}])
            V5_merged{indAge}(id,:) = NaN;
            continue;
        end
        load(curFile);
        V5_merged{indAge}(id,:) = squeeze(nanmean(nanmean(TrialSignal_V5,2),1));
    end
end

time = 0:.645:20*.645;

h = figure('units','normalized','position',[.1 .1 .15 .3]);
cla;
patches.timeVec = [1 6 9 13];
patches.colorVec = [1 1 1;.9 .9 .9; 1 1 1; 1 .9 .9; 1 1 1; .9 .9 .9];
for indP = 1:numel(patches.timeVec)-1
    p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                [-10 -10 [10 10]], patches.colorVec(indP,:));
    p.EdgeColor = 'none';
end; hold on;
for indAge = 1:2
    curData = smoothts(squeeze(V5_merged{indAge}),'b',1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    dimPlot{indAge} = shadedErrorBar(time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(indAge,:),'linewidth', 2}, 'patchSaturation', .25);
    ylim([-10 10])
    xlabel('Time (s) from stim onset'); ylabel('Signal change')
    xlim([0 12.9])
end
% % add canonical HRF deconvolution
% load('/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP/G_GLM/B_data/Z_hrf.mat')
% yyaxis right
% plot(time,hrfdata.stim, 'k:', 'LineWidth',1.5)
% plot(time,hrfdata.rsp, 'r:', 'LineWidth',2)
% % residual labeling
set(findall(gcf,'-property','FontSize'),'FontSize',18)
legend([dimPlot{1}.mainLine, dimPlot{2}.mainLine],...
        {'YA'; 'OA'}, 'orientation', 'horizontal', 'location', 'NorthWest')
legend('boxoff')
title({'V5 engagement';'bilateral grand avg.'})

pn.plotFolder = [pn.root, 'C_figures/G_decode/'];
figureName = 'GZ_V5_time';
saveas(h, [pn.plotFolder, figureName], 'png');

%% contrast this decoding result with avg. somatomotor engagement

SM_merged = cell(1,2);
for indAge = 1:2
    for id = 1:numel(IDs{indAge})
        curFile = [pn.root, 'B_data/B_singleTrialV5/' IDs{indAge}{id}, '_SM_allVox.mat'];
        if ~exist(curFile)
            disp(['File missing: ', IDs{indAge}{id}])
            SM_merged{indAge}(id,:) = NaN;
            continue;
        end
        load(curFile);
        SM_merged{indAge}(id,:) = squeeze(nanmean(nanmean(TrialSignal,2),1));
    end
end

time = 0:.645:20*.645;

h = figure('units','normalized','position',[.1 .1 .15 .3]);
cla;
patches.timeVec = [1 6 9 13];
patches.colorVec = [1 1 1;.9 .9 .9; 1 1 1; 1 .9 .9; 1 1 1; .9 .9 .9];
for indP = 1:numel(patches.timeVec)-1
    p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                [-5 -5 [5 5]], patches.colorVec(indP,:));
    p.EdgeColor = 'none';
end; hold on;
for indAge = 1:2
    curData = smoothts(squeeze(SM_merged{indAge}),'b',1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    dimPlot{indAge} = shadedErrorBar(time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(indAge,:),'linewidth', 2}, 'patchSaturation', .25);
    ylim([-4 4])
    xlabel('Time (s) from stim onset'); ylabel('Signal change')
    xlim([0 12.9])
end
% % add canonical HRF deconvolution
% load('/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP/G_GLM/B_data/Z_hrf.mat')
% yyaxis right
% plot(time,hrfdata.stim, 'k:', 'LineWidth',1.5)
% plot(time,hrfdata.rsp, 'r:', 'LineWidth',2)
% residual labeling
set(findall(gcf,'-property','FontSize'),'FontSize',18)
legend([dimPlot{1}.mainLine, dimPlot{2}.mainLine],...
        {'YA'; 'OA'}, 'orientation', 'horizontal', 'location', 'NorthWest')
legend('boxoff')
title({'Somatomotor engagement';'bilateral grand avg.'})

pn.plotFolder = [pn.root, 'C_figures/G_decode/'];
figureName = 'GZ_SM_time';
saveas(h, [pn.plotFolder, figureName], 'png');

%% contrast this decoding result with avg. visual cortex engagement

VC_merged = cell(1,2);
for indAge = 1:2
    for id = 1:numel(IDs{indAge})
        curFile = [pn.root, 'B_data/B_singleTrialV5/' IDs{indAge}{id}, '_VC_allVox.mat'];
        if ~exist(curFile)
            disp(['File missing: ', IDs{indAge}{id}])
            VC_merged{indAge}(id,:) = NaN;
            continue;
        end
        load(curFile);
        VC_merged{indAge}(id,:) = squeeze(nanmean(nanmean(TrialSignal,2),1));
    end
end

time = 0:.645:20*.645;

h = figure('units','normalized','position',[.1 .1 .15 .3]);
cla;
patches.timeVec = [1 6 9 13];
patches.colorVec = [1 1 1;.9 .9 .9; 1 1 1; 1 .9 .9; 1 1 1; .9 .9 .9];
for indP = 1:numel(patches.timeVec)-1
    p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                [-10 -10 [10 10]], patches.colorVec(indP,:));
    p.EdgeColor = 'none';
end; hold on;
for indAge = 1:2
    curData = smoothts(squeeze(VC_merged{indAge}),'b',1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    dimPlot{indAge} = shadedErrorBar(time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(indAge,:),'linewidth', 2}, 'patchSaturation', .25);
    ylim([-10 10])
    xlabel('Time (s) from stim onset'); ylabel('Signal change')
    xlim([0 12.9])
end
set(findall(gcf,'-property','FontSize'),'FontSize',18)
legend([dimPlot{1}.mainLine, dimPlot{2}.mainLine],...
        {'YA'; 'OA'}, 'orientation', 'horizontal', 'location', 'NorthWest')
legend('boxoff')
title({'Visual cortex engagement';'bilateral grand avg.'})

pn.plotFolder = [pn.root, 'C_figures/G_decode/'];
figureName = 'GZ_VC_time';
saveas(h, [pn.plotFolder, figureName], 'png');