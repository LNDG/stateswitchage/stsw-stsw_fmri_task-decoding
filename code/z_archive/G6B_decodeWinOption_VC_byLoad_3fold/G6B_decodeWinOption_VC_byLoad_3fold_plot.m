% plot group decoding results: attribute, trained on Dim1 data, tested on
% Dim x data

clear all; clc;

addpath('/Volumes/Kosciessa/Tools/shadedErrorBar-7002ebc')

% N = 42 YAs
IDs{1} = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

% N = 51 OAs (2131, 2237 removed due to missing run)
IDs{2} = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

pn.root = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP/L_V5motion/';
pn.data = [pn.root, 'B_data/G_DecodingResults/'];

DA_merged = cell(1,2);
for indAge = 1:2
    %indCount = 1;
    for id = 1:numel(IDs{indAge})
        curFile = [pn.data, IDs{indAge}{id}, '_G6B_DecodeWinOption_VC_byLoad_3fold.mat'];
        if ~exist(curFile)
            disp(['File missing: ', IDs{indAge}{id}])
            DA_merged{indAge}(id,:,:,:,:,:) = NaN;
            continue;
        end
        load(curFile);
        DA_merged{indAge}(id,:,:,:,:,:) = DA_end;
    end
end

%% plot results
    
time = 1:21;
%time(1) = [];

% average across features within dimensionality
for indAge = 1:2
    DA_merged_1{indAge} = []; DA_merged_1{indAge} = squeeze(nanmean(DA_merged{indAge}(:,1,:,:,:,:),3));
    DA_merged_2{indAge} = []; DA_merged_2{indAge} = squeeze(nanmean(DA_merged{indAge}(:,2,:,:,:,:),3));
    DA_merged_3{indAge} = []; DA_merged_3{indAge} = squeeze(nanmean(DA_merged{indAge}(:,3,:,:,:,:),3));
    DA_merged_4{indAge} = []; DA_merged_4{indAge} = squeeze(nanmean(DA_merged{indAge}(:,4,:,:,:,:),3));
    DA_merged_5{indAge} = []; DA_merged_5{indAge} = squeeze(nanmean(DA_merged{indAge}(:,5,:,:,:,:),3));
    DA_merged_5_womot{indAge} = []; DA_merged_5_womot{indAge} = squeeze(nanmean(DA_merged{indAge}(:,5,[1,3,4],:,:,:),3));
    DA_merged_all{indAge} = []; DA_merged_all{indAge} = squeeze(nanmean(nanmean(DA_merged{indAge}(:,1:4,:,:,:,:),3),2));
end

ageConditions = {'Young Adults'; 'Old Adults'};

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
cBrew = brewermap(4,'RdBu');

h = figure('units','normalized','position',[.1 .1 .15 .5]);
cla;
for indAge = 1:2
    subplot(2,1,indAge);
    patches.timeVec = [1 10 15 21];
    patches.colorVec = [1 1 1;.9 .9 .9; 1 1 1; 1 .9 .9; 1 1 1; .9 .9 .9];
    for indP = 1:numel(patches.timeVec)-1
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [0 0 [100 100]], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end; hold on;
    plot([0,21], [50,50], 'k--', 'LineWidth',2)
    for indCond = 1:4
        grandAverage = squeeze(nanmean(nanmean(nanmean(eval(['DA_merged_',num2str(indCond), '{indAge}']),2),3),1));
        curData = smoothts(squeeze(nanmean(nanmean(eval(['DA_merged_',num2str(indCond), '{indAge}']),2),3)),'b',1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        dimPlot{indCond} = shadedErrorBar(time,nanmean(curData,1),standError, ...
            'lineprops', {'color', cBrew(indCond,:),'linewidth', 2}, 'patchSaturation', .25);
    end
    ylim([46 58])
    title(ageConditions{indAge});
    xlabel('Time (TR) from stim onset'); ylabel('Decoding accuracy (%)')
    xlim([1 21])
end
set(findall(gcf,'-property','FontSize'),'FontSize',18)
legend([dimPlot{1}.mainLine, dimPlot{2}.mainLine, dimPlot{3}.mainLine, dimPlot{4}.mainLine],...
        {'L1'; 'L2'; 'L3'; 'L4'}, 'orientation', 'vertical', 'location', 'NorthWest')
legend('boxoff')
suptitle({'VC decoding';'prevalent option'})

%% across-feature average

cBrew = brewermap(4,'RdBu');
cBrew = cBrew([1,4],:);

h = figure('units','normalized','position',[.1 .1 .3 .3]);
subplot(1,2,1);
cla;
patches.timeVec = [1 10 15 21];
patches.colorVec = [1 1 1;.9 .9 .9; 1 1 1; 1 .9 .9; 1 1 1; .9 .9 .9];
for indP = 1:numel(patches.timeVec)-1
    p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                [0 0 [100 100]], patches.colorVec(indP,:));
    p.EdgeColor = 'none';
end; hold on;
plot([0,21], [50,50], 'k--', 'LineWidth',2)
for indAge = 1:2
    for indCond = 1
        curData = smoothts(squeeze(nanmean(nanmean(eval(['DA_merged_all{indAge}']),2),3)),'b',1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        dimPlot{indAge} = shadedErrorBar(time,nanmean(curData,1),standError, ...
            'lineprops', {'color', cBrew(indAge,:),'linewidth', 2}, 'patchSaturation', .25);
    end
    ylim([48 57])
    xlabel('Time (TR) from stim onset'); ylabel('Decoding accuracy (%)')
    xlim([1 21])
end
set(findall(gcf,'-property','FontSize'),'FontSize',18)
legend([dimPlot{1}.mainLine, dimPlot{2}.mainLine],...
        {'YA'; 'OA'}, 'orientation', 'horizontal', 'location', 'NorthWest')
legend('boxoff')
title({'Probed feature trials'})

%% plot only uncued trials
% motion deliberately excluded, see feature-specific supplement

subplot(1,2,2);
cla;
patches.timeVec = [1 10 15 21];
patches.colorVec = [1 1 1;.9 .9 .9; 1 1 1; 1 .9 .9; 1 1 1; .9 .9 .9];
for indP = 1:numel(patches.timeVec)-1
    p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                [0 0 [100 100]], patches.colorVec(indP,:));
    p.EdgeColor = 'none';
end; hold on;
plot([0,21], [50,50], 'k--', 'LineWidth',2)
for indAge = 1:2
    for indCond = 1
        curData = smoothts(squeeze(nanmean(nanmean(eval(['DA_merged_5_womot{indAge}']),2),3)),'b',1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        dimPlot{indAge} = shadedErrorBar(time,nanmean(curData,1),standError, ...
            'lineprops', {'color', cBrew(indAge,:),'linewidth', 2, 'linestyle', ':'}, 'patchSaturation', .25);
    end
    ylim([48 57])
    xlabel('Time (TR) from stim onset'); ylabel('Decoding accuracy (%)')
    xlim([1 21])
end
set(findall(gcf,'-property','FontSize'),'FontSize',18)
legend([dimPlot{1}.mainLine, dimPlot{2}.mainLine],...
        {'YA'; 'OA'}, 'orientation', 'horizontal', 'location', 'NorthWest')
legend('boxoff')
title({'Uncued feature trials'})

suptitle({'VC decoding';'prevalent option'})

pn.plotFolder = [pn.root, 'C_figures/'];
figureName = 'G6B_CuedUncued_YAOA';
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% differentiate into L1 and L4

h = figure('units','normalized','position',[.1 .1 .3 .3]);
    subplot(1,2,1)
    cla;
    patches.timeVec = [1 10 15 21];
    patches.colorVec = [1 1 1;.9 .9 .9; 1 1 1; 1 .9 .9; 1 1 1; .9 .9 .9];
    for indP = 1:numel(patches.timeVec)-1
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [0 0 [100 100]], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end; hold on;
    plot([0,21], [50,50], 'k--', 'LineWidth',2)
    for indAge = 1:2
        for indCond = 1
            curData = smoothts(squeeze(nanmean(nanmean(eval(['DA_merged_1{indAge}']),2),3)),'b',1);
            standError = nanstd(curData,1)./sqrt(size(curData,1));
            dimPlot{indAge,1} = shadedErrorBar(time,nanmean(curData,1),standError, ...
                'lineprops', {'color', cBrew(indAge,:),'linewidth', 2}, 'patchSaturation', .25);
        end
        ylim([48 58])
        xlabel('Time (TR) from stim onset'); ylabel('Decoding accuracy (%)')
        xlim([1 21])
    end
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    legend([dimPlot{1,1}.mainLine, dimPlot{2,1}.mainLine],...
            {'YA'; 'OA'}, 'orientation', 'horizontal', 'location', 'NorthWest')
    legend('boxoff')
    title({'VC decoding';'Load 1'})
subplot(1,2,2)
    cla;
    patches.timeVec = [1 10 15 21];
    patches.colorVec = [1 1 1;.9 .9 .9; 1 1 1; 1 .9 .9; 1 1 1; .9 .9 .9];
    for indP = 1:numel(patches.timeVec)-1
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [0 0 [100 100]], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end; hold on;
    plot([0,21], [50,50], 'k--', 'LineWidth',2)
    for indAge = 1:2
        for indCond = 1
            curData = smoothts(squeeze(nanmean(nanmean(eval(['DA_merged_4{indAge}']),2),3)),'b',1);
            standError = nanstd(curData,1)./sqrt(size(curData,1));
            dimPlot{indAge,2} = shadedErrorBar(time,nanmean(curData,1),standError, ...
                'lineprops', {'color', cBrew(indAge,:),'linewidth', 2, 'linestyle', '--'}, 'patchSaturation', .25);
        end
        ylim([48 58])
        xlabel('Time (TR) from stim onset'); ylabel('Decoding accuracy (%)')
        xlim([1 21])
    end
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    legend([dimPlot{1,2}.mainLine, dimPlot{2,2}.mainLine],...
            {'YA'; 'OA'}, 'orientation', 'horizontal', 'location', 'NorthWest')
    legend('boxoff')
    title({'VC decoding';'Load 4'})


%% load with within-SEM

h = figure('units','normalized','position',[.1 .1 .3 .3]);
    subplot(1,2,1)
    cla;
    patches.timeVec = [1 10 15 21];
    patches.colorVec = [1 1 1;.9 .9 .9; 1 1 1; 1 .9 .9; 1 1 1; .9 .9 .9];
    for indP = 1:numel(patches.timeVec)-1
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [0 0 [100 100]], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end; hold on;
    plot([0,21], [50,50], 'k--', 'LineWidth',2)
    grandAverage = smoothts(squeeze(nanmean(nanmean(eval(['DA_merged_all{1}']),2),3)),'b',1);
    curData = smoothts(squeeze(nanmean(nanmean(eval(['DA_merged_1{1}']),2),3)),'b',1);
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    dimPlot{1,1} = shadedErrorBar(time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
    curData = smoothts(squeeze(nanmean(nanmean(eval(['DA_merged_4{1}']),2),3)),'b',1);
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    dimPlot{2,1} = shadedErrorBar(time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(1,:),'linewidth', 2, 'linestyle', '--'}, 'patchSaturation', .25);
    ylim([46 58])
    xlabel('Time (TR) from stim onset'); ylabel('Decoding accuracy (%)')
    xlim([1 21])
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    legend([dimPlot{1,1}.mainLine, dimPlot{2,1}.mainLine],...
            {'L1'; 'L4'}, 'orientation', 'horizontal', 'location', 'NorthWest')
    legend('boxoff')
    title({'VC decoding';'YA'})
subplot(1,2,2)
    cla;
    patches.timeVec = [1 10 15 21];
    patches.colorVec = [1 1 1;.9 .9 .9; 1 1 1; 1 .9 .9; 1 1 1; .9 .9 .9];
    for indP = 1:numel(patches.timeVec)-1
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [0 0 [100 100]], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end; hold on;
    plot([0,21], [50,50], 'k--', 'LineWidth',2)
    grandAverage = smoothts(squeeze(nanmean(nanmean(eval(['DA_merged_all{2}']),2),3)),'b',1);
    curData = smoothts(squeeze(nanmean(nanmean(eval(['DA_merged_1{2}']),2),3)),'b',1);
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    dimPlot{1,2} = shadedErrorBar(time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(2,:),'linewidth', 2}, 'patchSaturation', .25);
    curData = smoothts(squeeze(nanmean(nanmean(eval(['DA_merged_4{2}']),2),3)),'b',1);
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    dimPlot{2,2} = shadedErrorBar(time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(2,:),'linewidth', 2, 'linestyle', '--'}, 'patchSaturation', .25);
    ylim([46 58])
    xlabel('Time (TR) from stim onset'); ylabel('Decoding accuracy (%)')
    xlim([1 21])
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    legend([dimPlot{1,2}.mainLine, dimPlot{2,2}.mainLine],...
            {'L1'; 'L4'}, 'orientation', 'horizontal', 'location', 'NorthWest')
    legend('boxoff')
    title({'VC decoding';'OA'})
    suptitle('within-subject SEM')
    
pn.plotFolder = [pn.root, 'C_figures/'];
figureName = 'G6B_ByLoad_YAOA';
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');
    
%% plot results as a function of feature 
    
time = 1:21;
%time(1) = [];

% average across features within dimensionality
for indAge = 1:2
    DA_merged_1{indAge} = []; DA_merged_1{indAge} = squeeze(nanmean(DA_merged{indAge}(:,1:4,1,:,:,:),2));
    DA_merged_2{indAge} = []; DA_merged_2{indAge} = squeeze(nanmean(DA_merged{indAge}(:,1:4,2,:,:,:),2));
    DA_merged_3{indAge} = []; DA_merged_3{indAge} = squeeze(nanmean(DA_merged{indAge}(:,1:4,3,:,:,:),2));
    DA_merged_4{indAge} = []; DA_merged_4{indAge} = squeeze(nanmean(DA_merged{indAge}(:,1:4,4,:,:,:),2));
    DA_merged_all{indAge} = []; DA_merged_all{indAge} = squeeze(nanmean(nanmean(DA_merged{indAge}(:,1:4,:,:,:,:),3),2));
end

features = {'color', 'motion', 'size', 'luminance'};

h = figure('units','normalized','position',[.1 .1 .5 .3]);
cla;
for indAge = 1:2
    for indCond = 1:4
        subplot(2,4,(indAge-1)*4+indCond);
        patches.timeVec = [1 10 15 21];
        patches.colorVec = [1 1 1;.9 .9 .9; 1 1 1; 1 .9 .9; 1 1 1; .9 .9 .9];
        for indP = 1:numel(patches.timeVec)-1
            p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                        [0 0 [100 100]], patches.colorVec(indP,:));
            p.EdgeColor = 'none';
        end; hold on;
        plot([0,21], [50,50], 'k--', 'LineWidth',2)
        grandAverage = squeeze(nanmean(nanmean(nanmean(eval(['DA_merged_',num2str(indCond), '{indAge}']),2),3),1));
        curData = smoothts(squeeze(nanmean(nanmean(eval(['DA_merged_',num2str(indCond), '{indAge}']),2),3)),'b',1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        dimPlot{indCond} = shadedErrorBar(time,nanmean(curData,1),standError, ...
            'lineprops', {'color', cBrew(indCond,:),'linewidth', 2}, 'patchSaturation', .25);
        ylim([45 60])
        title(features{indCond});
        xlabel('Time (TR) from stim onset'); ylabel('Decoding accuracy (%)')
        xlim([1 21])
    end
end
suptitle('Cued trials, avg. across dims')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% plot decoding for each feature, onyl for uncued trials

% average across features within dimensionality
for indAge = 1:2
    DA_merged_1{indAge} = []; DA_merged_1{indAge} = squeeze(nanmean(DA_merged{indAge}(:,5,1,:,:,:),2));
    DA_merged_2{indAge} = []; DA_merged_2{indAge} = squeeze(nanmean(DA_merged{indAge}(:,5,2,:,:,:),2));
    DA_merged_3{indAge} = []; DA_merged_3{indAge} = squeeze(nanmean(DA_merged{indAge}(:,5,3,:,:,:),2));
    DA_merged_4{indAge} = []; DA_merged_4{indAge} = squeeze(nanmean(DA_merged{indAge}(:,5,4,:,:,:),2));
    DA_merged_all{indAge} = []; DA_merged_all{indAge} = squeeze(nanmean(nanmean(DA_merged{indAge}(:,5,:,:,:,:),3),2));
end

features = {'color', 'motion', 'size', 'luminance'};

h = figure('units','normalized','position',[.1 .1 .5 .3]);
cla;
for indAge = 1:2
    for indCond = 1:4
        subplot(2,4,(indAge-1)*4+indCond);
        patches.timeVec = [1 10 15 21];
        patches.colorVec = [1 1 1;.9 .9 .9; 1 1 1; 1 .9 .9; 1 1 1; .9 .9 .9];
        for indP = 1:numel(patches.timeVec)-1
            p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                        [0 0 [100 100]], patches.colorVec(indP,:));
            p.EdgeColor = 'none';
        end; hold on;
        plot([0,21], [50,50], 'k--', 'LineWidth',2)
        grandAverage = squeeze(nanmean(nanmean(nanmean(eval(['DA_merged_',num2str(indCond), '{indAge}']),2),3),1));
        curData = smoothts(squeeze(nanmean(nanmean(eval(['DA_merged_',num2str(indCond), '{indAge}']),2),3)),'b',1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        dimPlot{indCond} = shadedErrorBar(time,nanmean(curData,1),standError, ...
            'lineprops', {'color', cBrew(indCond,:),'linewidth', 2}, 'patchSaturation', .25);
        ylim([45 60])
        title(features{indCond});
        xlabel('Time (TR) from stim onset'); ylabel('Decoding accuracy (%)')
        xlim([1 21])
    end
end
suptitle('Uncued trials')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% median split based on drift rates

cBrew = brewermap(4,'RdBu');

% average across features within dimensionality
for indAge = 1:2
    DA_merged_1{indAge} = []; DA_merged_1{indAge} = squeeze(nanmean(DA_merged{indAge}(:,1,:,:,:,:),3));
    DA_merged_2{indAge} = []; DA_merged_2{indAge} = squeeze(nanmean(DA_merged{indAge}(:,2,:,:,:,:),3));
    DA_merged_3{indAge} = []; DA_merged_3{indAge} = squeeze(nanmean(DA_merged{indAge}(:,3,:,:,:,:),3));
    DA_merged_4{indAge} = []; DA_merged_4{indAge} = squeeze(nanmean(DA_merged{indAge}(:,4,:,:,:,:),3));
    DA_merged_5{indAge} = []; DA_merged_5{indAge} = squeeze(nanmean(DA_merged{indAge}(:,5,:,:,:,:),3));
    DA_merged_all{indAge} = []; DA_merged_all{indAge} = squeeze(nanmean(nanmean(DA_merged{indAge}(:,1:4,:,:,:,:),3),2));
end

pn.dataOut = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/';
load([pn.dataOut, 'STSWD_summary.mat'], 'STSWD_summary')

idx_YA = ismember(STSWD_summary.IDs, IDs{1});

drift = squeeze(nanmean(cat(2,STSWD_summary.HDDM_vt.driftEEG(idx_YA,1),STSWD_summary.HDDM_vt.driftMRI(idx_YA,1)),2));
[sortVal, sortIdx] = sort(drift,'descend');

idx{1} = sortIdx(1:ceil(numel(sortIdx)/3));
idx{2} = sortIdx(ceil(numel(sortIdx)/3)+1:2*ceil(numel(sortIdx)/3));
idx{3} = sortIdx(2*ceil(numel(sortIdx)/3)+1:end);

h = figure('units','normalized','position',[.1 .1 .15 .3]);
cla;
patches.timeVec = [1 10 15 21];
patches.colorVec = [1 1 1;.9 .9 .9; 1 1 1; 1 .9 .9; 1 1 1; .9 .9 .9];
for indP = 1:numel(patches.timeVec)-1
    p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                [0 0 [100 100]], patches.colorVec(indP,:));
    p.EdgeColor = 'none';
end; hold on;
plot([0,21], [50,50], 'k--', 'LineWidth',2)
for indAge = 1%:2
    for indCond = 1:3
        curData = smoothts(squeeze(nanmean(nanmean(eval(['DA_merged_all{indAge}(idx{indCond},:,:,:)']),2),3)),'b',1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        dimPlot{indCond} = shadedErrorBar(time,nanmean(curData,1),standError, ...
            'lineprops', {'color', cBrew(indCond,:),'linewidth', 2}, 'patchSaturation', .25);
    end
    ylim([48 58])
    xlabel('Time (TR) from stim onset'); ylabel('Decoding accuracy (%)')
    xlim([1 21])
end
set(findall(gcf,'-property','FontSize'),'FontSize',18)
legend([dimPlot{1}.mainLine, dimPlot{2}.mainLine, dimPlot{3}.mainLine],...
        {'YA high drift'; 'YA mid drift';'YA low drift'}, 'orientation', 'vertical', 'location', 'NorthWest')
legend('boxoff')
title({'VC decoding';'prevalent option'})

%% plot the same for OAs

pn.dataOut = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/';
load([pn.dataOut, 'STSWD_summary.mat'], 'STSWD_summary')

idx_OA = ismember(STSWD_summary.IDs, IDs{2});

drift = squeeze(nanmean(cat(2,STSWD_summary.HDDM_vt.driftEEG(idx_OA,1),STSWD_summary.HDDM_vt.driftMRI(idx_OA,1)),2));
[sortVal, sortIdx] = sort(drift,'descend');

idx{1} = sortIdx(1:ceil(numel(sortIdx)/3));
idx{2} = sortIdx(ceil(numel(sortIdx)/3)+1:2*ceil(numel(sortIdx)/3));
idx{3} = sortIdx(2*ceil(numel(sortIdx)/3)+1:end);

h = figure('units','normalized','position',[.1 .1 .15 .3]);
cla;
patches.timeVec = [1 10 15 21];
patches.colorVec = [1 1 1;.9 .9 .9; 1 1 1; 1 .9 .9; 1 1 1; .9 .9 .9];
for indP = 1:numel(patches.timeVec)-1
    p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                [0 0 [100 100]], patches.colorVec(indP,:));
    p.EdgeColor = 'none';
end; hold on;
plot([0,21], [50,50], 'k--', 'LineWidth',2)
for indAge = 2
    for indCond = [1:3]
        curData = smoothts(squeeze(nanmean(nanmean(eval(['DA_merged_all{indAge}(idx{indCond},:,:,:)']),2),3)),'b',1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        dimPlot{indCond} = shadedErrorBar(time,nanmean(curData,1),standError, ...
            'lineprops', {'color', cBrew(indCond,:),'linewidth', 2}, 'patchSaturation', .25);
    end
    ylim([48 58])
    xlabel('Time (TR) from stim onset'); ylabel('Decoding accuracy (%)')
    xlim([1 21])
end
set(findall(gcf,'-property','FontSize'),'FontSize',18)
legend([dimPlot{1}.mainLine, dimPlot{2}.mainLine, dimPlot{3}.mainLine],...
        {'OA high drift'; 'OA mid drift';'OA low drift'}, 'orientation', 'vertical', 'location', 'NorthWest')
legend('boxoff')
title({'VC decoding';'prevalent option'})

%% plot decoding by feature, with L1 and L4 exemplars

% average across features within dimensionality
for indAge = 1:2
    for indFeature = 1:4
        for indDim = 1:4
            DA_merged_byDim{indAge,indDim,indFeature} = []; 
            DA_merged_byDim{indAge,indDim,indFeature} = squeeze(nanmean(DA_merged{indAge}(:,indDim,indFeature,:,:,:),2));
        end
        DA_merged_all{indAge,indFeature} = []; 
        DA_merged_all{indAge,indFeature} = squeeze(nanmean(DA_merged{indAge}(:,1:4,indFeature,:,:,:),2));
    end
end
features = {'color', 'motion', 'size', 'luminance'};

h = figure('units','normalized','position',[.1 .1 .5 .3]);
cla;
for indAge = 1:2
    for indCond = 1:4
        subplot(2,4,(indAge-1)*4+indCond);
        patches.timeVec = [1 10 15 21];
        patches.colorVec = [1 1 1;.9 .9 .9; 1 1 1; 1 .9 .9; 1 1 1; .9 .9 .9];
        for indP = 1:numel(patches.timeVec)-1
            p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                        [0 0 [100 100]], patches.colorVec(indP,:));
            p.EdgeColor = 'none';
        end; hold on;
        plot([0,21], [50,50], 'k--', 'LineWidth',2)
        grandAverage = squeeze(nanmean(nanmean(nanmean(DA_merged_all{indAge,indCond},2),3),1));
        curData = smoothts(squeeze(nanmean(nanmean(DA_merged_all{indAge,indCond},2),3)),'b',1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        dimPlot{indCond} = shadedErrorBar(time,nanmean(curData,1),standError, ...
            'lineprops', {'color', [.2 .2 .2],'linewidth', 2}, 'patchSaturation', .25);
        %% add on L1 and L4
        curData = smoothts(squeeze(nanmean(nanmean(DA_merged_byDim{indAge,1,indCond},2),3)),'b',1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        dimPlot{indCond} = shadedErrorBar(time,nanmean(curData,1),standError, ...
            'lineprops', {'color', cBrew(indCond,:),'linewidth', 2}, 'patchSaturation', .25);
        
        curData = smoothts(squeeze(nanmean(nanmean(DA_merged_byDim{indAge,4,indCond},2),3)),'b',1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        dimPlot{indCond} = shadedErrorBar(time,nanmean(curData,1),standError, ...
            'lineprops', {'color', cBrew(indCond,:),'linewidth', 2, 'linestyle', ':'}, 'patchSaturation', .25);
        
        ylim([45 65])
        title(features{indCond});
        xlabel('Time (TR) from stim onset'); ylabel('Decoding accuracy (%)')
        xlim([1 21])
    end
end
set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% export to R

LongStruct = [];
ages = {'YA'; 'OA'};
features = {'Color'; 'Direction'; 'Size'; 'Luminance'};
for indAge = 1:2
    for indID = 1:size(DA_merged{indAge},1)
       if indAge ==2
            curID = size(DA_merged{1},1)+indID;
       else curID = indID;
       end
        for indFeat = 1:4
            for indDim = 1:4
                for indTime = 1:21
                    curData = squeeze(nanmean(nanmean(nanmean(DA_merged{indAge}(indID,indDim,indFeat,:,:,indTime),5),4),3));
                    LongStruct = cat(1,LongStruct,...
                        [ages(indAge), curID, features(indFeat), indDim, indTime, curData]);
                end
            end
        end
    end
end
header = {'age', 'ID', 'feature', 'dim', 'time', 'dec_acc'};

LongStruct = [header; LongStruct];

save('/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP/L_V5motion/B_data/G_DecodingResults/G6B_decode4R.mat', 'LongStruct')
