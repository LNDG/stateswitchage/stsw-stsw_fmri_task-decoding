function H3_DecodeProbe(rootpath, id)

    % Time-resolved decoding of prevalent feature
    % single timepoint training and testing
    
    % implements single-trial testing
    
    if ismac
        id = '1117';
        currentFile = mfilename('fullpath');
        [pathstr,~,~] = fileparts(currentFile);
        cd(fullfile(pathstr,'..','..'))
        rootpath = pwd;
    end
    
    %% add paths

    pn.libsvm   = fullfile(rootpath, 'tools', 'libsvm'); addpath(genpath(pn.libsvm));
    pn.data     = fullfile(rootpath, 'data', 'B_singleTrialV5');
    pn.trlinfo  = fullfile(rootpath, 'data', 'C_trialInfo');
    pn.out      = fullfile(rootpath, 'data', 'G_DecodingResults');
    
    mex -setup
    cd(fullfile(pn.libsvm,'matlab'));
    make

    % load voxel-wise data
    load(fullfile(pn.data, [id,'_VC_allVox.mat']))
    
    % load trial info to get prevalent options
    load(fullfile(pn.trlinfo, [id,'_TrlInfo_mat.mat']))
    
    % if trials are missing: only keep initial trials
    
    Ntrials_available = size(TrialSignal,1);
    TrlInfo = TrlInfo(1:Ntrials_available,:);
    
    % prepare for the contingency that some voxels may be NaN
    TrialSignal(isnan(TrialSignal)) = 0;
    
    N_chans = size(TrialSignal,2);
    N_time = size(TrialSignal,3);
    
    numIncluded = [];
    for indDim = 1:4
        D{indDim} = NaN(4,16,N_chans,N_time); % condition*trial*channel*time matrix
        for indAtt = 1:4
            disp(num2str(indAtt));
            includedTrials = find(TrlInfo(:,6) == indAtt & TrlInfo(:,8) == indDim); % attribute is probed
            numIncluded(indDim,indAtt) = numel(includedTrials);
            for indTrial = 1:numIncluded(indDim,indAtt)
                D{indDim}(indAtt,indTrial,:,1:N_time) = TrialSignal(includedTrials(indTrial),:,:);
                % keep track of which trials we analyze (we need to always exlude the tested trial, otherwise we bias the solution)
                IncludedTrials{indDim,indAtt} = includedTrials;
            end
        end
    end
    
    %% 3) Decoding
    
    % How to create a single-trial test set here?
    % For each attribute, create a decoder trained on exemplars from all Dims
    % This decoder targets the contrast between attributes
    % Apply this trained decoder on each single trial
    % The trial in question must be excluded from the training set
    % Perform 100 permutations of training trials to bootstrap single-trial
    % testing accuracy
    
    num_permutations=100;
    DA=NaN(4,num_permutations,N_time);
    for indDim = 1:4
        for perm =1:num_permutations
            tic
            
            % select random subset of min. available trials across conditions in random order
            subsetAmount = min(min(numIncluded));
            for indD = unique([1, indDim])
                permutedD{indD} = NaN(4,subsetAmount,N_chans,N_time);
                for indCond = 1:4
                    trialsAvailable          = find(~isnan(D{indD}(indCond,:,1,1)));
                    trialsSelected           = trialsAvailable(randperm(numel(trialsAvailable),subsetAmount));
                    permutedD{indD}(indCond,:,:,:)      = D{indD}(indCond,trialsSelected,:,:);
                end
            end

            %% bin data into L=N/K pseudo trials
            % Not done here due to low N.

            L = subsetAmount;

            %% perform SVM classification
            
            for time_point =1:N_time % all time points are independent
                % L-1 pseudo trials go to testing set, the Lth to training set
                MEEG_training_data=[squeeze(permutedD{1}(1,1:end-1,:,time_point)) ; ...
                    squeeze(permutedD{1}(2,1:end-1,:,time_point)); ...
                    squeeze(permutedD{1}(3,1:end-1,:,time_point)); ...
                    squeeze(permutedD{1}(4,1:end-1,:,time_point))];
                MEEG_testing_data=[squeeze(permutedD{indDim}(1,end,:,time_point))' ; ...
                    squeeze(permutedD{indDim}(2,end,:,time_point))'; ...
                    squeeze(permutedD{indDim}(3,end,:,time_point))'; ...
                    squeeze(permutedD{indDim}(4,end,:,time_point))'];
                % you have to define class labels for the trials you will train and test
                labels_train=[ones(1,L-1) 2*ones(1,L-1) 3*ones(1,L-1) 4*ones(1,L-1)]; %they are L times 1, and L times 2 in a row (you just label any classes as 1 and 2, as the SVM at any time point only sees 2 classes
                labels_test= [1 2 3 4]; % you will test on one trial from condA, and one from cond B
                % train SVM
                model = svmtrain(labels_train', MEEG_training_data,'-s 0 -t 0 -q');
                % test SVM
                [predicted_label, accuracy, decision_values] = svmpredict(labels_test', MEEG_testing_data , model);
                DA(indDim,perm,time_point)=accuracy(1);
            end % time point
            toc
        end % permutation

    end % target dimensionality loop
    
    % average across permutations
    DA_end = squeeze(mean(DA,2));
    DA_std = squeeze(nanstd(DA,[],2));
    
    % figure; plot(squeeze(DA_end(1,:)))

    %% save results

    save(fullfile(pn.out, [id, '_h4_accuracy.mat']), 'DA_end', 'DA_std');
    
end % function end