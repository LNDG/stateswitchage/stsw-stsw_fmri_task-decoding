% output is trial-wise decoding accurace of attribute-specific prevalence
% discrimination
% trained across trials from all dims, excl. test trial

clear all; clc;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..', '..'))
rootpath = pwd;

pn.data = fullfile(rootpath, 'data', 'G_DecodingResults');
    addpath(fullfile(rootpath, 'tools', 'shadedErrorBar'))
    addpath(fullfile(rootpath, 'tools', 'BrewerMap'))
pn.trlinfo = fullfile(rootpath, 'data', 'C_trialInfo');
pn.fieldtrip = fullfile(rootpath, 'tools', 'fieldtrip');
    addpath(pn.fieldtrip); ft_defaults;
pn.plotFolder = fullfile(rootpath, 'figures', 'G_decode');
    addpath(genpath(fullfile(rootpath, 'tools', 'plot2svg')))

% 2131, 2237 removed due to missing runs
filename = fullfile(rootpath, 'code', 'id_list_mr_completeruns.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
tmp_IDs = IDs{1};

IDs{1} = tmp_IDs(cellfun(@str2num, tmp_IDs, 'un', 1)<2000);
IDs{2} = tmp_IDs(cellfun(@str2num, tmp_IDs, 'un', 1)>2000);

DA_merged = cell(1,2);
for indAge = 1:2
    for id = 1:numel(IDs{indAge})
        ID = IDs{indAge}{id};
        curFile = fullfile(pn.data, [ID, '_h4_accuracy.mat']);
        if ~exist(curFile)
            disp(['File missing: ', ID])
            DA_merged{indAge}(id,:,:) = NaN;
            continue;
        end
        load(curFile);
        if size(DA_end,1)==128 % if only the initial two runs are available
            DA_end = cat(1, DA_end, NaN(size(DA_end)));
        end
        DA_merged{indAge}(id,:,:) = DA_end;
    end
end

% set custom colormap
cBrew = brewermap(4,'RdBu');
cBrew = flipud(cBrew);
colormap(cBrew)

%% plot results

time = ([1:21]-1).*.645;

%% plot average across age groups

h = figure('units','normalized','position',[.1 .1 .3 .2]);
set(gcf,'renderer','opengl')
for indAge = 1:2
    subplot(1,2,indAge); cla; hold on;
    patches.timeVec = [0 5 8 12.9];
    patches.colorVec = [1 1 1;.9 .9 .9; 1 1 1; 1 .9 .9; 1 1 1; .9 .9 .9];
    for indP = 1:numel(patches.timeVec)-1
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [0 0 [100 100]], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end; hold on;
    plot([0,12.9], [25,25], 'k--', 'LineWidth',2)
    for indCond = 1:4
        curData = smoothts(squeeze(eval(['DA_merged{indAge}(:,indCond,:)'])),'b',1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        dimPlot{1} = shadedErrorBar(time,nanmean(curData,1),standError, ...
            'lineprops', {'color', cBrew(indCond,:),'linewidth', 2}, 'patchSaturation', .25);
    end
    ylim([23 40])
    xlabel('Time (TR) from stim onset'); ylabel('Decoding accuracy (%)')
    xlim([0 12.9])
 end
set(findall(gcf,'-property','FontSize'),'FontSize',18)
