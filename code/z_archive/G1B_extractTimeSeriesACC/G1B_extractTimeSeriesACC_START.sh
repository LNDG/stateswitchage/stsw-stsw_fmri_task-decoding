#!/bin/bash

fun_name="G1B_extractTimeSeriesACC"
job_name="stsw_accvoxels"

rootpath="$(pwd)/../.."
rootpath=$(builtin cd $rootpath; pwd)

mkdir ${rootpath}/log

# path to the text file with all subject ids:
path_ids="${rootpath}/code/id_list_mr_completeruns.txt"
# read subject ids from the list of the text file
IDS=$(cat ${path_ids} | tr '\n' ' ')

for subj in ${IDS}; do
	sbatch \
  		--job-name ${job_name}_${subj} \
  		--cpus-per-task 1 \
  		--mem 8gb \
  		--time 01:00:00 \
  		--output ${rootpath}/log/${job_name}_${subj}.out \
  		--workdir . \
  		--wrap=". /etc/profile ; module load matlab/R2020a; matlab -nodisplay -r \"${fun_name}('${rootpath}','${subj}')\""
done