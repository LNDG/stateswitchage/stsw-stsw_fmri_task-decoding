% Create an overview plot featuring the results of the multivariate PLS
% comparing spectral changes during the stimulus period under load

clear all; cla; clc;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.data = fullfile(rootpath, 'data', 'G_DecodingResults');
    addpath(fullfile(rootpath, 'tools', 'shadedErrorBar'))
    addpath(fullfile(rootpath, 'tools', 'BrewerMap'))
    addpath(genpath(fullfile(rootpath, 'tools', 'RainCloudPlots')))

% set custom colormap
cBrew = brewermap(500,'RdBu');
cBrew = flipud(cBrew);
colormap(cBrew)

load(fullfile(pn.data, 'g6d_pls_unprobed.mat'), 'stat', 'result', 'lvdat', 'lv_evt_list')

result.perm_result.sprob

indLV = 1;

lvdat = reshape(result.boot_result.compare_u(:,indLV), ...
    size(stat.prob,1), size(stat.prob,2), size(stat.prob,3));
stat.prob = lvdat;
stat.mask = lvdat > 2 | lvdat < -2;

%% invert solution

% stat.mask = stat.mask;
% stat.prob = stat.prob.*-1;
% result.vsc = result.vsc.*-1;
% result.usc = result.usc.*-1;

%% plot

h = figure('units','centimeter','position',[0 0 15 10]);
set(gcf,'renderer','Painters')
statsPlot = stat.prob;
plot(statsPlot, 'k')
xlabel('Time [s from stim onset]'); ylabel('BSR');
title({'Loadings'; ['p = ', num2str(round(result.perm_result.sprob(indLV),4))]})
set(findall(gcf,'-property','FontSize'),'FontSize',18)
figureName = ['e01_pls_traces'];

%% plot loadings

indLV = 1;

groupsizes=result.num_subj_lst;
conditions=lv_evt_list;
conds = {'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'};
condData = []; uData = [];
for indGroup = 1:2
    if indGroup == 1
        relevantEntries = 1:groupsizes(1)*numel(conds);
    elseif indGroup == 2
        relevantEntries = groupsizes(1)*numel(conds)+1:...
             groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
    end
    for indCond = 1:numel(conds)
        targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
        condData{indGroup}(indCond,:) = result.vsc(targetEntries,indLV);
        uData{indGroup}(indCond,:) = result.usc(targetEntries,indLV);
    end
end

cBrew(1,:) = 2.*[.3 .1 .1];
cBrew(2,:) = 2.*[.3 .1 .1];

idx_outlier = cell(1); idx_standard = cell(1);
for indGroup = 1:2
    dataToPlot = uData{indGroup}';
    % define outlier as lin. modulation that is more than three scaled median absolute deviations (MAD) away from the median
    X = [1 1; 1 2; 1 3; 1 4]; b=X\dataToPlot'; IndividualSlopes = b(2,:);
    outliers = isoutlier(IndividualSlopes, 'median');
    idx_outlier{indGroup} = find(outliers);
    idx_standard{indGroup} = find(outliers==0);
end

h = figure('units','centimeter','position',[0 0 25 10]);
for indGroup = 1:2
    dataToPlot = uData{indGroup}';
    % read into cell array of the appropriate dimensions
    data = []; data_ws = [];
    for i = 1:4
        for j = 1:1
            data{i, j} = dataToPlot(:,i);
            % individually demean for within-subject visualization
            data_ws{i, j} = dataToPlot(:,i)-...
                nanmean(dataToPlot(:,:),2)+...
                repmat(nanmean(nanmean(dataToPlot(:,:),2),1),size(dataToPlot(:,:),1),1);
            data_nooutlier{i, j} = data{i, j};
            data_nooutlier{i, j}(idx_outlier{indGroup}) = [];
            data_ws_nooutlier{i, j} = data_ws{i, j};
            data_ws_nooutlier{i, j}(idx_outlier{indGroup}) = [];
            % sort outliers to back in original data for improved plot overlap
            data_ws{i, j} = [data_ws{i, j}(idx_standard{indGroup}); data_ws{i, j}(idx_outlier{indGroup})];
        end
    end

    % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

    subplot(1,2,indGroup);
    set(gcf,'renderer','Painters')
        cla;
        cl = cBrew(indGroup,:);
        rm_raincloud_fixedSpacing(data_ws, [.8 .8 .8],1,[],[],[],15);
        h_rc = rm_raincloud_fixedSpacing(data_ws_nooutlier, cl,1,[],[],[],15);
        view([90 -90]);
        axis ij
    box(gca,'off')
    set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
    yticks = get(gca, 'ytick'); ylim([yticks(1)-(yticks(2)-yticks(1))./2, yticks(4)+(yticks(2)-yticks(1))./2]);
    minmax = [min(min(cat(2,data_ws{:}))), max(max(cat(2,data_ws{:})))];
    xlim(minmax+[-0.2*diff(minmax), 0.2*diff(minmax)])
    ylabel('Target load'); xlabel({'Brainscore'; '[Individually centered]'})

    % test linear effect
    curData = [data_nooutlier{1, 1}, data_nooutlier{2, 1}, data_nooutlier{3, 1}, data_nooutlier{4, 1}];
    X = [1 1; 1 2; 1 3; 1 4]; b=X\curData'; IndividualSlopes = b(2,:);
    [~, p, ci, stats] = ttest(IndividualSlopes);
    title(['M:', num2str(round(mean(IndividualSlopes),3)), '; p=', num2str(round(p,3))])
end
set(findall(gcf,'-property','FontSize'),'FontSize',18)
figureName = ['g01_pls_rcp'];

%% save individual brainscores & lin. modulation

% 2131, 2237 removed due to missing runs
filename = fullfile(rootpath, 'code', 'id_list_mr_completeruns.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
IDs = IDs{1};

LV1.data = cat(1,uData{1}',uData{2}');
X = [1 1; 1 2; 1 3; 1 4]; b=X\LV1.data'; 
LV1.linear = b(2,:);
LV1.IDs = IDs;

save(fullfile(pn.data, 'g6d_unprobed_LV1.mat'), 'LV1')
