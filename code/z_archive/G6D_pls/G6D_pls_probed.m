clear all; clc;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.data = fullfile(rootpath, 'data', 'G_DecodingResults');
    addpath(fullfile(rootpath, 'tools', 'shadedErrorBar'))
    addpath(fullfile(rootpath, 'tools', 'BrewerMap'))
    addpath(genpath(fullfile(rootpath, 'tools', 'RainCloudPlots')))
pn.trlinfo = fullfile(rootpath, 'data', 'C_trialInfo');
% pn.fieldtrip = fullfile(rootpath, 'tools', 'fieldtrip');
%     addpath(pn.fieldtrip); ft_defaults;
pn.tools        = fullfile(rootpath, 'tools');
    addpath(genpath(fullfile(pn.tools, 'MEGPLS_PIPELINE_v2.02b')))
pn.plotFolder = fullfile(rootpath, 'figures', 'G_decode');
    addpath(genpath(fullfile(rootpath, 'tools', 'plot2svg')))

cBrew = brewermap(4,'RdBu');

% 2131, 2237 removed due to missing runs
filename = fullfile(rootpath, 'code', 'id_list_mr_completeruns.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
tmp_IDs = IDs{1};

IDs{1} = tmp_IDs(cellfun(@str2num, tmp_IDs, 'un', 1)<2000);
IDs{2} = tmp_IDs(cellfun(@str2num, tmp_IDs, 'un', 1)>2000);

DA_merged = cell(1,2);
for indAge = 1:2
    for id = 1:numel(IDs{indAge})
        ID = IDs{indAge}{id};
        curFile = fullfile(pn.data, [ID, '_g6d_accuracy.mat']);
        load(curFile);
        DA_merged{indAge}(id,:,:,:,:,:) = DA_end;
        % load trial assignment to split into categories of interest
        load(fullfile(pn.trlinfo, [ID,'_TrlInfo_mat.mat']))
        for indAtt = 1:4
            for indDim = 1:4
                curTrials = TrlInfo(:,6) == indAtt & TrlInfo(:,8) == indDim;
                DecTarget_probed{indAge}(id,indDim,indAtt,:) = squeeze(nanmean(DA_end(curTrials,indAtt,1,2,:),1));
            end
        end
    end
end

% plot summary statistics

h = figure('units','normalized','position',[.1 .1 .2 .15]);
subplot(1,2,1); cla; hold on;
%plot([0,4], [50,50], 'k--', 'LineWidth',2)
for indAge = 1:2
    curData = squeeze(nanmean(nanmean(DecTarget_probed{indAge}(:,:,1:4,7:19),4),3));
    condAverage = nanmean(curData,2);
    curData = curData-condAverage+repmat(nanmean(condAverage,1),size(condAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    errorbar(nanmean(curData,1),standError, 'color', cBrew(indAge,:), 'linewidth', 2)
%     dimPlot{indAge} = shadedErrorBar(1:4,nanmean(curData,1),standError, ...
%         'lineprops', {'color', cBrew(indAge,:),'linewidth', 2}, 'patchSaturation', .25);
    ylim([50 52.5])
    xlabel('Target #'); ylabel('Decoding accuracy (%)')
    xlim([.5 4.5])
end
title('Probed')

%% add seed for reproducibility

rng(0, 'twister');

% create datamat

input_data{1} = squeeze(nanmean(DecTarget_probed{1},3));
input_data{2} = squeeze(nanmean(DecTarget_probed{2},3));

num_subj_lst = [numel(IDs{1}), numel(IDs{2})];
num_cond = 4;
num_grp = 2;
num_time = size(input_data{1},3);

datamat_lst = cell(num_grp); lv_evt_list = [];
indCount_cont = 1;
for indGroup = 1:num_grp
    indCount = 1;
    curTFRdata = input_data{indGroup};
    for indCond = 1:num_cond
        for indID = 1:num_subj_lst(indGroup)
            datamat_lst{indGroup}(indCount,:) = reshape(squeeze(curTFRdata(indID,indCond,:)), [], 1);
            lv_evt_list(indCount_cont) = indCond;
            indCount = indCount+1;
            indCount_cont = indCount_cont+1;
        end
    end
end
datamat_lst{indGroup}(isnan(datamat_lst{indGroup})) = 0;

%% set PLS options and run PLS

option = [];
option.method = 1; % [1] | 2 | 3 | 4 | 5 | 6
option.num_perm = 1000; %( single non-negative integer )
option.num_split = 0; %( single non-negative integer )
option.num_boot = 1000; % ( single non-negative integer )
option.cormode = 0; % [0] | 2 | 4 | 6
option.meancentering_type = 0;% [0] | 1 | 2 | 3
option.boot_type = 'strat'; %['strat'] | 'nonstrat'

result = pls_analysis(datamat_lst, num_subj_lst, num_cond, option);

%% rearrange into fieldtrip structure

lvdat = result.boot_result.compare_u(:,1);
%udat = reshape(result.u, num_chans, num_freqs, num_time);

stat = [];
stat.prob = lvdat;
stat.dimord = 'chan_freq_time';
stat.clusters = [];
stat.clusters.prob = result.perm_result.sprob; % check for significance of LV
stat.mask = lvdat > 3 | lvdat < -3;
stat.cfg = option;

save(fullfile(pn.data, 'g6d_pls_probed.mat'), 'stat', 'result', 'lvdat', 'lv_evt_list')
