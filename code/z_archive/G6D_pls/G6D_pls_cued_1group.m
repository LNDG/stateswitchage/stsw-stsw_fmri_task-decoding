% output is trial-wise decoding accurace of attribute-specific prevalence
% discrimination
% trained across trials from all dims, excl. test trial

clear all; clc;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

pn.data = fullfile(pn.root, 'data', 'G_DecodingResults');
    addpath(fullfile(pn.root, 'tools', 'shadedErrorBar'))
    addpath(fullfile(pn.root, 'tools', 'BrewerMap'))
    addpath(genpath(fullfile(pn.root, 'tools', 'RainCloudPlots')))
pn.trlinfo = fullfile(pn.root, 'data', 'C_trialInfo');
% pn.fieldtrip = fullfile(pn.root, 'tools', 'fieldtrip');
%     addpath(pn.fieldtrip); ft_defaults;
pn.tools        = fullfile(pn.root, 'tools');
    addpath(genpath(fullfile(pn.tools, 'MEGPLS_PIPELINE_v2.02b')))
pn.plotFolder = fullfile(pn.root, 'figures', 'G_decode');
    addpath(genpath(fullfile(pn.root, 'tools', 'plot2svg')))

cBrew = brewermap(4,'RdBu');

% N = 42 YAs
IDs{1} = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

% N = 51 OAs (2131, 2237 removed due to missing run)
% 2139, 2227 marked as anatomical anomalies 
IDs{2} = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227'; '2236';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

DA_merged = cell(1,2);
for indAge = 1:2
    for id = 1:numel(IDs{indAge})
        ID = IDs{indAge}{id};
        curFile = fullfile(pn.data, [ID, '_G6D_DecodeWinOption_VC_allTrials_3fold.mat']);
        load(curFile);
        DA_merged{indAge}(id,:,:,:,:,:) = DA_end;
        % load trial assignment to split into categories of interest
        load(fullfile(pn.trlinfo, [ID,'_TrlInfo_mat.mat']))
        for indAtt = 1:4
            for indDim = 1:4
                curTrials = TrlInfo(:,indAtt) == 1 & TrlInfo(:,8) == indDim;
                DecTarget_cued{indAge}(id,indDim,indAtt,:) = squeeze(nanmean(DA_end(curTrials,indAtt,1,2,:),1));
            end
        end
    end
end

% plot summary statistics

h = figure('units','normalized','position',[.1 .1 .2 .15]);
subplot(1,2,1); cla; hold on;
%plot([0,4], [50,50], 'k--', 'LineWidth',2)
for indAge = 1:2
    curData = squeeze(nanmean(nanmean(DecTarget_cued{indAge}(:,:,1:4,7:19),4),3));
    condAverage = nanmean(curData,2);
    curData = curData-condAverage+repmat(nanmean(condAverage,1),size(condAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    errorbar(nanmean(curData,1),standError, 'color', cBrew(indAge,:), 'linewidth', 2)
%     dimPlot{indAge} = shadedErrorBar(1:4,nanmean(curData,1),standError, ...
%         'lineprops', {'color', cBrew(indAge,:),'linewidth', 2}, 'patchSaturation', .25);
    ylim([50 52.5])
    xlabel('Target #'); ylabel('Decoding accuracy (%)')
    xlim([.5 4.5])
end
title('Probed')

%% add seed for reproducibility

rng(0, 'twister');

% create datamat

input_data = cat(1, squeeze(nanmean(DecTarget_cued{1},3)),squeeze(nanmean(DecTarget_cued{2},3)));

num_subj_lst = [numel(cat(1, IDs{1},IDs{2}))];
num_cond = 4;
num_grp = 1;
num_time = size(input_data,3);

datamat_lst = cell(num_grp); lv_evt_list = [];
indCount_cont = 1;
for indGroup = 1:num_grp
    indCount = 1;
    curTFRdata = input_data;
    for indCond = 1:num_cond
        for indID = 1:num_subj_lst(indGroup)
            datamat_lst{indGroup}(indCount,:) = reshape(squeeze(curTFRdata(indID,indCond,:)), [], 1);
            lv_evt_list(indCount_cont) = indCond;
            indCount = indCount+1;
            indCount_cont = indCount_cont+1;
        end
    end
end
datamat_lst{indGroup}(isnan(datamat_lst{indGroup})) = 0;

%% set PLS options and run PLS

option = [];
option.method = 1; % [1] | 2 | 3 | 4 | 5 | 6
option.num_perm = 1000; %( single non-negative integer )
option.num_split = 0; %( single non-negative integer )
option.num_boot = 1000; % ( single non-negative integer )
option.cormode = 0; % [0] | 2 | 4 | 6
option.meancentering_type = 0;% [0] | 1 | 2 | 3
option.boot_type = 'strat'; %['strat'] | 'nonstrat'

result = pls_analysis(datamat_lst, num_subj_lst, num_cond, option);

%% rearrange into fieldtrip structure

lvdat = result.boot_result.compare_u(:,1);
%udat = reshape(result.u, num_chans, num_freqs, num_time);

stat = [];
stat.prob = lvdat;
stat.dimord = 'chan_freq_time';
stat.clusters = [];
stat.clusters.prob = result.perm_result.sprob; % check for significance of LV
stat.mask = lvdat > 3 | lvdat < -3;
stat.cfg = option;

save(fullfile(pn.data, 'g6d_pls_cued_1group.mat'), 'stat', 'result', 'lvdat', 'lv_evt_list')
