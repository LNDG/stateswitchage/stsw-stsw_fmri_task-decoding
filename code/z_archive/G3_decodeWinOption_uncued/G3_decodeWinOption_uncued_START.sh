#!/bin/bash

# call the BOSC analysis by session and participant
RootPath="/home/mpib/LNDG/StateSwitch/WIP/L_V5motion/"

cd ${RootPath}/A_scripts/G_decodeOptionsVC/G3_decodeWinOption_uncued

subjList="1117 1118 1120 1124 1125 1126 1131 1132 1135 1136 1151 1160 1164 1167 1169 1172 1173 1178 1182 1214 1215 1216 1219 1223 1227 1228 1233 1234 1237 1239 1240 1243 1245 1247 1250 1252 1257 1261 1265 1266 1268 1270 1276 1281 2104 2107 2108 2112 2118 2120 2121 2123 2125 2129 2130 2132 2133 2134 2135 2139 2140 2145 2147 2149 2157 2160 2201 2202 2203 2205 2206 2209 2210 2211 2213 2214 2215 2216 2217 2219 2222 2224 2226 2227 2236 2238 2241 2244 2246 2248 2250 2251 2252 2258 2261"

for subj in $subjList; do
	echo "#!/bin/bash"                    			> job.slurm
	echo "#SBATCH --job-name STSW_decVC_uc_${subj}" 	>> job.slurm
	echo "#SBATCH --cpus-per-task 1"				>> job.slurm
	echo "#SBATCH --mem 4gb" 						>> job.slurm
	echo "#SBATCH --time 03:00:00" 					>> job.slurm
	echo "#SBATCH --output ${RootPath}/Y_logs/STSW_decVC_uc_${subj}.out"			>> job.slurm
	echo "#SBATCH --workdir ." 										>> job.slurm
	echo "./G3_decodeWinOption_uncued_run.sh /opt/matlab/R2016b ${subj}" 	>> job.slurm
	sbatch job.slurm
	rm -f job.slurm
done