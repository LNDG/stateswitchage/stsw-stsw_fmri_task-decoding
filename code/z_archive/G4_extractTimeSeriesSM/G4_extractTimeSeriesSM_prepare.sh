#!/bin/bash

# This script prepares tardis by compiling the necessary function in MATLAB.

ssh tardis # access tardis

# check and choose matlab version
module avail matlab
module load matlab/R2016b

# compile functions

matlab
% add toolboxes
addpath('/home/mpib/LNDG/StateSwitch/WIP/G_GLM/T_tools/NIFTI_toolbox/')
addpath('/home/mpib/LNDG/StateSwitch/WIP/G_GLM/T_tools/preprocessing_tools/')
%% go to analysis directory containing .m-file
cd('/home/mpib/LNDG/StateSwitch/WIP/L_V5motion/A_scripts/G_decodeOptionsVC/G4_extractTimeSeriesSM/')
%% compile function and append dependencies
mcc -m G4_extractTimeSeriesSM.m -a /home/mpib/LNDG/StateSwitch/WIP/G_GLM/T_tools/NIFTI_toolbox -a /home/mpib/LNDG/StateSwitch/WIP/G_GLM/T_tools/preprocessing_tools
exit