function G4_extractTimeSeriesSM(ID)

    % extract temporal traces from all voxels in bilateral s mask to
    % subject to decoding analysis
    
    if ismac
        pn.root = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP/L_V5motion/';
        pn.rootBOLD = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP/G_GLM/';
        % add tools (precompiled on cluster)
        pn.tools_1 = [pn.rootBOLD, 'T_tools/NIFTI_toolbox']; addpath(genpath(pn.tools_1))
        pn.tools_2 = [pn.rootBOLD, 'T_tools/preprocessing_tools']; addpath(genpath(pn.tools_2))
    else
        pn.root = '/home/mpib/LNDG/StateSwitch/WIP/L_V5motion/';
        pn.rootBOLD = '/home/mpib/LNDG/StateSwitch/WIP/G_GLM/';
    end
    pn.standards = [pn.root, 'B_data/A_standards/'];
    pn.BOLD = [pn.rootBOLD, 'B_data/BOLDin/'];
    pn.regressors = [pn.rootBOLD, 'B_data/A_regressors/'];
    pn.out = [pn.root, 'B_data/B_singleTrialV5/'];
    
    pn.vcMask = [pn.standards, 'SomatoMotor_unified_thr_MNI_3mm.nii'];
    [vcMask] = double(S_load_nii_2d(pn.vcMask));
    
    numConds_raw = 4; % number of conditions may have changed in PLS input structure during preproc
    
    disp(['Processing subject ', ID, '.']);

    TrialSignal = []; 
    for indRun = 1:numConds_raw

        % load nifti file for this run; %(x by y by z by time)
        % check for nii or nii.gz, unzip .gz and reshape to 2D

        % if proc on tardis: change path to tardis preproc directory
       
        fileName = [ID, '_run-',num2str(indRun),'_regressed.nii'];
        fname = [pn.BOLD, fileName];
        
        if ~exist(fname)
            warning(['File not available: ', ID, ' Run ', num2str(indRun)]);
            continue;
        end
        
        % If using preprocessing tools
        [img] = double(S_load_nii_2d(fname));
        
        % get time series within visual cortex mask
        vcsignal = img(vcMask==1,:);
        
        %% cut into trials
               
        load([pn.regressors, ID, '_Run',num2str(indRun),'_regressors.mat'])
        
        if strcmp(ID, '2132') && indRun == 2
            Regressors(1041-20:end) = [];
        end
        
        % get stimulus onsets
        onset_samples = find(Regressors(:,3));
        for indOnset = 1:numel(onset_samples)            
            TrialSignal((indRun-1)*64+indOnset,:,:) = vcsignal(:,onset_samples(indOnset):onset_samples(indOnset)+20);
        end
        
    end
    
    save([pn.out, ID, '_SM_allVox.mat'], 'TrialSignal')
        