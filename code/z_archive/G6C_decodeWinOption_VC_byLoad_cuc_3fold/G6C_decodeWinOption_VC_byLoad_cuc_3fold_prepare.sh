#!/bin/bash

# This script prepares tardis by compiling the necessary function in MATLAB.

ssh tardis # access tardis

# check and choose matlab version
module avail matlab
module load matlab/R2016b

# compile functions

matlab
% add libsvm toolbox
addpath('/home/mpib/LNDG/StateSwitch/WIP/L_V5motion/T_tools/libsvm-3.11/')
%% go to analysis directory containing .m-file
cd('/home/mpib/LNDG/StateSwitch/WIP/L_V5motion/A_scripts/G_decodeOptionsVC/G6C_decodeWinOption_VC_byLoad_cuc_3fold')
%% compile function and append dependencies
mcc -m G6C_decodeWinOption_VC_byLoad_cuc_3fold.m -a /home/mpib/LNDG/StateSwitch/WIP/L_V5motion/T_tools/libsvm-3.11
exit