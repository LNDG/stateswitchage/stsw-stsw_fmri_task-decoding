
function H1_decodeTargetD1(rootpath, id)

    % Time-resolved decoding of prevalent feature
    % single timepoint training and testing
    
    % implements single-trial testing
    
    if ismac
        id = '2131';
        currentFile = mfilename('fullpath');
        [pathstr,~,~] = fileparts(currentFile);
        cd(fullfile(pathstr,'..','..'))
        rootpath = pwd;
    end
    
    %% add paths

    pn.libsvm   = fullfile(rootpath, 'tools', 'libsvm'); addpath(genpath(pn.libsvm));
    pn.data     = fullfile(rootpath, 'data', 'B_singleTrialV5');
    pn.trlinfo  = fullfile(rootpath, 'data', 'C_trialInfo');
    pn.out      = fullfile(rootpath, 'data', 'G_DecodingResults');
    
    mex -setup
    cd(fullfile(pn.libsvm,'matlab'));
    make

    % load voxel-wise data
    load(fullfile(pn.data, [id,'_VC_allVox.mat']))
    
    % load trial info to get prevalent options
    load(fullfile(pn.trlinfo, [id,'_TrlInfo_mat.mat']))
    
    % prepare for the contingency that some voxels may be NaN
    TrialSignal(isnan(TrialSignal)) = 0;
    
    N_chans = size(TrialSignal,2);
    N_time = size(TrialSignal,3);
    N_conditions = 4;    
    
    numIncluded = [];
    for indDim = 1:5
        D{indDim} = NaN(4,32,N_chans,N_time); % condition*trial*channel*time matrix
        for indAtt = 1:4
            disp(num2str(indAtt));
            if indDim == 5
                includedTrials = find(isnan(TrlInfo(:,indAtt))); % 5th dim condition are uncued trials
            else
                includedTrials = find(TrlInfo(:,6) == indAtt & TrlInfo(:,8) == indDim); % attribute is probed
            end
            numIncluded(indDim,indAtt) = numel(includedTrials);
            for indTrial = 1:numIncluded(indDim,indAtt)
                D{indDim}(indAtt,indTrial,:,1:N_time) = TrialSignal(includedTrials(indTrial),:,:);
            end
        end
    end
    
    %% 3) Decoding
    
    num_permutations=100;
    DA=NaN(5,num_permutations,N_conditions,N_conditions,N_time);
    for indDim = 1:5
        for perm =1:num_permutations
            tic

            % create folds such that min. 1 of each category is not
            % included in the fold

            % reserve 1 random trial for each condition that will not be
            % included in training set

            train = cell(5,1); % training set specific to dim 1, attribute
            test = cell(5,1);
            for indDim_s = 1:5
                test{indDim_s} = NaN(4,1,N_chans,N_time);
                for indAtt_s = 1:4
                    trialsAvailable     = find(~isnan(D{indDim_s}(indAtt_s,:,1,1)));
                    randomTrials        = randperm(numel(trialsAvailable));
                    trialsTrain         = trialsAvailable(randomTrials(1:numel(trialsAvailable)-1));
                    trialsTest          = trialsAvailable(randomTrials(end));
                    if indDim_s == 1
                        train{indAtt_s} = cat(2, train{indAtt_s}, D{indDim_s}(indAtt_s,trialsTrain,:,:));
                    end
                    test{indDim_s}(indAtt_s,:,:,:) = D{indDim_s}(indAtt_s,trialsTest,:,:);
                    % for uncued trials, this does not guarantee
                    % independence from those trials contained in the
                    % training set (overlap between D1 and uncued definition here)
                end
            end

            % equalize number of training trials

            tmp_size = [];
            for indAtt_s = 1:4
                tmp_size = cat(1, tmp_size, size(train{indAtt_s},2));
            end
            L = min([tmp_size]);

            %% perform SVM classification

            for condA=1:N_conditions %loop through all conditions
                for condB = condA+1:N_conditions  %loop through all conditions >condA+1
                    for time_point =1:N_time % all time points are independent
                        % L-1 pseudo trials go to testing set, the Lth to training set
                        MEEG_training_data=[squeeze(train{condA}(1,1:L,:,time_point)) ; squeeze(train{condB}(1,1:L,:,time_point))];
                        MEEG_testing_data=[squeeze(test{indDim}(condA,1,:,time_point))' ; squeeze(test{indDim}(condB,1,:,time_point))'];
                        % you have to define class labels for the trials you will train and test
                        labels_train=[ones(1,L) 2*ones(1,L)]; %they are L times 1, and L times 2 in a row (you just label any classes as 1 and 2, as the SVM at any time point only sees 2 classes
                        labels_test= [1 2]; % you will test on one trial from condA, and one from cond B
                        % train SVM
                        model = svmtrain(labels_train', MEEG_training_data,'-s 0 -t 0 -q');
                        % test SVM
                        [predicted_label, accuracy, decision_values] = svmpredict(labels_test', MEEG_testing_data , model);
                        DA(indDim,perm,condA,condB,time_point)=accuracy(1);
                    end % time point
                end % cond A
            end %cond B
            toc
        end % permutation
    end
    
    % average across permutations
    DA_end = squeeze(nanmean(DA,2));
    DA_std = squeeze(nanstd(DA,[],2));
    
    %figure; plot(squeeze(DA_end(2,1,2,:)))
   
    %% save results

    save([pn.out, ID, '_DecodeTargetD1.mat'], 'DA_end', 'DA_std');
    
end % function end