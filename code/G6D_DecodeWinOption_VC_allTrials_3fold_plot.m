% output is trial-wise decoding accurace of attribute-specific prevalence
% discrimination
% trained across trials from all dims, excl. test trial

clear all; clc;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.data = fullfile(rootpath, 'data', 'G_DecodingResults');
    addpath(fullfile(rootpath, 'tools', 'shadedErrorBar'))
    addpath(fullfile(rootpath, 'tools', 'BrewerMap'))
pn.trlinfo = fullfile(rootpath, 'data', 'C_trialInfo');
pn.fieldtrip = fullfile(rootpath, 'tools', 'fieldtrip');
    addpath(pn.fieldtrip); ft_defaults;
pn.plotFolder = fullfile(rootpath, 'figures', 'G_decode');
    addpath(genpath(fullfile(rootpath, 'tools', 'plot2svg')))

% 2131, 2237 removed due to missing runs
filename = fullfile(rootpath, 'code', 'id_list_mr_completeruns.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
tmp_IDs = IDs{1};

IDs{1} = tmp_IDs(cellfun(@str2num, tmp_IDs, 'un', 1)<2000);
IDs{2} = tmp_IDs(cellfun(@str2num, tmp_IDs, 'un', 1)>2000);

DA_merged = cell(1,2);
for indAge = 1:2
    for id = 1:numel(IDs{indAge})
        ID = IDs{indAge}{id};
        curFile = fullfile(pn.data, [ID, '_g6d_accuracy.mat']);
        if ~exist(curFile)
            disp(['File missing: ', ID])
            DA_merged{indAge}(id,:,:,:,:,:) = NaN;
            continue;
        end
        load(curFile);
        if size(DA_end,1)==128 % if only the initial two runs are available
            DA_end = cat(1, DA_end, NaN(size(DA_end)));
        end
        DA_merged{indAge}(id,:,:,:,:,:) = DA_end;
        % load trial assignment to split into categories of interest
        load(fullfile(pn.trlinfo, [ID,'_TrlInfo_mat.mat']))
        for indAtt = 1:4
            for indDim = 1:4
                curTrials = TrlInfo(:,6) == indAtt & TrlInfo(:,8) == indDim;
                DecTarget_probed{indAge}(id,indDim,indAtt,:) = squeeze(nanmean(DA_end(curTrials,indAtt,1,2,:),1));
                % dec acc for feature x when feature x was NOT probed
                curTrials = TrlInfo(:,6) ~= indAtt & TrlInfo(:,8) == indDim;
                DecTarget_unprobed{indAge}(id,indDim,indAtt,:) = squeeze(nanmean(DA_end(curTrials,indAtt,1,2,:),1));
                % on trials on which attribute x was probed, was there
                % evidence for attributes y? There should be at increasing
                % loads as attributes y have also been cued
                curTrials = TrlInfo(:,6) == indAtt & TrlInfo(:,8) == indDim;
                DecTarget_probed_other{indAge}(id,indDim,indAtt,:) = squeeze(nanmean(nanmean(DA_end(curTrials,~ismember([1:4],indAtt),1,2,:),1),2));
                curTrials = TrlInfo(:,indAtt) == 1 & TrlInfo(:,8) == indDim;
                DecTarget_cued{indAge}(id,indDim,indAtt,:) = squeeze(nanmean(DA_end(curTrials,indAtt,1,2,:),1));
                curTrials = TrlInfo(:,indAtt) == 1 & TrlInfo(:,6) ~= indAtt & TrlInfo(:,8) == indDim;
                DecTarget_cued_unprobed{indAge}(id,indDim,indAtt,:) = NaN(1,21);
                if indDim > 1
                    DecTarget_cued_unprobed{indAge}(id,indDim,indAtt,:) = squeeze(nanmean(DA_end(curTrials,indAtt,1,2,:),1));
                end
%                 curTrials = TrlInfo(:,indAtt) == 1 & TrlInfo(:,6) ~= indAtt & TrlInfo(:,8) == indDim;
%                 DecTarget_cued_unprobed{indAge}(id,indDim,indAtt,:) = squeeze(nanmean(DA_end(curTrials,indAtt,1,2,:),1));
                curTrials = isnan(TrlInfo(:,indAtt)) & TrlInfo(:,8) == indDim;
                DecTarget_uncued{indAge}(id,indDim,indAtt,:) = squeeze(nanmean(DA_end(curTrials,indAtt,1,2,:),1));
                curTrials = TrlInfo(:,8) == indDim;
                DecTarget_all{indAge}(id,indDim,indAtt,:) = squeeze(nanmean(DA_end(curTrials,indAtt,1,2,:),1));
            end
        end
    end
end

%% plot results

time = ([1:21]-1).*.645;

% average across features within dimensionality
for indAge = 1:2
    DA_merged_1{indAge} = []; DA_merged_1{indAge} = squeeze(nanmean(DecTarget_cued{indAge}(:,1,:,:),3));
    DA_merged_2{indAge} = []; DA_merged_2{indAge} = squeeze(nanmean(DecTarget_cued{indAge}(:,2,:,:),3));
    DA_merged_3{indAge} = []; DA_merged_3{indAge} = squeeze(nanmean(DecTarget_cued{indAge}(:,3,:,:),3));
    DA_merged_4{indAge} = []; DA_merged_4{indAge} = squeeze(nanmean(DecTarget_cued{indAge}(:,4,:,:),3));
    DA_merged_uncued{indAge} = []; DA_merged_uncued{indAge} = squeeze(nanmean(nanmean(DecTarget_uncued{indAge}(:,1:4,:,:),3),2));
    DA_merged_cued{indAge} = []; DA_merged_cued{indAge} = squeeze(nanmean(nanmean(DecTarget_cued{indAge}(:,1:4,:,:),3),2));
end

ageConditions = {'Young Adults'; 'Old Adults'};

cBrew = brewermap(4,'RdBu');

h = figure('units','normalized','position',[.1 .1 .15 .5]);
cla;
for indAge = 1:2
    subplot(2,1,indAge);
    patches.timeVec = [0 5 8 12.9];
    patches.colorVec = [1 1 1;.9 .9 .9; 1 1 1; 1 .9 .9; 1 1 1; .9 .9 .9];
    for indP = 1:numel(patches.timeVec)-1
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [0 0 [100 100]], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end; hold on;
    plot([0,12.9], [50,50], 'k--', 'LineWidth',2)
    for indCond = 1:4
        grandAverage = squeeze(nanmean(eval(['DA_merged_',num2str(indCond), '{indAge}']),1));
        curData = smoothts(squeeze(eval(['DA_merged_',num2str(indCond), '{indAge}'])),'b',1);
        condAvg = squeeze(nanmean(nanmean(eval(['DA_merged_cued{indAge}']),2),3));
        % within-subject standard error bars
        curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        dimPlot{indCond} = shadedErrorBar(time,nanmean(curData,1),standError, ...
            'lineprops', {'color', cBrew(indCond,:),'linewidth', 2}, 'patchSaturation', .25);
    end
    ylim([46 58])
    title(ageConditions{indAge});
    xlabel('Time (TR) from stim onset'); ylabel('Decoding accuracy (%)')
    xlim([0 12.9])
end
set(findall(gcf,'-property','FontSize'),'FontSize',18)
legend([dimPlot{1}.mainLine, dimPlot{2}.mainLine, dimPlot{3}.mainLine, dimPlot{4}.mainLine],...
        {'L1'; 'L2'; 'L3'; 'L4'}, 'orientation', 'vertical', 'location', 'NorthWest')
legend('boxoff')
suptitle({'VC decoding';'prevalent option'})

%% perform cluster-based permutation tests (1D) of decoding vs. chance

stat = cell(1);
for indCued = 1:2
    if indCued == 1
        curData = DA_merged_cued;
    else
        curData = DA_merged_uncued;
    end
    for indGroup = 1:3
        if indGroup==3 % across younger and older adults
            data_forFT = cat(1,curData{1},curData{2});
        else
            data_forFT = cat(1,curData{indGroup});
        end
        decode_forFT = [];
        for indID = 1:size(data_forFT,1)
            decode_forFT{1,indID}.data = data_forFT(indID,:);
            decode_forFT{1,indID}.dimord = 'chan_time';
            decode_forFT{1,indID}.time = 1:size(data_forFT,2);
            decode_forFT{1,indID}.label{1} = 'decoding';
            decode_forFT{2,indID} = decode_forFT{1,indID};
            decode_forFT{2,indID}.data(:) = 50; 
        end

        cfgStat = [];
        cfgStat.method           = 'montecarlo';
        cfgStat.statistic        = 'ft_statfun_depsamplesT';
        cfgStat.correctm         = 'cluster';
        cfgStat.clusteralpha     = 0.05;
        cfgStat.clusterstatistic = 'maxsum';
        cfgStat.minnbchan        = 0;
        cfgStat.tail             = 0;
        cfgStat.clustertail      = 0;
        cfgStat.alpha            = 0.025;
        cfgStat.numrandomization = 500;
        cfgStat.parameter        = 'data';
        cfgStat.neighbours      = []; % no neighbors here 

        subj = size(data_forFT,1);
        conds = 2;
        design = zeros(2,conds*subj);
        for indCond = 1:conds
        for i = 1:subj
            design(1,(indCond-1)*subj+i) = indCond;
            design(2,(indCond-1)*subj+i) = i;
        end
        end
        cfgStat.design   = design;
        cfgStat.ivar     = 1;
        cfgStat.uvar     = 2;

        [stat{indCued, indGroup}] = ft_timelockstatistics(cfgStat, decode_forFT{1,:}, decode_forFT{2,:});
    end
end

h = figure('units','normalized','position',[.1 .1 .3 .3]);
set(gcf,'renderer','opengl')
subplot(1,2,1);
    cla;
    patches.timeVec = [0 5 8 12.9];
    patches.colorVec = [1 1 1;.9 .9 .9; 1 1 1; 1 .9 .9; 1 1 1; .9 .9 .9];
    for indP = 1:numel(patches.timeVec)-1
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [0 0 [100 100]], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end; hold on;
    plot([0,12.9], [50,50], 'k--', 'LineWidth',2)
    for indAge = 1:2
        for indCond = 1
            curData = smoothts(squeeze(eval(['DA_merged_cued{indAge}'])),'b',1);
            standError = nanstd(curData,1)./sqrt(size(curData,1));
            dimPlot{indAge} = shadedErrorBar(time,nanmean(curData,1),standError, ...
                'lineprops', {'color', cBrew(indAge,:),'linewidth', 2}, 'patchSaturation', .25);
        end
        ylim([49 53])
        xlabel('Time (TR) from stim onset'); ylabel('Decoding accuracy (%)')
        xlim([0 12.9])
    end
    statsMask = double(stat{1,1}.mask); statsMask(statsMask==0) = NaN;
    plot(time, 49.7.*statsMask, 'color', cBrew(1,:), 'LineWidth',2)
    statsMask = double(stat{1,2}.mask); statsMask(statsMask==0) = NaN;
    plot(time, 49.6.*statsMask, 'color', cBrew(2,:), 'LineWidth',2)
%     statsMask = double(stat{1,3}.mask); statsMask(statsMask==0) = NaN;
%     plot(time, 49.5.*statsMask, 'color', 'k', 'LineWidth',2)
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    legend([dimPlot{1}.mainLine, dimPlot{2}.mainLine],...
            {'YA'; 'OA'}, 'orientation', 'horizontal', 'location', 'NorthWest')
    legend('boxoff')
    title({'Cued trials'})
subplot(1,2,2); cla;
    patches.timeVec = [0 5 8 12.9];
    patches.colorVec = [1 1 1;.9 .9 .9; 1 1 1; 1 .9 .9; 1 1 1; .9 .9 .9];
    for indP = 1:numel(patches.timeVec)-1
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [0 0 [100 100]], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end; hold on;
    plot([0,12.9], [50,50], 'k--', 'LineWidth',2)
    for indAge = 1:2
        for indCond = 1
            curData = smoothts(squeeze(eval(['DA_merged_uncued{indAge}'])),'b',1);
            standError = nanstd(curData,1)./sqrt(size(curData,1));
            dimPlot{indAge} = shadedErrorBar(time,nanmean(curData,1),standError, ...
                'lineprops', {'color', cBrew(indAge,:),'linewidth', 2, 'linestyle', ':'}, 'patchSaturation', .25);
        end
        ylim([49 53])
        xlabel('Time (TR) from stim onset'); ylabel('Decoding accuracy (%)')
        xlim([0 12.9])
    end
    statsMask = double(stat{2,1}.mask); statsMask(statsMask==0) = NaN;
    plot(time, 49.7.*statsMask, 'color', cBrew(1,:), 'LineWidth',2)
    statsMask = double(stat{2,2}.mask); statsMask(statsMask==0) = NaN;
    plot(time, 49.6.*statsMask, 'color', cBrew(2,:), 'LineWidth',2)
%     statsMask = double(stat{2,3}.mask); statsMask(statsMask==0) = NaN;
%     plot(time, 49.5.*statsMask, 'color', 'k', 'LineWidth',2)
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    legend([dimPlot{1}.mainLine, dimPlot{2}.mainLine],...
            {'YA'; 'OA'}, 'orientation', 'horizontal', 'location', 'NorthWest')
    legend('boxoff')
    title({'Uncued trials'})

figureName = 'G6D_winOption_VC_CuedUncued';
saveas(h, fullfile(pn.plotFolder, figureName), 'png');
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
%plot2svg(fullfile(pn.plotFolder, figureName, '.svg'), h, 'png');

%% load with within-SEM

h = figure('units','normalized','position',[.1 .1 .3 .3]);
    subplot(1,2,1)
    cla;
    patches.timeVec = [0 5 8 12.9];
    patches.colorVec = [1 1 1;.9 .9 .9; 1 1 1; 1 .9 .9; 1 1 1; .9 .9 .9];
    for indP = 1:numel(patches.timeVec)-1
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [0 0 [100 100]], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end; hold on;
    plot([0,12.9], [50,50], 'k--', 'LineWidth',2)
    grandAverage = smoothts(squeeze(eval(['DA_merged_cued{1}'])),'b',1);
    curData = smoothts(squeeze(eval(['DA_merged_1{1}'])),'b',1);
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    dimPlot{1,1} = shadedErrorBar(time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
    curData = smoothts(squeeze(eval(['DA_merged_4{1}'])),'b',1);
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    dimPlot{2,1} = shadedErrorBar(time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(1,:),'linewidth', 2, 'linestyle', '--'}, 'patchSaturation', .25);
    ylim([46 58])
    xlabel('Time (TR) from stim onset'); ylabel('Decoding accuracy (%)')
    xlim([0 12.9])
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    legend([dimPlot{1,1}.mainLine, dimPlot{2,1}.mainLine],...
            {'L1'; 'L4'}, 'orientation', 'horizontal', 'location', 'NorthWest')
    legend('boxoff')
    title({'VC decoding';'YA'})
subplot(1,2,2)
    cla;
    patches.timeVec = [0 5 8 12.9];
    patches.colorVec = [1 1 1;.9 .9 .9; 1 1 1; 1 .9 .9; 1 1 1; .9 .9 .9];
    for indP = 1:numel(patches.timeVec)-1
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [0 0 [100 100]], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end; hold on;
    plot([0,12.9], [50,50], 'k--', 'LineWidth',2)
    grandAverage = smoothts(squeeze(eval(['DA_merged_cued{2}'])),'b',1);
    curData = smoothts(squeeze(eval(['DA_merged_1{2}'])),'b',1);
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    dimPlot{1,2} = shadedErrorBar(time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(2,:),'linewidth', 2}, 'patchSaturation', .25);
    curData = smoothts(squeeze(eval(['DA_merged_4{2}'])),'b',1);
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    dimPlot{2,2} = shadedErrorBar(time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(2,:),'linewidth', 2, 'linestyle', '--'}, 'patchSaturation', .25);
    ylim([46 58])
    xlabel('Time (TR) from stim onset'); ylabel('Decoding accuracy (%)')
    xlim([0 12.9])
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    legend([dimPlot{1,2}.mainLine, dimPlot{2,2}.mainLine],...
            {'L1'; 'L4'}, 'orientation', 'horizontal', 'location', 'NorthWest')
    legend('boxoff')
    title({'VC decoding';'OA'})
    suptitle('within-subject SEM')

%% plot decoding for each feature, only for uncued trials

% average across features within dimensionality
for indAge = 1:2
    DA_merged_1{indAge} = []; DA_merged_1{indAge} = squeeze(nanmean(DecTarget_uncued{indAge}(:,1:4,1,:),2));
    DA_merged_2{indAge} = []; DA_merged_2{indAge} = squeeze(nanmean(DecTarget_uncued{indAge}(:,1:4,2,:),2));
    DA_merged_3{indAge} = []; DA_merged_3{indAge} = squeeze(nanmean(DecTarget_uncued{indAge}(:,1:4,3,:),2));
    DA_merged_4{indAge} = []; DA_merged_4{indAge} = squeeze(nanmean(DecTarget_uncued{indAge}(:,1:4,4,:),2));
end

features = {'color', 'motion', 'size', 'luminance'};

for indAge = 1:2
    for indCond = 1:4
        subplot(2,4,(indAge-1)*4+indCond);
        hold on;
        plot([0,12.9], [50,50], 'k--', 'LineWidth',2)
        grandAverage = squeeze(nanmean(eval(['DA_merged_',num2str(indCond), '{indAge}']),1));
        curData = smoothts(squeeze(eval(['DA_merged_',num2str(indCond), '{indAge}'])),'b',1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        dimPlot{indCond} = shadedErrorBar(time,nanmean(curData,1),standError, ...
            'lineprops', {'color', cBrew(indCond,:),'linewidth', 2, 'linestyle', '--'}, 'patchSaturation', .25);
        ylim([48 55])
        title(features{indCond});
        xlabel('Time (TR) from stim onset'); ylabel('Decoding accuracy (%)')
        xlim([0 12.9])
    end
end
suptitle('Uncued trials')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

% As before, signs of decodability only for motion in younger adults

%% median split based on drift rates

% expectation: subjects with stronger drift rate decreases nonetheless
% retain highest dec accuracy for targets (but additionally increase dec acc for remaining attributes)

% average across features within dimensionality
for indAge = 1:2
    DA_merged_1{indAge} = []; DA_merged_1{indAge} = squeeze(nanmean(DecTarget_cued{indAge}(:,1,:,:),3));
    DA_merged_2{indAge} = []; DA_merged_2{indAge} = squeeze(nanmean(DecTarget_cued{indAge}(:,2,:,:),3));
    DA_merged_3{indAge} = []; DA_merged_3{indAge} = squeeze(nanmean(DecTarget_cued{indAge}(:,3,:,:),3));
    DA_merged_4{indAge} = []; DA_merged_4{indAge} = squeeze(nanmean(DecTarget_cued{indAge}(:,4,:,:),3));
    DA_merged_uncued{indAge} = []; DA_merged_uncued{indAge} = squeeze(nanmean(nanmean(DecTarget_uncued{indAge}(:,1:4,:,:),3),2));
    DA_merged_cued{indAge} = []; DA_merged_cued{indAge} = squeeze(nanmean(nanmean(DecTarget_cued{indAge}(:,1:4,:,:),3),2));
end

pn.dataOut = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/';
load([pn.dataOut, 'STSWD_summary.mat'], 'STSWD_summary')

idx_YA = ismember(STSWD_summary.IDs, IDs{1});

drift = squeeze(nanmean(cat(2,STSWD_summary.HDDM_vt.driftEEG(idx_YA,1),STSWD_summary.HDDM_vt.driftMRI(idx_YA,1)),2));
[sortVal, sortIdx] = sort(drift,'descend');

idx{1} = sortIdx(1:ceil(numel(sortIdx)/3));
idx{2} = sortIdx(ceil(numel(sortIdx)/3)+1:2*ceil(numel(sortIdx)/3));
idx{3} = sortIdx(2*ceil(numel(sortIdx)/3)+1:end);

h = figure('units','normalized','position',[.1 .1 .15 .3]);
cla;
patches.timeVec = [0 5 8 12.9];
patches.colorVec = [1 1 1;.9 .9 .9; 1 1 1; 1 .9 .9; 1 1 1; .9 .9 .9];
for indP = 1:numel(patches.timeVec)-1
    p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                [0 0 [100 100]], patches.colorVec(indP,:));
    p.EdgeColor = 'none';
end; hold on;
plot([0,12.9], [50,50], 'k--', 'LineWidth',2)
for indAge = 1%:2
    for indCond = 1:3
        curData = smoothts(squeeze(eval(['DA_merged_cued{indAge}(idx{indCond},:,:,:)'])),'b',1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        dimPlot{indCond} = shadedErrorBar(time,nanmean(curData,1),standError, ...
            'lineprops', {'color', cBrew(indCond,:),'linewidth', 2}, 'patchSaturation', .25);
    end
    ylim([48 58])
    xlabel('Time (TR) from stim onset'); ylabel('Decoding accuracy (%)')
    xlim([0 12.9])
end
set(findall(gcf,'-property','FontSize'),'FontSize',18)
legend([dimPlot{1}.mainLine, dimPlot{2}.mainLine, dimPlot{3}.mainLine],...
        {'YA high drift'; 'YA mid drift';'YA low drift'}, 'orientation', 'vertical', 'location', 'NorthWest')
legend('boxoff')
title({'VC decoding';'prevalent option'})

%% plot the same for OAs

pn.dataOut = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/';
load([pn.dataOut, 'STSWD_summary.mat'], 'STSWD_summary')

idx_OA = ismember(STSWD_summary.IDs, IDs{2});

drift = squeeze(nanmean(cat(2,STSWD_summary.HDDM_vt.driftEEG(idx_OA,1),STSWD_summary.HDDM_vt.driftMRI(idx_OA,1)),2));
[sortVal, sortIdx] = sort(drift,'descend');

idx{1} = sortIdx(1:ceil(numel(sortIdx)/3));
idx{2} = sortIdx(ceil(numel(sortIdx)/3)+1:2*ceil(numel(sortIdx)/3));
idx{3} = sortIdx(2*ceil(numel(sortIdx)/3)+1:end);

h = figure('units','normalized','position',[.1 .1 .15 .3]);
cla;
patches.timeVec = [0 5 8 12.9];
patches.colorVec = [1 1 1;.9 .9 .9; 1 1 1; 1 .9 .9; 1 1 1; .9 .9 .9];
for indP = 1:numel(patches.timeVec)-1
    p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                [0 0 [100 100]], patches.colorVec(indP,:));
    p.EdgeColor = 'none';
end; hold on;
plot([0,12.9], [50,50], 'k--', 'LineWidth',2)
for indAge = 2
    for indCond = [1:3]
        curData = smoothts(squeeze(eval(['DA_merged_cued{indAge}(idx{indCond},:,:,:)'])),'b',1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        dimPlot{indCond} = shadedErrorBar(time,nanmean(curData,1),standError, ...
            'lineprops', {'color', cBrew(indCond,:),'linewidth', 2}, 'patchSaturation', .25);
    end
    ylim([48 58])
    xlabel('Time (TR) from stim onset'); ylabel('Decoding accuracy (%)')
    xlim([0 12.9])
end
set(findall(gcf,'-property','FontSize'),'FontSize',18)
legend([dimPlot{1}.mainLine, dimPlot{2}.mainLine, dimPlot{3}.mainLine],...
        {'OA high drift'; 'OA mid drift';'OA low drift'}, 'orientation', 'vertical', 'location', 'NorthWest')
legend('boxoff')
title({'VC decoding';'prevalent option'})
