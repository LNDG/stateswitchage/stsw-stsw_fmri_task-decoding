% output is trial-wise decoding accurace of attribute-specific prevalence
% discrimination
% trained across trials from all dims, excl. test trial

clear all; clc;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

pn.data = fullfile(pn.root, 'data', 'G_DecodingResults');
    addpath(fullfile(pn.root, 'tools', 'shadedErrorBar'))
    addpath(fullfile(pn.root, 'tools', 'BrewerMap'))
pn.trlinfo = fullfile(pn.root, 'data', 'C_trialInfo');
pn.fieldtrip = fullfile(pn.root, 'tools', 'fieldtrip');
    addpath(pn.fieldtrip); ft_defaults;
pn.plotFolder = fullfile(pn.root, 'figures', 'G_decode');
    addpath(genpath(fullfile(pn.root, 'tools', 'plot2svg')))

cBrew = brewermap(4,'RdBu');

% N = 42 YAs
IDs{1} = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

% N = 51 OAs (2131, 2237 removed due to missing run)
% 2139, 2227 marked as anatomical anomalies 
IDs{2} = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227'; '2236';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

DA_merged = cell(1,2);
for indAge = 1:2
    for id = 1:numel(IDs{indAge})
        ID = IDs{indAge}{id};
        curFile = fullfile(pn.data, [ID, '_g6d_accuracy.mat']);
        if ~exist(curFile)
            disp(['File missing: ', ID])
            DA_merged{indAge}(id,:,:,:,:,:) = NaN;
            continue;
        end
        load(curFile);
        DA_merged{indAge}(id,:,:,:,:,:) = DA_end;
        % load trial assignment to split into categories of interest
        load(fullfile(pn.trlinfo, [ID,'_TrlInfo_mat.mat']))
        for indAtt = 1:4
            for indDim = 1:4
                curTrials = TrlInfo(:,indAtt) == 1 & TrlInfo(:,8) == indDim;
                DecTarget_cued{indAge}(id,indDim,indAtt,:) = squeeze(nanmean(DA_end(curTrials,indAtt,1,2,:),1));
                curTrials = isnan(TrlInfo(:,indAtt)) & TrlInfo(:,8) == indDim;
                DecTarget_uncued{indAge}(id,indDim,indAtt,:) = squeeze(nanmean(DA_end(curTrials,indAtt,1,2,:),1));
            end
        end
    end
end

time = ([1:21]-1).*.645;

%% cued and uncued features: statistics and plots across age groups

stat = cell(1);
for indFeature = 1:4
    for indAge = 1
        for indCued = 1:2
            if indCued == 1
                data_forFT = cat(1,squeeze(nanmean(DecTarget_cued{1}(:,1:4,indFeature,:),2)),...
                    squeeze(nanmean(DecTarget_cued{2}(:,1:4,indFeature,:),2)));
            else
                data_forFT = cat(1,squeeze(nanmean(DecTarget_uncued{1}(:,1:4,indFeature,:),2)),...
                    squeeze(nanmean(DecTarget_uncued{2}(:,1:4,indFeature,:),2)));
            end
            decode_forFT = [];
            for indID = 1:size(data_forFT,1)
                decode_forFT{1,indID}.data = data_forFT(indID,:);
                decode_forFT{1,indID}.dimord = 'chan_time';
                decode_forFT{1,indID}.time = 1:size(data_forFT,2);
                decode_forFT{1,indID}.label{1} = 'decoding';
                decode_forFT{2,indID} = decode_forFT{1,indID};
                decode_forFT{2,indID}.data(:) = 50; 
            end

            cfgStat = [];
            cfgStat.method           = 'montecarlo';
            cfgStat.statistic        = 'ft_statfun_depsamplesT';
            cfgStat.correctm         = 'cluster';
            cfgStat.clusteralpha     = 0.05;
            cfgStat.clusterstatistic = 'maxsum';
            cfgStat.minnbchan        = 0;
            cfgStat.tail             = 0;
            cfgStat.clustertail      = 0;
            cfgStat.alpha            = 0.025;
            cfgStat.numrandomization = 500;
            cfgStat.parameter        = 'data';
            cfgStat.neighbours      = []; % no neighbors here 

            subj = size(data_forFT,1);
            conds = 2;
            design = zeros(2,conds*subj);
            for indCond = 1:conds
            for i = 1:subj
                design(1,(indCond-1)*subj+i) = indCond;
                design(2,(indCond-1)*subj+i) = i;
            end
            end
            cfgStat.design   = design;
            cfgStat.ivar     = 1;
            cfgStat.uvar     = 2;

            [stat{indFeature,indAge,indCued}] = ft_timelockstatistics(cfgStat, decode_forFT{1,:}, decode_forFT{2,:});
        end
    end
end

% average across features within dimensionality
for indAge = 1:2
    DA_merged_1{indAge} = []; DA_merged_1{indAge} = squeeze(nanmean(DecTarget_cued{indAge}(:,1:4,1,:),2));
    DA_merged_2{indAge} = []; DA_merged_2{indAge} = squeeze(nanmean(DecTarget_cued{indAge}(:,1:4,2,:),2));
    DA_merged_3{indAge} = []; DA_merged_3{indAge} = squeeze(nanmean(DecTarget_cued{indAge}(:,1:4,3,:),2));
    DA_merged_4{indAge} = []; DA_merged_4{indAge} = squeeze(nanmean(DecTarget_cued{indAge}(:,1:4,4,:),2));
    DA_merged_uc_1{indAge} = []; DA_merged_uc_1{indAge} = squeeze(nanmean(DecTarget_uncued{indAge}(:,1:4,1,:),2));
    DA_merged_uc_2{indAge} = []; DA_merged_uc_2{indAge} = squeeze(nanmean(DecTarget_uncued{indAge}(:,1:4,2,:),2));
    DA_merged_uc_3{indAge} = []; DA_merged_uc_3{indAge} = squeeze(nanmean(DecTarget_uncued{indAge}(:,1:4,3,:),2));
    DA_merged_uc_4{indAge} = []; DA_merged_uc_4{indAge} = squeeze(nanmean(DecTarget_uncued{indAge}(:,1:4,4,:),2));
end
features = {'color', 'motion', 'size', 'luminance'};

h = figure('units','normalized','position',[.1 .1 .4 .15]);
set(gcf,'renderer','painters')
cla;
for indAge = 1
    for indCond = 1:4
        subplot(1,4,(indAge-1)*4+indCond);
        patches.timeVec = [0 5 8 12.9];
        patches.colorVec = [1 1 1;.9 .9 .9; 1 1 1; 1 .9 .9; 1 1 1; .9 .9 .9];
        for indP = 1:numel(patches.timeVec)-1
            p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                        [0 0 [100 100]], patches.colorVec(indP,:));
            p.EdgeColor = 'none';
        end; hold on;
        plot([0,12.9], [50,50], 'k--', 'LineWidth',2)
        statsCued = double(stat{indCond,indAge,1}.mask); statsCued(statsCued==0) = NaN;
        statsUncued = double(stat{indCond,indAge,2}.mask); statsUncued(statsUncued==0) = NaN;
        plot(time, 49.5.*statsCued, 'r', 'LineWidth',2)
        plot(time, 49.*statsUncued, 'k','LineWidth',2)
        curData = smoothts(squeeze(cat(1,eval(['DA_merged_',num2str(indCond), '{1}']),...
            eval(['DA_merged_',num2str(indCond), '{2}']))),'b',1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        dimPlot{indCond} = shadedErrorBar(time,nanmean(curData,1),standError, ...
            'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
        % add uncued trials
        curData = smoothts(squeeze(cat(1,eval(['DA_merged_uc_',num2str(indCond), '{1}']),...
            eval(['DA_merged_uc_',num2str(indCond), '{2}']))),'b',1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        dimPlot{indCond} = shadedErrorBar(time,nanmean(curData,1),standError, ...
            'lineprops', {'color', [.4 .4 .4] ,'linewidth', 1, 'linestyle', '--'}, 'patchSaturation', .25);
        ylim([48.5 54])
        if indAge == 1
            title(features{indCond});
        else
            xlabel('Time (TR) from stim onset');
        end
        if indCond ==1
            ylabel('Decoding accuracy (%)')
        end
        xlim([0 12.9])
    end
end
set(findall(gcf,'-property','FontSize'),'FontSize',18)

figureName = 'G6D_winOption_VC_byFeature_acrossGroups';
saveas(h, fullfile(pn.plotFolder, figureName), 'png');
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');