function G1_extractTimeSeriesVisualCortex(rootpath, id)
    % extract temporal traces from all voxels in bilateral visual cortex mask to
    % subject to decoding analysis
    
    pn.tools = fullfile(rootpath, '..', 'extended_preproc', 'tools');
        addpath(fullfile(pn.tools, 'nifti_toolbox'));
        addpath(fullfile(pn.tools, 'preprocessing_tools'));
    pn.standards = fullfile(rootpath, 'data', 'A_standards');
    pn.BOLD = fullfile(rootpath, '..', 'extended_preproc', 'data', 'data_clean');
    pn.regressors = fullfile(rootpath, '..', 'design_timing', 'data', 'regressors');
    pn.out = fullfile(rootpath, 'data', 'B_singleTrialV5');
    
    pn.vcMask = fullfile(pn.standards, 'VisualCortex_unified_thr_MNI_3mm.nii');
    [vcMask] = double(S_load_nii_2d(pn.vcMask));
    
    if strcmp(id, '2131') || strcmp(id, '2237')
        numOfRuns = 2;
    else
        numOfRuns = 4;
    end
        
    disp(['Processing subject ', id, '.']);

    TrialSignal = []; 
    for indRun = 1:numOfRuns

        % load nifti file for this run; %(x by y by z by time)
        % check for nii or nii.gz, unzip .gz and reshape to 2D

        % if proc on tardis: change path to tardis preproc directory
       
        fileName = [id, '_run-',num2str(indRun),'_regressed.nii'];
        fname = fullfile(pn.BOLD, fileName);
        
        if ~exist(fname)
            warning(['File not available: ', id, ' Run ', num2str(indRun)]);
            continue;
        end
        
        % If using preprocessing tools
        [img] = double(S_load_nii_2d(fname));
        
        % get time series within visual cortex mask
        vcsignal = img(vcMask==1,:);
        
        %% cut into trials
               
        load(fullfile(pn.regressors, [id, '_Run',num2str(indRun),'_regressors.mat']))
        
        if strcmp(id, '2132') && indRun == 2
            Regressors(1041-20:end) = [];
        end
        
        % get stimulus onsets
        onset_samples = find(Regressors(:,3));
        for indOnset = 1:numel(onset_samples)            
            TrialSignal((indRun-1)*64+indOnset,:,:) = vcsignal(:,onset_samples(indOnset):onset_samples(indOnset)+20);
        end
        
    end
    
    save(fullfile(pn.out, [id, '_VC_allVox.mat']), 'TrialSignal')
        