% output is trial-wise decoding accurace of attribute-specific prevalence
% discrimination
% trained across trials from all dims, excl. test trial

clear all; clc;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

pn.data = fullfile(pn.root, 'data', 'G_DecodingResults');
    addpath(fullfile(pn.root, 'tools', 'shadedErrorBar'))
    addpath(fullfile(pn.root, 'tools', 'BrewerMap'))
    addpath(genpath(fullfile(pn.root, 'tools', 'RainCloudPlots')))
pn.trlinfo = fullfile(pn.root, 'data', 'C_trialInfo');
pn.fieldtrip = fullfile(pn.root, 'tools', 'fieldtrip');
    addpath(pn.fieldtrip); ft_defaults;
pn.figures = fullfile(pn.root, 'figures', 'G_decode');
    addpath(genpath(fullfile(pn.root, 'tools', 'plot2svg')))

cBrew = brewermap(4,'RdBu');

% N = 42 YAs
IDs{1} = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

% N = 51 OAs (2131, 2237 removed due to missing run)
% 2139, 2227 marked as anatomical anomalies 
IDs{2} = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227'; '2236';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

DA_merged = cell(1,2);
for indAge = 1:2
    for id = 1:numel(IDs{indAge})
        ID = IDs{indAge}{id};
        curFile = fullfile(pn.data, [ID, '_g6d_accuracy.mat']);
        if ~exist(curFile)
            disp(['File missing: ', ID])
            DA_merged{indAge}(id,:,:,:,:,:) = NaN;
            continue;
        end
        load(curFile);
        DA_merged{indAge}(id,:,:,:,:,:) = DA_end;
        % load trial assignment to split into categories of interest
        load(fullfile(pn.trlinfo, [ID,'_TrlInfo_mat.mat']))
        for indAtt = 1:4
            for indDim = 1:4
                curTrials = TrlInfo(:,6) == indAtt & TrlInfo(:,8) == indDim;
                DecTarget_probed{indAge}(id,indDim,indAtt,:) = squeeze(nanmean(DA_end(curTrials,indAtt,1,2,:),1));
                % dec acc for feature x when feature x was NOT probed
                curTrials = TrlInfo(:,6) ~= indAtt & TrlInfo(:,8) == indDim;
                DecTarget_unprobed{indAge}(id,indDim,indAtt,:) = squeeze(nanmean(DA_end(curTrials,indAtt,1,2,:),1));
                % on trials on which attribute x was probed, was there
                % evidence for attributes y? There should be at increasing
                % loads as attributes y have also been cued
                curTrials = TrlInfo(:,6) == indAtt & TrlInfo(:,8) == indDim;
                DecTarget_probed_other{indAge}(id,indDim,indAtt,:) = squeeze(nanmean(nanmean(DA_end(curTrials,~ismember([1:4],indAtt),1,2,:),1),2));
                curTrials = TrlInfo(:,indAtt) == 1 & TrlInfo(:,8) == indDim;
                DecTarget_cued{indAge}(id,indDim,indAtt,:) = squeeze(nanmean(DA_end(curTrials,indAtt,1,2,:),1));
                curTrials = TrlInfo(:,indAtt) == 1 & TrlInfo(:,6) ~= indAtt & TrlInfo(:,8) == indDim;
                DecTarget_cued_unprobed{indAge}(id,indDim,indAtt,:) = NaN(1,21);
                if indDim > 1
                    DecTarget_cued_unprobed{indAge}(id,indDim,indAtt,:) = squeeze(nanmean(DA_end(curTrials,indAtt,1,2,:),1));
                end
%                 curTrials = TrlInfo(:,indAtt) == 1 & TrlInfo(:,6) ~= indAtt & TrlInfo(:,8) == indDim;
%                 DecTarget_cued_unprobed{indAge}(id,indDim,indAtt,:) = squeeze(nanmean(DA_end(curTrials,indAtt,1,2,:),1));
                curTrials = isnan(TrlInfo(:,indAtt)) & TrlInfo(:,8) == indDim;
                DecTarget_uncued{indAge}(id,indDim,indAtt,:) = squeeze(nanmean(DA_end(curTrials,indAtt,1,2,:),1));
                curTrials = TrlInfo(:,8) == indDim;
                DecTarget_all{indAge}(id,indDim,indAtt,:) = squeeze(nanmean(DA_end(curTrials,indAtt,1,2,:),1));
            end
        end
    end
end

%% plot summary statistics by age

h = figure('units','normalized','position',[.1 .1 .2 .15]);
subplot(1,2,1); cla; hold on;
%plot([0,4], [50,50], 'k--', 'LineWidth',2)
for indAge = 1:2
    curData = squeeze(nanmean(nanmean(DecTarget_probed{indAge}(:,:,1:4,7:19),4),3));
    condAverage = nanmean(curData,2);
    curData = curData-condAverage+repmat(nanmean(condAverage,1),size(condAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    errorbar(nanmean(curData,1),standError, 'color', cBrew(indAge,:), 'linewidth', 2)
%     dimPlot{indAge} = shadedErrorBar(1:4,nanmean(curData,1),standError, ...
%         'lineprops', {'color', cBrew(indAge,:),'linewidth', 2}, 'patchSaturation', .25);
    ylim([50 52.5])
    xlabel('Target #'); ylabel('Decoding accuracy (%)')
    xlim([.5 4.5])
end
title('Probed')

subplot(1,2,2); cla; hold on;
%plot([0,4], [50,50], 'k--', 'LineWidth',2)
for indAge = 1:2
    curData = squeeze(nanmean(nanmean(DecTarget_unprobed{indAge}(:,:,1:4,7:19),4),3));
    condAverage = nanmean(curData,2);
    curData = curData-condAverage+repmat(nanmean(condAverage,1),size(condAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    errorbar(nanmean(curData,1),standError, 'color', cBrew(indAge,:), 'linewidth', 2)
%     dimPlot{indAge} = shadedErrorBar(1:4,nanmean(curData,1),standError, ...
%         'lineprops', {'color', cBrew(indAge,:),'linewidth', 2}, 'patchSaturation', .25);
    ylim([50 51])
    xlabel('Target #'); ylabel('Decoding accuracy (%)')
    xlim([.5 4.5])
end
title('Unprobed')

set(findall(gcf,'-property','FontSize'),'FontSize',15)

% figureName = 'G6D_winOption_VC_loadeffects';
% saveas(h, fullfile(pn.figures, figureName), 'epsc');

%% plot main effect of age using errorbars

h = figure('units','normalized','position',[.1 .1 .05 .15]);
cla; hold on;
    curData1 = squeeze(nanmean(nanmean(DecTarget_all{1}(:,:,1:4,7:19),4),3));
    condAverage1 = nanmean(curData1,2);
    curData2 = squeeze(nanmean(nanmean(DecTarget_all{2}(:,:,1:4,7:19),4),3));
    condAverage2 = nanmean(curData2,2);
    standError1 = nanstd(condAverage1,1)./sqrt(size(condAverage1,1));
    standError2 = nanstd(condAverage2,1)./sqrt(size(condAverage2,1));
    errorbar([nanmean(condAverage1,1),nanmean(condAverage2,1)],...
        [standError1,standError2], ...
        'color', 'k', 'linewidth', 2)
    ylim([50.3 51])
    xlabel('age'); ylabel('Decoding accuracy (%)')
    xlim([.5 2.5])
    title('All')
    set(findall(gcf,'-property','FontSize'),'FontSize',15)

    figureName = 'G6D_agediff';
    saveas(h, fullfile(pn.figures, figureName), 'epsc');
    saveas(h, fullfile(pn.figures, figureName), 'png');

%% plot main age effect using bar charts with individual values

colorm = [.7 .7 .7; 1 .6 .6; .6 .8 1];
plot_data{1} = squeeze(nanmean(nanmean(nanmean(DecTarget_all{1}(:,:,1:4,7:19),4),3),2));
plot_data{2} = squeeze(nanmean(nanmean(nanmean(DecTarget_all{2}(:,:,1:4,7:19),4),3),2));

    h = figure('units','centimeters','position',[.1 .1 7 10]);
    set(gcf,'renderer','Painters')
    plot([0,3], [50,50], 'k--', 'LineWidth',2)
    cla;
    hold on;
    for indGroup = 1:2
        if ttest(plot_data{indGroup})==1 & nanmean(plot_data{indGroup})>0
            bar(indGroup, nanmean(plot_data{indGroup}), 'FaceColor', colorm(2,:), 'EdgeColor', 'none', 'BarWidth', 0.8);
        elseif ttest(plot_data{indGroup})==1 & nanmean(plot_data{indGroup})<0
            bar(indGroup, nanmean(plot_data{indGroup}), 'FaceColor', colorm(3,:), 'EdgeColor', 'none', 'BarWidth', 0.8);
        elseif ttest(plot_data{indGroup})==0
            bar(indGroup, nanmean(plot_data{indGroup}), 'FaceColor', colorm(1,:), 'EdgeColor', 'none', 'BarWidth', 0.8);
        end
        bar(indGroup, 50, 'FaceColor', colorm(1,:), 'EdgeColor', 'none', 'BarWidth', 0.8);
        % plot individual values on top
        scatter(repmat(indGroup,numel(plot_data{indGroup}),1)+(rand(numel(plot_data{indGroup}),1)-.5).*.4,...
            plot_data{indGroup}, 20, 'filled', 'MarkerFaceColor', [.3 .3 .3]);
    end
    [htest, p] = ttest2(plot_data{1}, plot_data{2});
    if p>10^-3
        title(['p = ', num2str(round(p,3))]);
    else
        tit = sprintf('p = %.3e',p);
        title(tit);
    end

    curData1 = squeeze(nanmean(nanmean(DecTarget_all{1}(:,:,1:4,7:19),4),3));
    condAverage1 = nanmean(curData1,2);
    curData2 = squeeze(nanmean(nanmean(DecTarget_all{2}(:,:,1:4,7:19),4),3));
    condAverage2 = nanmean(curData2,2);
    standError1 = nanstd(condAverage1,1)./sqrt(size(condAverage1,1));
    standError2 = nanstd(condAverage2,1)./sqrt(size(condAverage2,1));
    errorbar([nanmean(condAverage1,1),nanmean(condAverage2,1)],...
        [standError1,standError2], ...
        'color', 'k', 'linewidth', 3)
    xlim([.25 2.75]); ylim([49 54])
    set(gca,'xtick',[1,2]); set(gca,'xTickLabel',{'YA'; 'OA'});
    xlabel('Age Group'); ylabel('Decoding accuracy (%)')
    set(findall(gcf,'-property','FontSize'),'FontSize',20)

    figureName = 'G6D_agediff_individual';
    saveas(h, fullfile(pn.figures, figureName), 'epsc');
    saveas(h, fullfile(pn.figures, figureName), 'png');

%% plot average across groups

DecTarget_all_twogroups = cat(1,DecTarget_all{1},DecTarget_all{2});
DecTarget_probed_twogroups = cat(1,DecTarget_probed{1},DecTarget_probed{2});
DecTarget_unprobed_twogroups = cat(1,DecTarget_unprobed{1},DecTarget_unprobed{2});

h = figure('units','normalized','position',[.1 .1 .3 .15]);
set(gcf,'renderer','Painters')

subplot(1,3,1); cla; hold on;
curData = squeeze(nanmean(nanmean(DecTarget_all_twogroups(:,:,1:4,7:19),4),3));
condAverage = nanmean(curData,2);
curData = curData-condAverage+repmat(nanmean(condAverage,1),size(condAverage,1),1);
standError = nanstd(curData,1)./sqrt(size(curData,1));
errorbar(nanmean(curData,1),standError, 'color', 'k', 'linewidth', 2)
ylim([50.3 51])
ylabel('Decoding accuracy (%)')
xlim([.5 4.5])
title('all')

subplot(1,3,2); cla; hold on;
curData = squeeze(nanmean(nanmean(DecTarget_probed_twogroups(:,:,1:4,7:19),4),3));
condAverage = nanmean(curData,2);
curData = curData-condAverage+repmat(nanmean(condAverage,1),size(condAverage,1),1);
standError = nanstd(curData,1)./sqrt(size(curData,1));
errorbar(nanmean(curData,1),standError, 'color', 'k', 'linewidth', 2)
ylim([50.5 52])
xlabel('Target #');
xlim([.5 4.5])
title('Probed')

X = [1 1; 1 2; 1 3; 1 4]; probedChange = curData/X';

subplot(1,3,3); cla; hold on;
curData = squeeze(nanmean(nanmean(DecTarget_unprobed_twogroups(:,:,1:4,7:19),4),3));
condAverage = nanmean(curData,2);
curData = curData-condAverage+repmat(nanmean(condAverage,1),size(condAverage,1),1);
standError = nanstd(curData,1)./sqrt(size(curData,1));
errorbar(nanmean(curData,1),standError, 'color', 'k', 'linewidth', 2)
ylim([50 51])
xlim([.5 4.5])
title('Unprobed')

X = [1 1; 1 2; 1 3; 1 4]; unprobedChange = curData/X';

%scatter(unprobedChange(:,2), probedChange(:,2), 'filled')

set(findall(gcf,'-property','FontSize'),'FontSize',15)

figureName = 'G6D_winOption_VC_loadeffects_groupavg';
saveas(h, fullfile(pn.figures, figureName), 'epsc');

%% plot average across groups (aligned axes)

DecTarget_all_twogroups = cat(1,DecTarget_all{1},DecTarget_all{2});
DecTarget_probed_twogroups = cat(1,DecTarget_probed{1},DecTarget_probed{2});
DecTarget_unprobed_twogroups = cat(1,DecTarget_unprobed{1},DecTarget_unprobed{2});

h = figure('units','normalized','position',[.1 .1 .3 .15]);
set(gcf,'renderer','Painters')

subplot(1,3,1); cla; hold on;
    curData = squeeze(nanmean(nanmean(DecTarget_probed_twogroups(:,:,1:4,7:18),4),3));
    condAverage = nanmean(curData,2);
    curData = curData-condAverage+repmat(nanmean(condAverage,1),size(condAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    errorbar(nanmean(curData,1),standError, 'color', 'k', 'linewidth', 2)
    ylim([50 52])
    yticks([50,52])
    xlabel('Target #');
    xlim([.5 4.5])
    title('Probed')
    ylabel('Decoding accuracy (%)')

    X = [1 1; 1 2; 1 3; 1 4]; probedChange = curData/X';

subplot(1,3,2); cla; hold on;
    curData = squeeze(nanmean(nanmean(DecTarget_unprobed_twogroups(:,:,1:4,7:18),4),3));
    condAverage = nanmean(curData,2);
    curData = curData-condAverage+repmat(nanmean(condAverage,1),size(condAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    errorbar(nanmean(curData,1),standError, 'color', 'k', 'linewidth', 2)
    ylim([50 52])
    yticks([])
    xlim([.5 4.5])
    title('Unprobed')

    X = [1 1; 1 2; 1 3; 1 4]; unprobedChange = curData/X';

subplot(1,3,3); cla; hold on;

    plot_data{1} = squeeze(nanmean(nanmean(nanmean(DecTarget_all{1}(:,:,1:4,7:18),4),3),2));
    plot_data{2} = squeeze(nanmean(nanmean(nanmean(DecTarget_all{2}(:,:,1:4,7:18),4),3),2));

    curData1 = squeeze(nanmean(nanmean(DecTarget_all{1}(:,:,1:4,7:18),4),3));
    condAverage1 = nanmean(curData1,2);
    curData2 = squeeze(nanmean(nanmean(DecTarget_all{2}(:,:,1:4,7:18),4),3));
    condAverage2 = nanmean(curData2,2);
    standError1 = nanstd(condAverage1,1)./sqrt(size(condAverage1,1));
    standError2 = nanstd(condAverage2,1)./sqrt(size(condAverage2,1));
    errorbar([nanmean(condAverage1,1),nanmean(condAverage2,1)],...
        [standError1,standError2], ...
        'color', 'k', 'linewidth', 3)
    xlim([.25 2.75]); ylim([50 52])
    yticks([])
    set(gca,'xtick',[1,2]); set(gca,'xTickLabel',{'YA'; 'OA'});
    xlabel('Age Group');
    set(findall(gcf,'-property','FontSize'),'FontSize',20)

set(findall(gcf,'-property','FontSize'),'FontSize',15)

figureName = 'G6D_winOption_VC_loadeffects_groupavg_samescale';
saveas(h, fullfile(pn.figures, figureName), 'epsc');

%% plot YA & OA in same plot with our features

h = figure('units','normalized','position',[.1 .1 .1 .15]);
set(gcf,'renderer','Painters')
	cla; hold on;
    curData = squeeze(nanmean(nanmean(DecTarget_all{1}(:,:,1:4,7:18),4),3));
    condAverage = nanmean(curData,2);
    curData = curData-condAverage+repmat(nanmean(condAverage,1),size(condAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    errorbar(nanmean(curData,1),standError, 'color', 'k', 'linewidth', 2)
    curData = squeeze(nanmean(nanmean(DecTarget_all{2}(:,:,1:4,7:18),4),3));
    condAverage = nanmean(curData,2);
    curData = curData-condAverage+repmat(nanmean(condAverage,1),size(condAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    errorbar(nanmean(curData,1),standError, 'color', 'r', 'linewidth', 2)
    ylim([50 52])
    yticks([50,52])
    xlabel('Target #');
    xlim([.5 4.5])
    title('All')
    ylabel('Decoding accuracy (%)')

set(findall(gcf,'-property','FontSize'),'FontSize',15)

figureName = 'G6D_winOption_VC_loadeffects_byage';
saveas(h, fullfile(pn.figures, figureName), 'epsc');

%% plot as raincloudplots: probed

colorm = [0.0314, 0.3176, 0.6118; 2.*[.3 .1 .1]; 2.*[.3 .1 .1]];

h = figure('units','normalized','position',[.1 .1 .15 .2]);
set(gcf,'renderer','Painters')
hold on;

curData =[];
curData{1} = squeeze(nanmean(nanmean(DecTarget_probed_twogroups(:,:,1:4,7:18),4),3));
curData{2} = squeeze(nanmean(nanmean(DecTarget_unprobed_twogroups(:,:,1:4,7:18),4),3));

% read into cell array of the appropriate dimensions
data = []; data_ws = [];
for i = 1:4
    for j = 1:2
        data{i, j} = squeeze(curData{j}(:,i));
        % individually demean for within-subject visualization
        data_ws{i, j} = curData{j}(:,i)-...
            nanmean(curData{j}(:,:),2)+...
            repmat(nanmean(nanmean(curData{j}(:,:),2),1),size(curData{j}(:,:),1),1);
    end
end

% IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

cl = colorm(1:2,:);

box off
h_rc = rm_raincloud(data_ws, cl,1);
view([90 -90]);
axis ij
box(gca,'off')
set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
ylabel('Target load'); xlabel('Decoding')
set(findall(gcf,'-property','FontSize'),'FontSize',20)
curYTick = get(gca, 'YTick'); ylim([curYTick(1)-.5*(curYTick(2)-curYTick(1)) curYTick(4)+.5*(curYTick(2)-curYTick(1))]);

%% age contrast

colorm = [0.0314, 0.3176, 0.6118; 2.*[.3 .1 .1]; 2.*[.3 .1 .1]];

h = figure('units','normalized','position',[.1 .1 .15 .2]);
set(gcf,'renderer','Painters')

hold on;
%plot([50,50],[0.5,1.5], 'k--', 'LineWidth',2)

curData =[];
curData{1} = squeeze(nanmean(nanmean(nanmean(DecTarget_all{1}(:,:,1:4,7:18),4),3),2));
curData{2} = squeeze(nanmean(nanmean(nanmean(DecTarget_all{2}(:,:,1:4,7:18),4),3),2));

% read into cell array of the appropriate dimensions
data = []; data_ws = [];
for i = 1
    for j = 1:2
        data{i, j} = squeeze(curData{j}(:,i));
        % individually demean for within-subject visualization
        data_ws{i, j} = curData{j}(:,i)-...
            nanmean(curData{j}(:,:),2)+...
            repmat(nanmean(nanmean(curData{j}(:,:),2),1),size(curData{j}(:,:),1),1);
    end
end

% IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

cl = colorm(1:2,:);

box off
h_rc = rm_raincloud(data, cl,1);
view([90 -90]);
axis ij
box(gca,'off')
xlabel('Decoding')
xlim([48.5, 54])
set(findall(gcf,'-property','FontSize'),'FontSize',20)
curYTick = get(gca, 'YTick'); ylim([curYTick(1)-.5*(curYTick(2)-curYTick(1)) curYTick(4)+.5*(curYTick(2)-curYTick(1))]);

figureName = 'G6D_winOption_VC_age_rcp';
saveas(h, fullfile(pn.figures, figureName), 'epsc');