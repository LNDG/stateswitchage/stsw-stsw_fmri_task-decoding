% This script combines results from visual cortex decoding with results
% from somatosensory decoding.

clear all; clc;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.data = fullfile(rootpath, 'data', 'G_DecodingResults');
    addpath(fullfile(rootpath, 'tools', 'shadedErrorBar'))
    addpath(fullfile(rootpath, 'tools', 'BrewerMap'))
pn.trlinfo = fullfile(rootpath, 'data', 'C_trialInfo');
% pn.fieldtrip = fullfile(rootpath, 'tools', 'fieldtrip');
%     addpath(pn.fieldtrip); ft_defaults;
pn.plotFolder = fullfile(rootpath, 'figures', 'G_decode');
    addpath(genpath(fullfile(rootpath, 'tools', 'plot2svg')))
    
% 2131, 2237 removed due to missing runs
filename = fullfile(rootpath, 'code', 'id_list_mr_completeruns.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
tmp_IDs = IDs{1};

IDs{1} = tmp_IDs(cellfun(@str2num, tmp_IDs, 'un', 1)<2000);
IDs{2} = tmp_IDs(cellfun(@str2num, tmp_IDs, 'un', 1)>2000);

Somatomotor = cell(1,2);
for indAge = 1:2
    for id = 1:numel(IDs{indAge})
        ID = IDs{indAge}{id};
        curFile = fullfile(pn.data, [ID, '_g6d_accuracy.mat']);
        if ~exist(curFile)
            disp(['File missing: ', ID])
            Somatomotor{indAge}(id,:,:,:,:,:) = NaN;
            continue;
        end
        load(curFile);
        if size(DA_end,1)==128 % if only the initial two runs are available
            DA_end = cat(1, DA_end, NaN(size(DA_end)));
        end
        Somatomotor{indAge}(id,:,:,:,:,:) = DA_end;
        % load trial assignment to split into categories of interest
        load(fullfile(pn.trlinfo, [ID,'_TrlInfo_mat.mat']))
        for indAtt = 1:4
            for indDim = 1:4
                curTrials = TrlInfo(:,indAtt) == 1 & TrlInfo(:,8) == indDim;
                DecTarget_cued{indAge}(id,indDim,indAtt,:) = squeeze(nanmean(DA_end(curTrials,indAtt,1,2,:),1));
                curTrials = isnan(TrlInfo(:,indAtt)) & TrlInfo(:,8) == indDim;
                DecTarget_uncued{indAge}(id,indDim,indAtt,:) = squeeze(nanmean(DA_end(curTrials,indAtt,1,2,:),1));
                curTrials = TrlInfo(:,8) == indDim;
                DecTarget_all{indAge}(id,indDim,indAtt,:) = squeeze(nanmean(DA_end(curTrials,indAtt,1,2,:),1));
            end
        end
    end
end

% average across features within dimensionality
for indAge = 1:2
    Visual_uncued{indAge} = []; Visual_uncued{indAge} = squeeze(nanmean(nanmean(DecTarget_uncued{indAge}(:,1:4,:,:),3),2));
    Visual_cued{indAge} = []; Visual_cued{indAge} = squeeze(nanmean(nanmean(DecTarget_cued{indAge}(:,1:4,:,:),3),2));
    Visual_all{indAge} = []; Visual_all{indAge} = squeeze(nanmean(nanmean(DecTarget_all{indAge}(:,1:4,:,:),3),2));
end
Visual_uncued{3} = cat(1,Visual_uncued{1},Visual_uncued{2});
Visual_cued{3} = cat(1,Visual_cued{1},Visual_cued{2});
Visual_all{3} = cat(1,Visual_all{1},Visual_all{2});

%% load results from somatosensory decoding

Somatomotor = cell(1,2);
for indAge = 1:2
    for id = 1:numel(IDs{indAge})
        curFile = fullfile(pn.data, [IDs{indAge}{id}, '_DecodeWinOptionD1_SM_correct.mat']);
        if ~exist(curFile)
            disp(['File missing: ', IDs{indAge}{id}])
            Somatomotor{indAge}(id,:,:,:,:) = NaN;
            continue;
        end
        load(curFile);
        Somatomotor{indAge}(id,:,:,:,:) = DA_end;
    end
end

for indAge = 1:2
    Somatomotor_all{indAge} = []; Somatomotor_all{indAge} = squeeze(nanmean(Somatomotor{indAge}(:,1:4,:,:,:),2));
end
Somatomotor_all{3} = cat(1,Somatomotor_all{1},Somatomotor_all{2});

%% plotting visual and motor decoding in the same plot
% Note: slightly different decoder setups

cBrew = brewermap(4,'RdBu');

time = ([1:21]-1).*.645;

h = figure('units','normalized','position',[.1 .1 .15 .2]);
set(gcf,'renderer','opengl')
    cla; hold on;
    patches.timeVec = [0 5 8 12.9];
    patches.colorVec = [1 1 1;.9 .9 .9; 1 1 1; 1 .9 .9; 1 1 1; .9 .9 .9];
    for indP = 1:numel(patches.timeVec)-1
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [0 0 [100 100]], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end; hold on;
    plot([0,12.9], [50,50], 'k--', 'LineWidth',2)
    for indAge = 3
        curData = smoothts(squeeze(eval(['Visual_cued{indAge}'])),'b',1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        dimPlot{1} = shadedErrorBar(time,nanmean(curData,1),standError, ...
            'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
        
        curData = smoothts(squeeze(nanmean(nanmean(eval(['Somatomotor_all{indAge}']),2),3)),'b',1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        dimPlot{2} = shadedErrorBar(time,nanmean(curData,1),standError, ...
            'lineprops', {'color', cBrew(4,:),'linewidth', 2}, 'patchSaturation', .25);
        
        % plot rescaled visual version to motor
        tmpData = nanmean(curData,1);
        curData = smoothts(squeeze(eval(['Visual_cued{indAge}'])),'b',1);
        curData = nanmean(curData,1);
        % Rescale the curData to lie between the minimum and maximum of tmpData
        rescaledCurData = rescale(curData, min(tmpData), max(tmpData));
        standError(:) = 0;
        dimPlot{3} = shadedErrorBar(time,rescaledCurData,standError, ...
            'lineprops', {'color', cBrew(1,:),'linewidth', 1, 'linestyle', ':'}, 'patchSaturation', .25);

        ylim([49.5 82])
        xlabel('Time (s) from stim onset'); ylabel('Decoding accuracy (%)')
        xlim([0 12.9])
    end
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    legend([dimPlot{1}.mainLine, dimPlot{2}.mainLine],...
            {'Visual'; 'Motor'}, 'orientation', 'vertical', 'location', 'NorthWest')
    legend('boxoff')

figureName = 'g6d_visuomotor';
saveas(h, fullfile(pn.plotFolder, figureName), 'png');
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
