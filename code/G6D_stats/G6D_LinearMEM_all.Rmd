---
title: "StateSwitch Linear MEM All"
output:
  html_document:
    fig_crop: no
  pdf_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)

#install.packages('R.matlab')
#install.packages('lmSupport')
#install.packages("rlang") 
#install.packages("here") 
#install.packages("devtools") 
#install.packages("sjPlot") 
#install.packages("patchwork")
#install.packages("influence.ME")
library(here)
library(R.matlab)
library(knitr)
library(lme4)
library(lmerTest)
library(influence.ME)
#library(devtools)
library(sjPlot)
#library(strengejacke)
library(patchwork)

options(digits = 2)
opts_knit$set(global.par=TRUE)
```
```{r, include=FALSE}
par(mar=c(5,5,0,0))
```

# read the data for the ANOVA from MATLAB output

```{r getData}
rm(list = ls()) # clear workspace

rootpath = here()
data = readMat(file.path(rootpath, 'data', 'G_DecodingResults', 'G6D_decode4R_all.mat'))
data = as.data.frame(matrix(unlist(data), nrow=7, byrow=T),stringsAsFactors=TRUE, header = TRUE)
data = t(data) # transpose matrix
data = data.frame(data) # convert to data frame
names(data) = as.character(unlist(data[1, ]))
data = data[-1, ]
```

# create numeric or categorical variables

```{r dataRead}
data$ID = as.factor(data$ID)
data$age = as.factor(data$age)
data$feature = as.factor(data$feature)
#data$dim = as.factor(data$dim)
data$dim = as.numeric(sub(",", ".", data$dim, fixed = TRUE))
#data$time = as.numeric(sub(",", ".", data$time, fixed = TRUE))
data$time = as.factor(data$time)
data$dec_acc = as.numeric(sub(",", ".", data$dec_acc, fixed = TRUE))
data$rt = as.numeric(sub(",", ".", data$rt, fixed = TRUE))

# change contrast to ya as reference group
data$age = C(data$age, contr.treatment, base = 2)
# set type III sums of squares as default
options(contrasts = c("contr.sum","contr.poly"))
```

# select subset of data (by timepoint)

```{r selectTime}

curTimes = as.factor(c(7:18))
curData = data[which(data$time %in% curTimes), ]

# List of predictor variables to aggregate across
predictor_vars <- c("dim", "age")
# Create the formula for aggregation
formula <- as.formula(paste(". ~ ID +", paste(predictor_vars, collapse = " + ")))
# Averaging the data across time points, excluding the 'time' variable
averaged_data <- aggregate(formula, data = curData, FUN = mean)

```

## linear mixed effects models

Here, we probe the data for an effect of dimensionality on decoding accuracy, as well as a potential age*dim interaction.

```{r  , echo = FALSE,  out.width="50%", fig.align = "center"}
acc_dim_age <-lmerTest::lmer(dec_acc ~ dim*age + (1|ID), data=curData)
summary(acc_dim_age)
anova(acc_dim_age, type = 3)
```

```{r  , echo = FALSE,  out.width="50%", fig.align = "center"}
plot_model(acc_dim_age, type = "int")
```
# Exploratory: Rerun following removal of Cook's distance outliers
```{r  , echo = FALSE,  out.width="50%", fig.align = "center"}
plot(acc_dim_age)
cooksd <- cooks.distance(acc_dim_age)
# Plot the Cook's Distance using the traditional 4/n criterion
n <- nrow(curData)
plot(cooksd, pch="*", cex=2, main="Influential Obs by Cooks distance")  # plot cook's distance
abline(h = 5*mean(cooksd), col="red")  # add cutoff line

#identify influential points
influential_obs <- (cooksd > 5*mean(cooksd))

#define new data frame with influential points removed
outliers_removed <- curData[influential_obs==FALSE, ]

acc_dim_age <-lmerTest::lmer(dec_acc ~ dim*age + (1|ID), data=outliers_removed)
summary(acc_dim_age)
anova(acc_dim_age)
plot_model(acc_dim_age, type="int")
```
## now models, where we do not estimate the interaction

```{r  , echo = FALSE,  out.width="50%", fig.align = "center"}
acc_dim_age <-lmerTest::lmer(dec_acc ~ dim+age + (1|ID), data=curData)
summary(acc_dim_age)
anova(acc_dim_age)
```
```{r  , echo = FALSE,  out.width="50%", fig.align = "center"}
#plot_model(acc_dim_age, type="eff")
```
# Exploratory: Rerun following removal of Cook's distance outliers
```{r  , echo = FALSE,  out.width="50%", fig.align = "center"}
plot(acc_dim_age)
cooksd <- cooks.distance(acc_dim_age)
# Plot the Cook's Distance using the traditional 4/n criterion
n <- nrow(curData)
plot(cooksd, pch="*", cex=2, main="Influential Obs by Cooks distance")  # plot cook's distance
abline(h = 5*mean(cooksd), col="red")  # add cutoff line

#identify influential points
influential_obs <- (cooksd > 5*mean(cooksd))

#define new data frame with influential points removed
outliers_removed <- curData[influential_obs==FALSE, ]

acc_dim_age <-lmerTest::lmer(dec_acc ~ dim+age + (1|ID), data=outliers_removed)
summary(acc_dim_age)
anova(acc_dim_age)
# plot_model(acc_dim_age, type="eff")
```

## now a model that includes RTs

```{r  , echo = FALSE,  out.width="50%", fig.align = "center"}
acc_dim_age_rt <- lmerTest::lmer(dec_acc ~ dim + age + rt + (1|ID), data = averaged_data)
summary(acc_dim_age_rt)
anova(acc_dim_age_rt)
```
