%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ADVANCED SETTINGS SEGMENTATION, HEADMODELS, LEADFIELDS: %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% This settings file will be read by the BuilderGUI.
% - Settings here will be loaded into the Builder .mat and applied to analysis.
%
% Usage:
%  a)  Settings under "DO NOT EDIT" are handled by [MEG]PLS. Do not alter these.
%  b)  Change other values as needed. See "help" command for FieldTrip section.
%  c)  Optional settings can be uncommented and specified.
%
function [cfgSegMRI, cfgHdm, cfgLead] = Settings_SegHdmLeadfield(InputFTcfg)

% DO NOT EDIT:
cfgHdm  = InputFTcfg.Hdm;
cfgLead = InputFTcfg.Lead;



%--- "FT_VOLUMESEGMENT" SETTINGS FOR MRI SEGMENT: ---%
%----------------------------------------------------%

% DO NOT EDIT:
cfgSegMRI.spmversion = 'spm8';
SpmPath = spm('dir');

% GENERAL SETTINGS:
cfgSegMRI.output   = 'brain';                        % 'tpm', 'brain', 'skull', 'gray', 'white', 'skullstrip', 'scalp'
cfgSegMRI.template = [SpmPath,'/templates/T1.nii'];  % Filename of template anatomical MRI.

% SMOOTH & THRESHOLD SETTINGS:
cfgSegMRI.downsample     = 1;       % Integer # to downsample (Default = 1; no downsampling).
cfgSegMRI.brainsmooth    = 5;
cfgSegMRI.scalpsmooth    = 5;
cfgSegMRI.brainthreshold = 0.5;     % Can lower thresholds if MRI noisy.
cfgSegMRI.scalpthreshold = 0.5;

% OPTIONAL SETTINGS:
%  cfgSegMRI.write = 'no';          % Write to SPM8-NIFTI file for mask.
%  cfgSegMRI.name  = ''             % String for filename if write = 'yes'.
%  cfgSegMRI.tpm   = {}             % Cell-array of filenames for tissue-probability maps.



%--- "FT_PREPARE_HEADMODEL" SETTINGS: ---%
%----------------------------------------%

% DO NOT EDIT:
cfgHdm.method = InputFTcfg.Hdm.method;
cfgHdm.grad   = [];

% GENERAL SETTINGS:
cfgHdm.tissue    = cfgSegMRI.output;         % SegMRI output used for headmodel.

% LOCALSPHERE SETTINGS:
cfgHdm.radius    = 9;
cfgHdm.maxradius = 25;
cfgHdm.feedback  = 'no';

% OPTIONAL LOCALSPHERE SETTINGS:
%  cfgHdm.baseline

% OTHER OPTIONAL SETTINGS:
%  cfgHdm.conductivity
%  cfgHdm.hdmfile



%--- "FT_PREPARE_LEADFIELD" SETTINGS: ---%
%----------------------------------------%

% DO NOT EDIT:
cfgLead.grid.resolution = InputFTcfg.Lead.grid.resolution;
cfgLead.reducerank      = 2;    % Required for MEG.
cfgLead.vol             = [];
cfgLead.channel         = 'MEG';
cfgLead.grid.unit       = 'mm';

% GENERAL SETTINGS:
cfgLead.grid.xgrid     = 'auto';
cfgLead.grid.ygrid     = 'auto';
cfgLead.grid.zgrid     = 'auto';
cfgLead.normalize      = 'no';
cfgLead.normalizeparam = 0.5;

% OPTIONAL ROI SETTINGS (Replaces xgrid, ygrid, and zgrid):
%  cfgLead.grid.pos
%  cfgLead.grid.dim
%  cfgLead.grid.inside
%  cfgLead.grid.outside

% OTHER OPTIONAL SETTINGS:
%  cfgLead.grad                 % Note: Normally present in the data.
%  cfgLead.gradfile             % Note: Normally present in the data.
