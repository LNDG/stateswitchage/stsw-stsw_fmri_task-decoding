%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ADVANCED SETTINGS FOR TIMELOCK / FREQ ANALYSIS: %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% This settings file will be read by the BuilderGUI.
% - Settings here will be applied to timelocked and frequency analysis.
%
% Usage:
%  a)  Settings under "DO NOT EDIT" are handled by [MEG]PLS. Do not alter these.
%  b)  Change other values as needed. See "help" command for FieldTrip section.
%  c)  Optional settings can be uncommented and specified.
%  d)  For frequency analysis, choose only one method (comment out others).
%
function [cfgTimelock, cfgTLAvg, cfgGFP, cfgFreq, cfgFreqAvg] = Settings_TimelockFreqAnalysis(handles)

% DO NOT EDIT:
InputFTcfg = handles.FTcfg;
time       = handles.time;
gui        = handles.gui;

cfgTimelock = InputFTcfg.Timelock;

if isfield(InputFTcfg, 'Freq')
    cfgFreq = InputFTcfg.Freq;
end



%--- "FT_TIMELOCKANALYSIS" SETTINGS: ---%
%---------------------------------------%

% DO NOT EDIT:
cfgTimelock.removemean       = InputFTcfg.Timelock.removemean;
cfgTimelock.covariancewindow = InputFTcfg.Timelock.covariancewindow;
cfgTimelock.channel          = 'MEG';

% GENERAL SETTINGS:
cfgTimelock.covariance   = 'yes';   % Note: Auto-enabled for source-filter calculation.
cfgTimelock.keeptrials   = 'no';    % Note: Auto-disabled for source-filter calculation.
cfgTimelock.trials       = 'all';
cfgTimelock.vartrllength = 0;



%--- "FT_TIMELOCKGRANDAVERAGE" SETTINGS: ---%
%-------------------------------------------%

% DO NOT EDIT:
cfgTLAvg.channel = 'MEG';
cfgTLAvg.latency = 'all';

% GENERAL SETTINGS:
cfgTLAvg.method         = 'across';  % Averaging across subjects.
cfgTLAvg.parameter      = 'avg';
cfgTLAvg.keepindividual = 'no';
cfgTLAvg.normalizevar   = 'N-1';



%--- "FT_GLOBALMEANFIELD" SETTINGS: ---%
%--------------------------------------%

% DO NOT EDIT:
cfgGFP.channel = 'MEG';

% GENERAL SETTINGS:
cfgGFP.method = 'power';



%--- "FT_FREQANALYSIS" SETTINGS: ---%
%-----------------------------------%

% DO NOT EDIT:
cfgFreq.channel = 'MEG';
cfgFreq.method  = InputFTcfg.Freq.method;

if strcmp(InputFTcfg.Freq.method, 'mtmfft')
    cfgFreq.foilim = [gui.FreqStart, gui.FreqEnd];
else
    cfgFreq.foi = [gui.FreqStart : gui.FreqRes : gui.FreqEnd];
end

if ismember(InputFTcfg.Freq.method, {'mtmconvol', 'wavelet'})
    cfgFreq.toi = [time.Start : time.Step : time.End];
end

switch gui.FreqMethod
    case 'mtmconvol: Fixed Time Win.'
        WindowSize = 1 / gui.FreqRes;
        cfgFreq.t_ftimwin = ones(length(cfgFreq.foi),1) .* WindowSize;
        
    case 'mtmconvol: Freq. Dep. Time Win.'
        DesiredNumCycles = 7;       % Exception: Can edit the number of cycles here.
        cfgFreq.t_ftimwin = DesiredNumCycles ./ cfgFreq.foi;
end


    % IMPORTANT: FOR THE FIXED TIME-WINDOW METHOD, SIZE OF TIME-WINDOW DETERMINES FREQUENCY RESOLUTION.
    % - Ex: If you have a 500 ms time-window, you have a 2 Hz frequency resolution (1/0.5s).
    %
    % IF USING A FIXED TIME-WINDOW METHOD, CHECK THE SIZE OF YOUR TIME-WINDOW:
    % - An integer number of oscillatory cycles MUST fit in the time-window specified.


% GENERAL SETTINGS:
cfgFreq.output     = 'pow';         % 'pow', 'powandcsd', or 'fourier' (Note: 'powandcsd' auto-enabled for source-filters)
cfgFreq.trials     = 'all';
cfgFreq.keeptrials = 'no';          % Note: Auto-disabled for source-filter calculation.
cfgFreq.keeptapers = 'no';


    % FOR DICS SOURCE ANALYSIS ON MTMFFT DATA, CONFIGURATION CAN BE SET AS FOLLOWS:
    % See summary below to configure freq. analysis settings for DICS.
    %
    % Link to FT tutorial:
    % fieldtrip.fcdonders.nl/tutorial/beamformer
    %
    % EXAMPLE: If interested in DICS source analysis for 40-70Hz.
    % - In the GUI, set the start & end frequency to 55Hz (middle of frequency range).
    %   In source analysis adv. settings, set cfg.frequency for DICS beamformer to 55Hz as well.
    %
    % - Set "tapsmofrq" (smoothing) to accomodate the desired frequency band.
    %   For the example above, set "tapsmofrq" to 15Hz (smoothing window of +/- 15).
    %   Make sure that taper type is set to "dpss" or "tapsmofrq" will have no effect.
    %
    % - Amount of smoothing is dependent on desired frequency band.
    %   In general, for higher frequencies, you want to increase the width of the smoothing.


% FOR MTMFFT OR MTMCONVOL METHODS:
cfgFreq.taper = 'hanning';  % 'hanning' or 'dpss'

if strcmp(cfgFreq.taper, 'dpss')
    if strcmp(cfgFreq.method, 'mtmfft')  % FOR MTMFFT
        cfgFreq.tapsmofrq = 4;           % Note: Value of 1 is effectively no smoothing.
        
    elseif strcmp(cfgFreq.method, 'mtmconvol')  % FOR MTMCONVOL
        cfgFreq.tapsmofrq = 0.4 * cfgFreq.foi;  % Note: Smoothing increases with frequency.
                                                % Use smaller values for less smoothing.
    end
end

    
    % "tapsmofrq" is the amount of spectral smoothing through dpss multi-tapering.
    % - For "mtmfft": This should be a single number.
    % - For "mtmconvol": This is a vector (1 x NumFOI).
    %
    % Smoothing window will be +/- the value specified.
    % - Ex: If "tapsmofrq" = 4, then smoothing window is an 8 Hz box (+/- 4).
    %
    % Example: For DICS source analysis, if interested in frequency band of 40 - 70 Hz:
    % - Set frequency of interest (cfg.foi) to 55 Hz with a smoothing window of +/- 15 Hz.
    %   Don't forget to set cfgSource.frequency to 55 Hz as well.
    %
    % Note: For time-freq analyses, for lower frequencies (i.e < 15 Hz), Hanning window is recommended over dpss.
    % - Smoothing for dpss may not be applicable for lower frequencies.
    % - Ex: If running "mtmfft" with foi of 2-30Hz, cannot smooth by more than 2Hz.
    %
    % For more info, see: http://fieldtrip.fcdonders.nl/tutorial/timefrequencyanalysis     


% FOR WAVELET OR TFR METHODS:
cfgFreq.width  = 7;  % Number of cycles of the wavelet (Default = 7).
cfgFreq.gwidth = 3;  % (Default = 3)


% OPTIONAL SETTINGS:
%  cfgFreq.pad
%  cfgFreq.padtype
%  cfgFreq.polyremoval
%  cfgFreq.channelcmb = {'MEG', 'MEG'};



%--- "FT_FREQGRANDAVERAGE" SETTINGS: ---%
%---------------------------------------%

% DO NOT EDIT:
cfgFreqAvg.channel = 'MEG';
cfgFreqAvg.foilim  = 'all';
cfgFreqAvg.toilim  = 'all';

% GENERAL SETTINGS:
cfgFreqAvg.parameter      = 'powspctrm';
cfgFreqAvg.keepindividual = 'no';
