%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Wrapper for source filter computation.             %
% Inputs: Processed MEG data, Headmodels, Leadfield. %
% Last modified: Jan. 15, 2014                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Copyright (C) 2013-2014, Michael J. Cheung
%
% This file is a part of the MEG & PLS Pipeline (MEGPLS). For more 
% details, see the documentation included with the software package.
%
% MEGPLS is free software: you can redistribute it and/or modify it under
% the terms of the GNU General Public License version 2 as published by 
% the Free Software Foundation. This program is distributed in the hope 
% that it will be useful, but WITHOUT ANY WARRANTY; without even the 
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, you can download the license here: 
% <http://www.gnu.org/licenses/old-licenses/gpl-2.0>.


function MEGpipeline_SourceFilter_Wrap(BuilderMat)


% Make sure java paths added:
UseProgBar = CheckParforJavaPaths;


% Load builder .mat file:
Builder      = load(BuilderMat);
name         = Builder.name;
paths        = Builder.paths;
FTcfg        = Builder.FTcfg;
PipeSettings = Builder.PipeSettings;

SourceContrast     = PipeSettings.Source.Contrast;
EstNoiseCommonFilt = PipeSettings.Source.EstNoiseCommonFilt;


% Clear existing Errorlog & Diary:
if exist('ErrorLog_SourceFilter.txt', 'file')
    system('rm ErrorLog_SourceFilter.txt');
end
if exist('Diary_SourceFilter.txt', 'file')
    system('rm Diary_SourceFilter.txt');
end

diary Diary_SourceFilter.txt
ErrLog = fopen('ErrorLog_SourceFilter.txt', 'a');


% Check gradiometer info:
GradMismatch = CheckGradInfo(Builder, 'SourceFilter');

if GradMismatch == 1
    open('ErrorLog_SourceFilter.txt');
    error('ERROR: Custom-filter(s) cannot be applied due to mismatched grad. info. See ErrorLog.')
end



%============================================%
% COMPUTE COMMON-FILTER FOR SOURCE-ANALYSIS: %
%============================================%

for g = 1:length(name.GroupID)
    NumSubj = length(name.SubjID{g});
    
    if UseProgBar == 1
        ppm = ParforProgMon...
            (['COMPUTING SOURCE FILTERS:  ',name.GroupID{g},'.  '], NumSubj, 1, 700, 80);
    end
    
    parfor s = 1:length(name.SubjID{g})
        
        
        %--- EstNoise with Custom-Filter: ---%
        %------------------------------------%

        % Compute filter from custom datasets and apply to each condition:
        if strcmp(SourceContrast, 'EstNoise') && strcmp(EstNoiseCommonFilt, 'yes')
            
            Headmodel = paths.Hdm{g}{s,1};
            Leadfield = paths.Lead{g}{s,1};
            MEGdata   = paths.MEGdataCommonFilt{g}{s};
            
            MEGpipeline_SourceFilter_Guts(PipeSettings, FTcfg, Headmodel, Leadfield, ...
                MEGdata, paths.SourceCovCsd{g}{s,1}, paths.SourceFilter{g}{s,1})
            
            CovCsd       = LoadFTmat(paths.SourceCovCsd{g}{s,1}, 'SourceFilter');
            SourceFilter = LoadFTmat(paths.SourceFilter{g}{s,1}, 'SourceFilter');
            if isempty(CovCsd) || isempty(SourceFilter)
                continue;
            end
            
            for c = 1:length(name.CondID)
                CheckSavePath(paths.SourceCovCsd{g}{s,c}, 'SourceFilter');
                ParforSaveCovCsd(paths.SourceCovCsd{g}{s,c}, CovCsd)
                
                CheckSavePath(paths.SourceFilter{g}{s,c}, 'SourceFilter');
                ParforSaveFilter(paths.SourceFilter{g}{s,c}, SourceFilter)
            end
            
            CovCsd       = [];  % Free memory
            SourceFilter = [];
        end
        
        
        %--- EstNoise (No custom-filter) or DiffBase: ---%
        %------------------------------------------------%
        
        % Compute filter separately for each dataset.
        if strcmp(SourceContrast, 'DiffBase') || ...
                (strcmp(SourceContrast, 'EstNoise') && strcmp(EstNoiseCommonFilt, 'no'))
            for c = 1:length(name.CondID)
                
                Headmodel = paths.Hdm{g}{s,c};
                Leadfield = paths.Lead{g}{s,c};
                MEGdata   = paths.MEGdata{g}{s,c};
                
                MEGpipeline_SourceFilter_Guts(PipeSettings, FTcfg, Headmodel, Leadfield, ...
                    MEGdata, paths.SourceCovCsd{g}{s,c}, paths.SourceFilter{g}{s,c})
                
            end  % Cond
        end
        
        
        %--- DiffCond with Common-Filter: ---%
        %------------------------------------%

		% For each active dataset, merge with control for common-filter:
		% Note: Filter for control not saved since it is same as merged.
        if strcmp(SourceContrast, 'DiffCond') && GradMismatch == 0
            for c = 1:length(name.CondID)
                
                Headmodel = paths.Hdm{g}{s,c};
                Leadfield = paths.Lead{g}{s,c};
                
                CheckInput1 = CheckPipelineMat(paths.MEGdataControl{g}{s}, 'SourceFilter');
                CheckInput2 = CheckPipelineMat(paths.MEGdata{g}{s,c},      'SourceFilter');
                
                if CheckInput1 == 1 && CheckInput2 == 1  % Success
                    cfgMerge              = [];
                    cfgMerge.inputfile{1} = paths.MEGdata{g}{s,c}
                    cfgMerge.inputfile{2} = paths.MEGdataControl{g}{s};
                    
                    MEGdataMerged = ft_appenddata(cfgMerge);
                else
                    continue;
                end
                
                MEGpipeline_SourceFilter_Guts(PipeSettings, FTcfg, Headmodel, Leadfield, ...
                    MEGdataMerged, paths.SourceCovCsd{g}{s,c}, paths.SourceFilter{g}{s,c});
                
                MEGdataMerged = [];  % Free memory
                cfgMerge      = [];
                
            end  % Cond
        end
        
        if UseProgBar == 1 && mod(s, 1) == 0
            ppm.increment();  % move up progress bar
        end
        
    end  % Subj
    if UseProgBar == 1
        ppm.delete();
    end
    
end  % Group



%=========================%
% CHECK FOR OUTPUT FILES: %
%=========================%

for g = 1:length(name.GroupID)
	for s = 1:length(name.SubjID{g})
		for c = 1:length(name.CondID)
            
            if ~exist(paths.SourceFilter{g}{s,c}, 'file')
                fprintf(ErrLog, ['ERROR: Output source filter missing:'...
                    '\n %s \n\n'], paths.SourceFilter{g}{s,c});
            end
            
		end  % Cond
	end  % Subj
end  % Group



%=================%

if exist([pwd,'/ErrorLog_SourceFilter.txt'], 'file')
    LogCheck = dir('ErrorLog_SourceFilter.txt');
    if LogCheck.bytes ~= 0  % File not empty
        open('ErrorLog_SourceFilter.txt');
    else
        delete('ErrorLog_SourceFilter.txt');
    end
end

fclose(ErrLog);
diary off

end



function ParforSaveCovCsd(OutputPath, CovCsd)
    CovCsd;
    save(OutputPath, 'CovCsd');
end

function ParforSaveFilter(OutputPath, SourceFilter)
    SourceFilter;
    save(OutputPath, 'SourceFilter');
end
