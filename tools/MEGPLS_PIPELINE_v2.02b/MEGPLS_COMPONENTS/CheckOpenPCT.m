%====================================================%
% Check if parallel computing toolbox is available.  %
% If so, ask user and open a worker pool if desired. %
% Last modified: Sept. 16, 2014                      %
%====================================================%

% Copyright (C) 2013-2014, Michael J. Cheung
%
% This file is a part of the MEG & PLS Pipeline (MEGPLS). For more 
% details, see the documentation included with the software package.
%
% MEGPLS is free software: you can redistribute it and/or modify it under
% the terms of the GNU General Public License version 2 as published by 
% the Free Software Foundation. This program is distributed in the hope 
% that it will be useful, but WITHOUT ANY WARRANTY; without even the 
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, you can download the license here: 
% <http://www.gnu.org/licenses/old-licenses/gpl-2.0>.


function CheckOpenPCT


% Check if user has Parallel Computing Toolbox: 
Toolboxes = ver;

if any(strcmp('Parallel Computing Toolbox', {Toolboxes.Name}))
    PCTavail = 1;
else
    PCTavail = 0;
    return;
end


% If matlab pool is already open, return:
if matlabpool('size') > 0
    return;
end


% Ask user and open matlab pool:
if PCTavail == 1 && matlabpool('size') == 0
    prompt = {'MATLAB Parallel Computing Toolbox has been detected.';
        'If available, [MEG]PLS supports parallel processing.'; '';
        'Do you wish to open a worker pool for parallel computation?'; ''};
    
    OpenPool = questdlg(prompt, 'PARALLEL COMPUTING:', 'YES', 'NO', 'NO');
    if strcmp(OpenPool, 'NO')
        return;
    end
    
    CheckLicense = license('inuse', 'Parallel Computing Toolbox');
    if isempty(CheckLicense)
        license('checkout', 'Parallel Computing Toolbox');
    end
    
    NumWorkers = inputdlg...
        ('Enter # of Workers for Pool:', 'Num. Workers:', 1, {'2'});
    
    if isempty(NumWorkers)
        return;  % If user cancels
    else
        NumWorkers = str2num(NumWorkers{1});
    end
    
    if isempty(NumWorkers) || length(NumWorkers) > 1
        error('Error: Input must be a single number.')
    end
    
    matlabpool('OPEN', NumWorkers)
end
            
    