%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Reads PLS input data & settings, and calls PLS analysis scripts. %
% Last modified: Jan. 15, 2014                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Copyright (C) 2013-2014, Michael J. Cheung
%
% This file is a part of the MEG & PLS Pipeline (MEGPLS). For more 
% details, see the documentation included with the software package.
%
% MEGPLS is free software: you can redistribute it and/or modify it under
% the terms of the GNU General Public License version 2 as published by 
% the Free Software Foundation. This program is distributed in the hope 
% that it will be useful, but WITHOUT ANY WARRANTY; without even the 
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, you can download the license here: 
% <http://www.gnu.org/licenses/old-licenses/gpl-2.0>.


function MEGpipeline_RunPLS(PLSmatReady, PLSrunSettings)

% Load PLSmat and PLSrun settings:
PLSrun = PLSrunSettings;
PLSmat = PLSmatReady;
name   = PLSmat.name;
time   = PLSmat.time;
paths  = PLSmat.paths;

% Clear existing Errorlog & Diary:
if exist('ErrorLog_RunPLS.txt', 'file')
    system('rm ErrorLog_RunPLS.txt');
end
if exist('ErrorLog_PlotPLSresLV.txt', 'file')
    system('rm ErrorLog_PlotPLSresLV.txt');
end
if exist('Diary_RunPLS.txt', 'file')
    system('rm Diary_RunPLS.txt');
end

diary Diary_RunPLS.txt
ErrLog = fopen('ErrorLog_RunPLS.txt', 'a');



%===============================%
% GET PARAMETERS FOR PLS INPUT: %
%===============================%

NumSubj  = cellfun(@numel, name.SubjID);
NumCond  = length(name.CondID);
NumGroup = length(name.GroupID);

for g = 1:length(name.GroupID)
    PLSmat.Datamat{g} = double(PLSmat.Datamat{g});
end

switch PLSrun.Method
	case 'MCENT'
		Method     = 1;
		NRContrast = '';
		Bhvmat	   = '';
	case 'NROT'
		Method     = 2;
		NRContrast = [PLSrun.NRContrast]';  % Transmute for input
		Bhvmat     = '';
	case 'BHV'
		Method     = 3;
		NRContrast = '';
		Bhvmat	   = double(PLSmat.Bhvmat);
        NumBhv     = size(Bhvmat, 2);
	case 'MBLOCK'
		Method     = 4;
		NRContrast = '';
		Bhvmat     = double(PLSmat.Bhvmat);
        NumBhv     = size(Bhvmat, 2);
    case 'NR-BHV'
        Method     = 5;
        NRContrast = [PLSrun.NRContrast]';
        Bhvmat     = double(PLSmat.Bhvmat);
        NumBhv     = size(Bhvmat, 2);
    case 'NR-MBLOCK'
        Method     = 6;
        NRContrast = [PLSrun.NRContrast]';
        Bhvmat     = double(PLSmat.Bhvmat);
        NumBhv     = size(Bhvmat, 2);
end

if strcmp(PLSrun.SplitHalf, 'no')
    PLSrun.NumSplit = 0;
end

if strcmp(PLSmat.MaskType, 'BrainMask')
    MaskIndices = PLSmat.BrainMaskFlatIndices;
elseif strcmp(PLSmat.MaskType, 'ROIMask')
    MaskIndices = PLSmat.ROIMaskFlatIndices;
end

% Check non-rotated contrast:
switch PLSrun.Method
    case 'NROT'
        ContrastSize = (NumGroup * NumCond);
        if size(PLSrun.NRContrast, 2) ~= ContrastSize
            fprintf(ErrLog, ['ERROR: Size of NR-contrast should be equal to:'...
                '\n (NumGroup * NumCond): %s \n\n'], num2str(ContrastSize));
            error('ERROR: Incorrect NROT contrast size. See ErrorLog.');
        end
        
    case 'NR-BHV'
        ContrastSize = (NumGroup * NumCond * NumBhv);
        if size(PLSrun.NRContrast, 2) ~= ContrastSize
            fprintf(ErrLog, ['ERROR: Size of NR-contrast should be equal to:'...
                '\n (NumGroup * NumCond * NumBhv): %s \n\n'], num2str(ContrastSize));
            error('ERROR: Incorrect NROT contrast size. See ErrorLog.');
        end
        
    case 'NR-MBLOCK'
        ContrastSize = (NumGroup * NumCond + NumGroup * NumCond * NumBhv);
        if size(PLSrun.NRContrast, 2) ~= ContrastSize
            fprintf(ErrLog, ['ERROR: Size of NR-contrast should be equal to:'...
                '\n (NumGroup * NumCond + NumGroup * NumCond * NumBhv): %s \n\n'],...
                num2str(ContrastSize));
            error('ERROR: Incorrect NROT contrast size. See ErrorLog.');
        end
end



%==================================%
% GENERATE OUTPUT STRINGS & PATHS: %
%==================================%

% Group, Subj, Cond strings:
GrpStr  = [num2str(NumGroup),'grp'];
SubjStr = [regexprep(num2str(NumSubj), '\s+', '+'),'subj'];
CondStr = [num2str(NumCond),'cond'];
DataStr = [GrpStr,'_',SubjStr,'_',CondStr];

% Time parameter string:
if strcmp(PLSmat.ManualTimeSelect, 'no')
    BeginIndex = PLSmat.TimeIndices(1);
    EndIndex   = PLSmat.TimeIndices(end);
	TimeStr    = [time.str.Windows{BeginIndex,1},'s_',time.str.Windows{EndIndex,2},'s'];
else
	TimeStr = [num2str(length(PLSmat.TimeIndices)),'TimeIndices'];
end

% Behavior string:
BhvStr = [];
if isfield(PLSmat, 'Bhvmat')
    NumBhvs = size(PLSmat.Bhvmat, 2);
	BhvStr  = ['_',num2str(NumBhvs),'bhv_',PLSmat.BhvSeedID];
end

% String for non-rotated contrast ID:
if ismember(PLSrun.Method, {'NROT', 'NR-BHV', 'NR-MBLOCK'})
    NRContrastStr = ['_',PLSrun.NRContrastID];
end

% String for mean-centering type:
if ismember(PLSrun.Method, {'MCENT', 'NROT', 'MBLOCK', 'NR-MBLOCK'})
    MCTypeStr = ['_MCtype',num2str(PLSrun.MCentType)];
end

% String or number of perms, boots, and splits:
NumPerm  = num2str(PLSrun.NumPerm);
NumBoot  = num2str(PLSrun.NumBoot);
NumSplit = num2str(PLSrun.NumSplit); 

if strcmp(PLSrun.SplitHalf, 'no')
    PermBootStr = [NumPerm,'p',NumBoot,'b'];
else
    PermBootStr = [NumPerm,'p',NumBoot,'b',NumSplit,'s'];
end


% Generate paths for PLS output folder:
PLSrun.SessionFolder = ...
	[paths.AnalysisID,'/PLS_',DataStr,'_',TimeStr,'_',PLSmat.MaskType,'_',PLSmat.SessionID];

switch PLSrun.Method
    case 'MCENT'
		PLSrun.OutFolder = [PLSrun.SessionFolder,'/',PLSrun.Method];
        
    case 'NROT'
		PLSrun.OutFolder = [PLSrun.SessionFolder,'/',PLSrun.Method,NRContrastStr];
		
	case 'BHV'
		PLSrun.OutFolder = [PLSrun.SessionFolder,'/',PLSrun.Method,BhvStr];
		
	case 'NR-BHV'
		PLSrun.OutFolder = [PLSrun.SessionFolder,'/',PLSrun.Method,BhvStr,NRContrastStr];
		
	case 'MBLOCK'
		PLSrun.OutFolder = [PLSrun.SessionFolder,'/',PLSrun.Method,BhvStr];

	case 'NR-MBLOCK'
		PLSrun.OutFolder = [PLSrun.SessionFolder,'/',PLSrun.Method,BhvStr,NRContrastStr];
end

% Generate PLS output filenames:
switch PLSrun.Method
    case {'MCENT', 'NROT'}
        PLSrun.OutName = [PermBootStr,MCTypeStr];
        
	case {'BHV', 'NR-BHV'}
		if strcmp(PLSrun.SingleBhv, 'no')
			PLSrun.OutName = PermBootStr;
		else
			for b = 1:NumBhv
				PLSrun.OutName{b} = [PermBootStr,'_',PLSmat.BhvNames{b}];
			end
		end
		
	case {'MBLOCK', 'NR-MBLOCK'}
		if strcmp(PLSrun.SingleBhv, 'no')
			PLSrun.OutName = [PermBootStr,MCTypeStr];
		else
			for b = 1:NumBhv
				PLSrun.OutName{b} = [PermBootStr,MCTypeStr,'_',PLSmat.BhvNames{b}];
			end
		end
end


% Check if files exist:
if exist(PLSrun.OutFolder, 'dir')
    CheckDir = dir(PLSrun.OutFolder);
    CheckDir = {CheckDir.name};
    CheckDir(ismember(CheckDir, {'.', '..'})) = [];
    
    if ~isempty(CheckDir)
        prompt = {'WARNING:'; '';
            'Existing files detected in the output PLS folder:';
            [PLSrun.OutFolder]; '';
            'Do you wish to continue and overwrite previous results?'};
        
        Overwrite = questdlg(prompt, 'Warning:', 'Yes', 'No', 'No');
        if strcmp(Overwrite, 'No')
            msgbox('Note: Change the PLS Session-ID to change output folder.');
            return;
        end
    end

% Create output directory:
elseif ~exist(PLSrun.OutFolder, 'dir')
    [Status, ErrMsg] = mkdir(PLSrun.OutFolder);
	
    if Status == 0
        fprintf(ErrLog, ['ERROR: Failed to create output directory:'...
            '\n Reason: %s \n Folder: %s \n\n'], ErrMsg, PLSrun.OutFolder);
        error(ErrMsg);
    end
end



%=========================%
% RUN PLS COMMAND & SAVE: %
%=========================%

% Get BRIK info for WriteBrik function (called by savepls function):
WriteBrikInfo = PLSmat.BrikSampleInfo;
SaveBrikDims  = PLSmat.BrikDim4D;

if ~isequal(WriteBrikInfo.DATASET_RANK(2), length(PLSmat.TimeIndices))
    WriteBrikInfo.DATASET_RANK(2) = length(PLSmat.TimeIndices);
    
    if isfield(WriteBrikInfo, 'TAXIS_NUMS')
        WriteBrikInfo.TAXIS_NUMS(1)    = length(PLSmat.TimeIndices);
    end
    if isfield(WriteBrikInfo, 'BRICK_TYPES')
        WriteBrikInfo.BRICK_TYPES      = WriteBrikInfo.BRICK_TYPES(PLSmat.TimeIndices);
    end
    if isfield(WriteBrikInfo, 'BRICK_FLOAT_FACS')
        WriteBrikInfo.BRICK_FLOAT_FACS = WriteBrikInfo.BRICK_FLOAT_FACS(PLSmat.TimeIndices);
    end
    
    if length(SaveBrikDims) == 4  % If 4D file
        SaveBrikDims(4) = length(PLSmat.TimeIndices);
    end
end

    
%--- General PLS routine: ---%
%----------------------------%
if ismember(PLSrun.Method, {'MCENT', 'NROT'}) || strcmp(PLSrun.SingleBhv, 'no')
    
    % Load in options for PLS run:
    PLSoptions.method             = Method;
    PLSoptions.num_perm           = PLSrun.NumPerm;
    PLSoptions.num_boot           = PLSrun.NumBoot;
    PLSoptions.num_split          = PLSrun.NumSplit;
    PLSoptions.meancentering_type = PLSrun.MCentType;
    PLSoptions.is_struct          = PLSrun.PermType;
    PLSoptions.boot_type          = PLSrun.BootType;
    PLSoptions.cormode            = PLSrun.CorrMode;
    %PLSoptions.clim              = 95;
    %PLSoptions.bscan             = [1:NumCond];
    
    if ismember(PLSrun.Method, {'NROT', 'NR-BHV', 'NR-MBLOCK'})
        PLSoptions.stacked_designdata = NRContrast;
    end
    if ismember(PLSrun.Method, {'BHV', 'NR-BHV', 'MBLOCK', 'NR-MBLOCK'})
        PLSoptions.stacked_behavdata  = Bhvmat;
    end
	
    % Run PLS command:
    PLSresult = pls_analysis(PLSmat.Datamat, NumSubj, NumCond, PLSoptions);
    
    % Append # groups for plot & save functions:
    PLSresult.num_groups = NumGroup;
    
    
    % Clear existing files for overwrite:
    CheckDir = dir(PLSrun.OutFolder);
    CheckDir = {CheckDir.name};
    CheckDir(ismember(CheckDir, {'.', '..'})) = [];
    
    if ~isempty(CheckDir)
        system(['rm ',PLSrun.OutFolder,'/*']);
    end
	
    
	% Check for potentially significant LV's:
	% Note: If SigLV threshold changed, also change in save & plot fcns.
    if strcmp(PLSrun.SplitHalf, 'no')
        SigLVs = find(PLSresult.perm_result.sprob < 0.1);
        SigLVs = SigLVs';  % Change to horizontal so for LV loops properly.
        
    else
        SigLVs = [];
        NumLVs = size(PLSresult.u, 2);
        
        for LV = 1:NumLVs
            if (PLSresult.perm_splithalf.ucorr_prob(LV) < 0.1 ||...
                    PLSresult.perm_splithalf.vcorr_prob(LV) < 0.1 ||...
                    PLSresult.perm_result.sprob(LV) < 0.1)
                SigLVs = [SigLVs, LV];
            end
        end
    end
            
    if isempty(SigLVs)
        TextFile = ['NoSigLV_for_',PLSrun.OutName];
        TempID   = fopen(TextFile, 'a'); fclose(TempID);
        movefile(TextFile, [PLSrun.OutFolder,'/'], 'f');
    end
	
    
	% Plot results:
    TempName.GroupID = name.GroupID;
    TempName.CondID  = name.CondID;
    if ismember(PLSrun.Method, {'BHV', 'NR-BHV', 'MBLOCK', 'NR-MBLOCK'})
        TempName.BhvNames = PLSmat.BhvNames;
    end
    
	MEGpipeline_PlotPLSresLV...
        (PLSresult, PLSrun, TempName, PLSrun.OutFolder, PLSrun.OutName);
	
    
	% Save results:    
    CheckSavePath([PLSrun.OutFolder,'/PLSres_',PLSrun.OutName,'.mat'], 'RunPLS');
	save([PLSrun.OutFolder,'/PLSres_',PLSrun.OutName,'.mat'], 'PLSresult');
	
	if ~isempty(SigLVs)
        CheckSavePath(['BSRimg_',PLSrun.OutName,'+tlrc.BRIK'], 'RunPLS');
        
		save_pls_result_splithalf(PLSresult, PLSmat.Datamat, SaveBrikDims, MaskIndices,...
			0, 0, [-5000 5000], [-5000 5000], WriteBrikInfo, ...
            ['BSRimg_',PLSrun.OutName], ['SALimg_',PLSrun.OutName]);
        
        if ~isequal(pwd, PLSrun.OutFolder)
            movefile(['BSRimg_',PLSrun.OutName,'*'], [PLSrun.OutFolder,'/'], 'f');
            movefile(['SALimg_',PLSrun.OutName,'*'], [PLSrun.OutFolder,'/'], 'f');
        end
	end
	
	% Check if output exists:
	if ~exist([PLSrun.OutFolder,'/PLSres_',PLSrun.OutName,'.mat'], 'file')
		fprintf(ErrLog, ['ERROR: Output PLSresult .mat file is missing:'...
			'\n %s \n\n'], [PLSrun.OutFolder,'/PLSres_',PLSrun.OutName,'.mat']);
    end
    
    PLSresult  = [];  % Free memory
    PLSoptions = [];
	
end
	

%--- PLS command if one bhv at a time: ---%
%-----------------------------------------%
if strcmp(PLSrun.SingleBhv, 'yes') && ...
		ismember(PLSrun.Method, {'BHV', 'NR-BHV', 'MBLOCK', 'NR-MBLOCK'})
	for b = 1:NumBhv
        
        % Load in options for PLS run:
        PLSoptions.method             = Method;
        PLSoptions.num_perm           = PLSrun.NumPerm;
        PLSoptions.num_boot           = PLSrun.NumBoot;
        PLSoptions.num_split          = PLSrun.NumSplit;
        PLSoptions.meancentering_type = PLSrun.MCentType;
        PLSoptions.is_struct          = PLSrun.PermType;
        PLSoptions.boot_type          = PLSrun.BootType;
        PLSoptions.cormode            = PLSrun.CorrMode;
        %PLSoptions.clim              = 95;
        %PLSoptions.bscan             = [1:NumCond];
        
        if ismember(PLSrun.Method, {'NR-BHV', 'NR-MBLOCK'})
            PLSoptions.stacked_designdata = NRContrast;
        end
        if ismember(PLSrun.Method, {'BHV', 'NR-BHV', 'MBLOCK', 'NR-MBLOCK'})
            PLSoptions.stacked_behavdata  = [];
            PLSoptions.stacked_behavdata  = Bhvmat(:,b);
        end
        
        % Run PLS command:
        PLSresult = [];
        PLSresult = pls_analysis(PLSmat.Datamat, NumSubj, NumCond, PLSoptions);
        
        % Append # groups for plot & save functions:
        PLSresult.num_groups = NumGroup;
        
        
        % Clear existing files for overwrite:
        if b == 1  % Only clear on the first run.
            CheckDir = dir(PLSrun.OutFolder);
            CheckDir = {CheckDir.name};
            CheckDir(ismember(CheckDir, {'.', '..'})) = [];
            
            if ~isempty(CheckDir)
                system(['rm ',PLSrun.OutFolder,'/*']);
            end
        end
		
        % Check for potentially significant LV's:
        % Note: If SigLV threshold changed, also change in save & plot fcns.
        if strcmp(PLSrun.SplitHalf, 'no')
            SigLVs = find(PLSresult.perm_result.sprob < 0.1);
            SigLVs = SigLVs';  % Change to horizontal so "for LV = SigLVs" loops properly.
            
        else
            SigLVs = [];
            NumLVs = size(PLSresult.u, 2);
            
            for LV = 1:NumLVs
                if (PLSresult.perm_splithalf.ucorr_prob(LV) < 0.1 ||...
                        PLSresult.perm_splithalf.vcorr_prob(LV) < 0.1 ||...
                        PLSresult.perm_result.sprob(LV) < 0.1)
                    SigLVs = [SigLVs, LV];
                end
            end
        end
        
        if isempty(SigLVs)
            TextFile = ['NoSigLV_for_',PLSrun.OutName{b}];
            TempID   = fopen(TextFile, 'a'); fclose(TempID);
            movefile(TextFile, [PLSrun.OutFolder,'/'], 'f');
        end
        
		% Plot results:
        TempName = [];
        TempName.GroupID  = name.GroupID;
        TempName.CondID   = name.CondID;
        TempName.BhvNames = cellstr(PLSmat.BhvNames{b});
        
		MEGpipeline_PlotPLSresLV...
            (PLSresult, PLSrun, TempName, PLSrun.OutFolder, PLSrun.OutName{b});
    
		
		% Save results:        
        CheckSavePath([PLSrun.OutFolder,'/PLSres_',PLSrun.OutName{b},'.mat'], 'RunPLS');
        save([PLSrun.OutFolder,'/PLSres_',PLSrun.OutName{b},'.mat'], 'PLSresult');
		
		if ~isempty(SigLVs)
            CheckSavePath(['BSRimg_',PLSrun.OutName{b},'+tlrc.BRIK'], 'RunPLS');
            
			save_pls_result_splithalf(PLSresult, PLSmat.Datamat, SaveBrikDims, MaskIndices,...
				0, 0, [-5000 5000], [-5000 5000], WriteBrikInfo, ...
                ['BSRimg_',PLSrun.OutName{b}], ['SALimg_',PLSrun.OutName{b}]);
			
            if ~isequal(pwd, PLSrun.OutFolder)
                movefile(['BSRimg_',PLSrun.OutName{b},'*'], [PLSrun.OutFolder,'/'], 'f');
                movefile(['SALimg_',PLSrun.OutName{b},'*'], [PLSrun.OutFolder,'/'], 'f');
            end
		end
		
		% Check if output exists:
		if ~exist([PLSrun.OutFolder,'/PLSres_',PLSrun.OutName{b},'.mat'], 'file')
			fprintf(ErrLog, ['ERROR: Output PLSresult .mat file is missing:'...
				'\n %s \n\n'], [PLSrun.OutFolder,'/PLSres_',PLSrun.OutName{b},'.mat']);
        end
        
        PLSresult  = [];  % Free memory
        PLSoptions = [];
		
	end
end


%--- Rename "avgts" or "corrts" files: ---%
%-----------------------------------------%

% Change dir for 3drename:
CurrentDir = pwd;
cd([PLSrun.OutFolder,'/']);

% Rename group-average time-series files:
if ismember(PLSrun.Method, {'MCENT', 'NROT', 'MBLOCK', 'NR-MBLOCK'})
    
    for g = 1:length(name.GroupID)
        for c = 1:length(name.CondID)
            
            OldFile = ['BSRimg_',PLSrun.OutName,...
                '_group',num2str(g),'_cond',num2str(c),'_avgts+tlrc.BRIK'];
            
            NewFile = ['GrpAvgTS_',PLSrun.OutName,...
                '_',name.GroupID{g},'_',name.CondID{c},'+tlrc.BRIK'];
            
            if exist(OldFile, 'file')
                system(['3drename ',OldFile,' ',NewFile]);
                
                if ~exist(NewFile, 'file')
                    fprintf(ErrLog, ['ERROR: Failed to rename "avgts" file:'...
                        '\n OrigName: %s \n TargetName: %s \n'], OldFile, NewFile);
                end
            end
            
        end  % Cond
    end  % Group
    
end

% Rename correlation time-series files:
if ismember(PLSrun.Method, {'BHV', 'MBLOCK', 'NR-BHV', 'NR-MBLOCK'})
    
    for g = 1:length(name.GroupID)
        for c = 1:length(name.CondID)
            for b = 1:length(PLSmat.BhvNames)
                
                if strcmp(PLSrun.SingleBhv, 'no')
                    OldFile = ['BSRimg_',PLSrun.OutName,...
                        '_group',num2str(g),'_cond',num2str(c),...
                        '_behav',num2str(b),'_corrts+tlrc.BRIK'];
                    
                    NewFile = ['CorrTS_',PLSrun.OutName,...
                        '_',name.GroupID{g},'_',name.CondID{c},...
                        '_',PLSmat.BhvNames{b},'+tlrc.BRIK'];
                    
                elseif strcmp(PLSrun.SingleBhv, 'yes')
                    OutNameShort = strrep(PLSrun.OutName{b}, ['_',PLSmat.BhvNames{b}], '');
                    
                    OldFile = ['BSRimg_',PLSrun.OutName{b},...
                        '_group',num2str(g),'_cond',num2str(c),...
                        '_behav1_corrts+tlrc.BRIK'];  % Each run is only 1bhv.
                    
                    NewFile = ['CorrTS_',OutNameShort,...
                        '_',name.GroupID{g},'_',name.CondID{c},...
                        '_',PLSmat.BhvNames{b},'+tlrc.BRIK'];
                end
                
                if exist(OldFile, 'file')
                    system(['3drename ',OldFile,' ',NewFile]);
                    
                    if ~exist(NewFile, 'file')
                        fprintf(ErrLog, ['ERROR: Failed to rename "corrts" file:'...
                            '\n OrigName: %s \n TargetName: %s \n'], OldFile, NewFile);
                    end
                end
                
            end  % Bhv
        end  % Cond
    end  % Group
    
end

cd(CurrentDir);


%--- Save backup of settings: ---%
%--------------------------------%

% Save datamat (Warning: Uses tons of space)!!
if strcmp(PLSrun.KeepDatamat, 'yes')
    CheckSavePath([PLSrun.OutFolder,'/Datamat.mat'], 'RunPLS');
    save([PLSrun.OutFolder,'/Datamat.mat'], 'PLSmatReady');
end

% Save backup copy of Builder and PLS settings:
PLSmat.Datamat = [];
PLSmat.Bhvmat  = [];  % Only keep settings here

CheckSavePath([PLSrun.OutFolder,'/Backup_PLSmat_Settings.mat'], 'RunPLS');
save([PLSrun.OutFolder,'/Backup_PLSmat_Settings.mat'], 'PLSmat');

CheckSavePath([PLSrun.OutFolder,'/Backup_PLSrun_Settings.mat'], 'RunPLS');
save([PLSrun.OutFolder,'/Backup_PLSrun_Settings.mat'], 'PLSrun');



%==========================================%
% GENERATE TIME LEGEND FOR PLS .BRIK FILE: %
%==========================================%

if exist([PLSrun.OutFolder,'/BSRimg_TimeLegend.txt'], 'file')
    delete([PLSrun.OutFolder,'/BSRimg_TimeLegend.txt']);
end
TimeLegend = fopen('BSRimg_TimeLegend.txt', 'a');

fprintf(TimeLegend, ['/// IMPORTANT: ///'...
	'\n - In the AFNI-VIEWER, voxel and time indices start at 0.    '...
    '\n   In the data however, voxel and time indices start at 1. \n'...
    '\n - Therefore, when selecting indices for use outside of the  '...
    '\n   AFNI-viewer or AFNI functions, make sure you are using the'...
    '\n   data index, and NOT the AFNI-viewer index. \n\n']);

fprintf(TimeLegend, ['/// TIME LEGEND: ///'...
	'\n AFNI-Viewer ------- AFNI/NIFTI ------- TIME-Interval:'...
	'\n ViewerIndex ------- Data Index ------- In seconds:']);

CurrentIndex = 1;
for t = PLSmat.TimeIndices
    fprintf(TimeLegend, '\n---- %s --------------- %s ------------ %s',...
		num2str(CurrentIndex - 1), num2str(CurrentIndex), time.str.Windows{t,3});
    
    CurrentIndex = CurrentIndex + 1;
end

fclose(TimeLegend);
if ~isequal(pwd, PLSrun.OutFolder)
    movefile('BSRimg_TimeLegend.txt', [PLSrun.OutFolder,'/'], 'f');
end



%=================%

if exist([pwd,'/ErrorLog_RunPLS.txt'], 'file')
    LogCheck1 = dir('ErrorLog_RunPLS.txt');
    if LogCheck1.bytes ~= 0  % File not empty
        open('ErrorLog_RunPLS.txt');
    else
        delete('ErrorLog_RunPLS.txt');
    end
end

if exist([pwd,'/ErrorLog_PlotPLSresLV.txt'], 'file')
    LogCheck2 = dir('ErrorLog_PlotPLSresLV.txt');
    if LogCheck2.bytes ~=0  % File is not empty
        open('ErrorLog_PlotPLSresLV.txt');
    else
        delete('ErrorLog_PlotPLSresLV.txt');
    end
end

fclose(ErrLog);
diary off
