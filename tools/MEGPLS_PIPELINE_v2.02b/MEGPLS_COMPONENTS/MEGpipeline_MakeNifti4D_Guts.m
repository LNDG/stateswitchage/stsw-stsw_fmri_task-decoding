%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Merges list of NIFTI files into one 4D-NIFTI. %
% Last modified: Jan. 15, 2014                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Usage:
%  MEGpipeline_MakeNifti4D_Guts(MergeFileList, OutpathNifti4D)
%
% Inputs:
%  MergeFileList  = Cell-array of .nii files to be merged.
%  OutpathNifti4D = Output /path/filename of 4D-NIFTI.


% Copyright (C) 2013-2014, Michael J. Cheung
%
% This file is a part of the MEG & PLS Pipeline (MEGPLS). For more 
% details, see the documentation included with the software package.
%
% MEGPLS is free software: you can redistribute it and/or modify it under
% the terms of the GNU General Public License version 2 as published by 
% the Free Software Foundation. This program is distributed in the hope 
% that it will be useful, but WITHOUT ANY WARRANTY; without even the 
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, you can download the license here: 
% <http://www.gnu.org/licenses/old-licenses/gpl-2.0>.


function MEGpipeline_MakeNifti4D_Guts(MergeFileList, OutpathNifti4D)


% Check number of input arguments:
if nargin ~= 2
    error('ERROR: Incorrect number of input arguments. See help for usage info.')
end


% Check if all files exist:
MissingFiles = 0;

for f = 1:length(MergeFileList)
    if ~exist(MergeFileList{f}, 'file')
        disp(['ERROR: Missing file detected: ',MergeFileList{f}])
        MissingFiles = 1;
    end
end

if MissingFiles == 1
    return;
end


% Check save path early so if same directory, we don't remove newly created file.
% Similar to where we used CheckSavePath prior to volumewrite.
CheckSavePath(OutpathNifti4D, 'MakeNifti4D');


% Set parameters & run SPM merge:
[~, NameNifti4D, ~] = fileparts(OutpathNifti4D);

MatlabBatch						 = [];
MatlabBatch{1}.spm.util.cat.vols = MergeFileList;
MatlabBatch{1}.spm.util.cat.name = [pwd,'/',NameNifti4D,'.nii'];


% Get data type of source:
SampleFile = ft_read_mri(MergeFileList{1});
DataType   = class(SampleFile.anatomy);

switch DataType
    case 'uint8'  % unsigned
        MatlabBatch{1}.spm.util.cat.dtype = 2;
    case 'int16'
        MatlabBatch{1}.spm.util.cat.dtype = 4;
    case 'int32'
        MatlabBatch{1}.spm.util.cat.dtype = 8;
    case 'single'  % float32
        MatlabBatch{1}.spm.util.cat.dtype = 16;
    case 'double'  % float64
        MatlabBatch{1}.spm.util.cat.dtype = 64;
    otherwise
        MatlabBatch{1}.spm.util.cat.dtype = 64;
end


% Merge files into Nifti4D:
disp('Merging files into 4D-NIFTI:')
spm_jobman('initcfg');
spm_jobman('run', MatlabBatch)

if ~exist(MatlabBatch{1}.spm.util.cat.name, 'file')
    disp('ERROR: Failed to generate Nifti4D file.')
    return;
end

[DumpFolder, DumpName, ~] = fileparts(MatlabBatch{1}.spm.util.cat.name);
if exist([DumpFolder,'/',DumpName,'.mat'], 'file')
    system(['rm ',DumpFolder,'/',DumpName,'.mat']);  % Remove remnant .mat
end

if ~isequal(OutpathNifti4D, MatlabBatch{1}.spm.util.cat.name)
    Status = movefile(MatlabBatch{1}.spm.util.cat.name, OutpathNifti4D, 'f');
    
    if Status == 0
        disp('ERROR: Failed to move Nifti4D file to output directory.')
        return;
    end
end

