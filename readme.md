[![made-with-datalad](https://www.datalad.org/badges/made_with.svg)](https://datalad.org)

## Decoding prevalent on-screen features

We performed a decoding analysis to probe the extent to which participants’ visual cortices contained information about the prevalent option of each feature. In particular, we trained a decoder based on signals from across voxels in visual cortex. A visual cortical mask was defined based on Jülich parcellations, including visual areas ranging from V1 to area MT. We resliced the mask to 3mm, and created an intersection mask with the cortical grey matter mask also used throughout the remaining analyses. For classiﬁcation analyses, we used linear support-vector machines (SVM) (Muller, Mika, Ratsch, Tsuda, & Scholkopf, 2001) via the libsvm implementation (www.csie.ntu.edu.tw/~cjlin/libsvm). We trained classifiers on the basis of trials across cue dimensionalities to maximize trial counts for training. No separate session was recorded for classifier training.  Within each group, the minimum number of available trials for each feature option were randomly selected and split into leave-one-out training and test sets 100 times for cross-validation. This approach maximizes the available trials for each split. To increase the signal-to-noise ratio, we averaged three trials for each exemplar in the training and testing set. The trained decoders were applied to left-out trials depending on their cue dimensionality. For each training and decoding condition, only trials where the respective feature was probed (cf. cued) were included in the analysis. This approach allowed us to investigate feature-specific discriminatory signatures that were independent of cue dimensionality, and probe their representation as a function of cue dimensionality. In addition, we applied the same classifiers on all trials, during which the respective feature was not cued, to test whether participants represented prevalent feature options when the feature was not task-relevant.

**Note:** branch ```Paper2``` contains additional uncleaned exploratory analyses.

**A_masks**

- create NeuroSynth mask of visual motion
- create Juelich mask of visual ROIs
- create Juelich somatomotor mask

**B_extractV5TimeSeries**

- extract signals from left and right v5

**C_getTrialInfo**

- get trial-wise information about dominating option

**G6D_DecodeWinOption_VC_allTrials_3fold**

- Note: Matlab R2020a will not work for the makefile, R2016b works fine
- create a training set based on cued trials from each dim conditions (approx. normalized between dims to have similar representatives and not bias training set)
- implements single-trial bootstrapping (across 100 repetitions)

![image](figures/G_decode/G6D_winOption_VC_CuedUncued.png)
![image](figures/G_decode/G6D_winOption_VC_byFeature_acrossGroups.png)
![image](figures/G_decode/G6D_winOption_VC_byFeature.png)

To run, execute the following:

```datalad get ./tools/*```  
```datalad get ./data/C_trialInfo/*```  
```datalad get ./data/G_DecodingResults/*_g6d_accuracy.mat```

Statistical results reported in the manuscript can be found here (main effects model without Cook's-based data exclusion):
- G6D_stats/G6D_LinearMEM_all.html
- G6D_stats/G6D_LinearMEM_probed.html
- G6D_stats/G6D_LinearMEM_unprobed.html

Note: models including random subject slopes for dim did not converge or indicated extremely small random effects (see output in G6D_stats_rd_effect)!